import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import models.dto.client.UpdateAccountDTO;
import org.apache.commons.lang3.ClassUtils;
import org.junit.Test;
import utils.Logs;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-08-29 17:47
 * @description
 */
public class DocGeneratorTest {

    private static File file = new File("test/doc.yaml");

    static {
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                Logs.error("Failed to create file");
            }
        }
    }

    @Test
    public void testGenerateDocTxt() throws ClassNotFoundException {
        Class cls = UpdateAccountDTO.class;
        generateDoc(cls);

    }

    public void generateDoc(Class<?> cls) throws ClassNotFoundException {
        Logs.info("Reflecting class:{}", cls.getName());
        Field[] fields = cls.getDeclaredFields();
        int indent = 2;
        String modelName = cls.getSimpleName();
        StringBuilder content = new StringBuilder();
        content.append(multipleSpace(indent) + modelName + ":\n");
        indent += 2;
        content.append(multipleSpace(indent) + "type: " + Object.class.getSimpleName().toLowerCase() + "\n");
        content.append(multipleSpace(indent) + "description: \n");
        content.append(multipleSpace(indent) + "properties: \n");
        List<Class> classes = Lists.newArrayList();
        for (Field field : fields) {
            Integer propertyIndent = indent + 2;
            Class<?> typeClass = field.getType();
            content.append(multipleSpace(propertyIndent) + field.getName() + ":\n");
            propertyIndent += 2;
            if (typeClass == Date.class) {
                content.append(multipleSpace(propertyIndent) + "type: string\n");
                content.append(multipleSpace(propertyIndent) + "format: 'data-time'\n");
            } else if (typeClass.isArray() || "list".equalsIgnoreCase(typeClass.getSimpleName())) {
                content.append(multipleSpace(propertyIndent) + "type: array\n");
                content.append(multipleSpace(propertyIndent) + "items:\n");
                Type type = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                Class<?> nestedClass = null;
                try {
                    nestedClass = ClassUtils.getClass(type.getTypeName(), true);
                } catch (ClassNotFoundException e) {
                    Logs.error("Class {} not found", nestedClass.getName());
                }
                content.append(multipleSpace(propertyIndent + 2) + "$ref: \'#/definitions/" + nestedClass.getSimpleName() + "\'\n");
                if (String.class != nestedClass && Date.class != nestedClass && !nestedClass.isPrimitive() && !isWrapClass(nestedClass)) {
                    classes.add(nestedClass);
                }
            } else if (!typeClass.isPrimitive() && typeClass != String.class && !isWrapClass(typeClass)) {
                content.append(multipleSpace(propertyIndent) + "type: object\n");
                content.append(multipleSpace(propertyIndent) + "schema:\n");
                Class<?> objectClz = ClassUtils.getClass(field.getGenericType().getTypeName());
                content.append(multipleSpace(propertyIndent + 2) + "$ref: \'#/definitions/" + objectClz.getSimpleName() + "\'\n");
                if (String.class != objectClz && Date.class != objectClz && !objectClz.isPrimitive() && !isWrapClass(objectClz)) {
                    classes.add(objectClz);
                }
            } else {
                content.append(multipleSpace(propertyIndent) + "type: " + typeClass.getSimpleName().toLowerCase() + "\n");
            }
            content.append(multipleSpace(propertyIndent) + "description: \n");
        }
        try {
            Files.append(content, file, Charsets.UTF_8);
        } catch (IOException e) {
            Logs.error("Faild to append content to doc.yaml");
        }
        classes.forEach(aClass -> {
            try {
                Logs.info("Class name:" + aClass.getName());
                generateDoc(aClass);
            } catch (ClassNotFoundException e) {
                Logs.error("Class not found");
            }
        });
        Logs.info("Create {} model doc success", cls.getSimpleName());
    }

    public String multipleSpace(Integer spaceNumber) {
        char space = ' ';
        StringBuilder stringBuilder = new StringBuilder("");
        for (int i = 0; i < spaceNumber; i++) {
            stringBuilder.append(space);
        }
        return stringBuilder.toString();
    }

    public static boolean isWrapClass(Class clz) {
        try {
            return ((Class) clz.getField("TYPE").get(null)).isPrimitive();
        } catch (Exception e) {
            return false;
        }
    }

}
