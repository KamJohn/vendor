package models.dto.client;

import com.alibaba.fastjson.annotation.JSONField;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

import java.util.Date;
import java.util.Map;


/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-30 下午4:28
 * @description 申请单列表DTO
 */
public class RequisitionDTO {
    @MongoId
    @MongoObjectId
    public String id;

    /**
     * 申请单编号
     */
    public String clientNumber;

    /**
     * 检测项目
     */
    public String project;


    /**
     * 检测项目id
     */
    public String projectId;

    /**
     * 送检单位
     */
    public String organization;

    /**
     * 申请单创建日期
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    public Date createAt;


    /**
     * 受检者 以、分隔
     */
    public String customers;

    /**
     * 样本数量
     */
    public Integer sampleCount;

    /**
     * 申请单状态 是否可编辑 true:是 false:否
     */
    public boolean editable;

    @JSONField(serialize = false)
    public Map<String, Object> data;

    /**
     * 用来关联同一申请单下的不同样本批次
     */
    public String marker;

    /**
     * 快递单号
     */
    public String trackingNumber;

    /**
     * 预实验报告
     */
    @JSONField(serialize = false)
    public String preTestReport;

    /**
     * 报告
     */
    @JSONField(serialize = false)
    public String report;
    /**
     * 是否有报告
     */
    public boolean hasReport = false;

    /**
     * 预计生成报告时间
     */
    @JSONField(format = "yyyy-MM-dd")
    public Date expectedReportTime;

    /**
     * 是否有快递单号
     */
    public boolean hasTrackingNumber = false;

    /**
     * 申请单类型 pgs/gdna/mdpgd/rtpgd/azf
     */
    public String type;

    public boolean showTips;

    public Integer totalBatch;

    /**
     * 申请单样本批次
     */
    public Integer batchNumber;


    public String detectionModel;

}
