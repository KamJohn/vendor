package utils;

import com.google.common.collect.Maps;
import com.opencsv.bean.CsvToBeanBuilder;
import models.Region;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Map;

/**
 * 省市区三级联动.
 */
public class RegionUtils {
    private static final Logger logger = LoggerFactory.getLogger(RegionUtils.class);
    private static Map<String, String> lastRegion = Maps.newLinkedHashMap();


    /**
     * 初始化数据
     */
    private static void init() {
        List<Region> regions;
        try {
            regions = getAll();
        } catch (FileNotFoundException e) {
            Logs.error("Failed to get regions from csv file");
            return;
        }
        Map<Integer, String> r1 = Maps.newLinkedHashMap();
        Map<Integer, String> r2 = Maps.newLinkedHashMap();
        Map<String, String> r3 = Maps.newLinkedHashMap();
        for (Region region : regions) {
            Integer parentId = region.parentId;
            if (parentId == 0) {
                continue;
            }
            String name = region.regionName;
            Integer id = region.regionId;
            if (name.equals("县") || name.equals("市辖区")) {
                name = "";
            }
            if (parentId == 1) {
                r1.put(id, name);
                //一级区域也放进来
                r3.put(region.regionCode, name);
            }
            String r1Name = r1.get(parentId);
            if (!StringUtils.isBlank(r1Name)) {
                r2.put(id, r1Name + name);
                //广东省东莞市没有三级区域，所以要把二级也放进来
                r3.put(region.regionCode, r1Name + name);
            }
            String r2Name = r2.get(parentId);
            if (!StringUtils.isBlank(r2Name)) {
                r3.put(region.regionCode, r2Name + name);
            }
        }
        lastRegion = r3;
    }

    /**
     * 根据code获取完整的行政地址
     */
    public static String getFullRegionName(String code) {
        if (StringUtils.isNullOrEmpty(code)) {
            return "";
        }
        if (lastRegion.isEmpty()) {
            init();
        }
        String fullName = lastRegion.get(code);
        logger.debug("code:{} -> region:{}", code, fullName);
        return fullName;
    }


    public static List<Region> getAll() throws FileNotFoundException {
        List<Region> regions = new CsvToBeanBuilder(new FileReader("conf/region.csv"))
                .withSkipLines(1)
                .withType(Region.class).build().parse();
        return regions;
    }

}
