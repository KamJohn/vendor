package client;

import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.junit.Test;
import play.test.FunctionalTest;
import utils.Logs;

import java.io.IOException;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-05 下午4:43
 * @description
 */
public class OrganizationTest extends FunctionalTest {


    @Test
    public void testGetOrganizations() throws IOException {
        Response response = Request.Get("http://localhost:9010/v1/organizations")
                .execute();
        Logs.info(response.returnContent().asString());
    }


}
