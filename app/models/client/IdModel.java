package models.client;

import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-06 上午10:55
 * @description MongoIdModel
 */
public class IdModel {

    @MongoId
    @MongoObjectId
    public String id;

}
