package models.client;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-07 下午3:25
 * @description
 */
public class SampleState {

    /**
     * 样本检测状态
     */
    public String sampleState;
    /**
     * 样本检测状态变更时间
     */
    public String endDate;

    /**
     * 质检失败原因
     */
    public String qualityNot;

    public String getSampleState() {
        return sampleState;
    }


    public String getEndDate() {
        return endDate;
    }

    public String getQualityNot() {
        return qualityNot;
    }
}
