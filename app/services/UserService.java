package services;

import controllers.v1.open.LimsApi;
import exceptions.LimsException;
import models.client.UserInfo;
import models.dto.client.InputUserDTO;

import java.io.IOException;

public final class UserService {

    /**
     * @author  kam   @date 2019-09-09 14:25
     * @desc    用户登录验证
     * @param   inputUserDTO [用户名,密码,用户平台信息等]
     * @return  boolean
     */
    public static boolean verify(InputUserDTO inputUserDTO) {
        boolean success = false;
        if (inputUserDTO.platForm.equals("jb")) {
            try {
                success = LimsApi.verify(inputUserDTO.id, inputUserDTO.password);
            } catch (IOException | LimsException e) {
                e.printStackTrace();
            }
        } else {
            // 到数据库查询
            success = true;
        }
        return success;
    }

    public static UserInfo getUserInfo(InputUserDTO inputUserDTO){
        UserInfo userInfo = new UserInfo();

        return userInfo;
    }
}
