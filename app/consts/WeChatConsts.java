package consts;

import utils.APIConfig;

public class WeChatConsts {

    private static final String WECHAT_APPID = "wechat.appid";
    private static final String WECHAT_SECRET = "wechat.secret";


    public static String getWechatAppid() {
        return APIConfig.get(WECHAT_APPID);
    }

    public static String getWechatSecret() {
        return APIConfig.get(WECHAT_SECRET);
    }

}
