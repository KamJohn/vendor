package models;

import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

import java.util.Date;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-12-07 17:43
 * @description
 */
public class BaseOpenModel {

    @MongoId
    @MongoObjectId
    public String id;
    public Date createAt;
    public Date updateAt;
}
