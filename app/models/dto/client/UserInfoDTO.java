package models.dto.client;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-05 下午6:31
 * @description
 */
public class UserInfoDTO {

    /**
     * 用户id
     */
    public String id;

    /**
     * 送检单位
     */
    public String organization;

    /**
     * 送检科室
     */
    public String department;


    /**
     * 用户姓名
     */
    public String name;

    /**
     * 报告接收邮箱
     */
    public String email;

    /**
     * 联系电话
     */
    public String phone;

    public String sender;

    public String senderPhone;


}
