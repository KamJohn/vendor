package utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import consts.WeChatConsts;
import models.wechat.AuthInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.AlgorithmParameters;
import java.security.Security;
import java.text.MessageFormat;
import java.util.Arrays;

/**
 * @author: 克比
 * @Desc: 微信小程序相关工具类
 * @createTime: 2018年9月17日
 */
public final class WechatUtil {

    private static final Logger logger = LoggerFactory.getLogger(WechatUtil.class);


    public static AuthInfo code2Session(String code) {
        // 接收用户传过来的code，required=false表明如果这个参数没有传过来也可以。

        // 接收从客户端获取的code， 向微信后台发起请求获取openid的url
        String wxUrl = "https://api.weixin.qq.com/sns/jscode2session?appid={0}&secret={1}&js_code={2}&grant_type=#{3}";

        String requestUrl = MessageFormat.format(wxUrl, WeChatConsts.getWechatAppid(), WeChatConsts.getWechatSecret(), code, "authorization_code");

        // 调用get方法发起get请求，并把返回值赋值给returnvalue
        String returnvalue = HttpUtil.sendGet(requestUrl);

        // 将获取的结果转换为json对象
        JSONObject convertValue = JSON.parseObject(returnvalue);

        Integer errCode = convertValue.getInteger("errcode");
        AuthInfo authInfo = new AuthInfo();
        //微信官方错误码,标识接口调用失败
        if (errCode != null && !errCode.equals(0)) {
            String errMsg = convertValue.getString("errmsg");
            authInfo.setErrCode(errCode);
            authInfo.setErrMsg(errMsg);
            logger.error("登录凭证校验失败,code:{},errCode:{},errMsg:{}", code, errCode, errMsg);
            return authInfo;
        }
        String openId = convertValue.getString("openid");
        String sessionKey = convertValue.getString("session_key");
        String unionId = convertValue.getString("unionid");
        // 将获取得到的openid和sessionkey封装到map中
        authInfo.setOpenId(openId);
        authInfo.setSessionKey(sessionKey);
        authInfo.setUnionId(unionId);
        logger.info("登录凭证校验成功,code:{},errCode:{},openId:{},sessionKey:{}", code, errCode, openId, sessionKey);
        return authInfo;
    }

    /**
     * @param encryptedData, code, iv [ 原始加密数据,用户登录code,加密算法初始向量 ]
     * @return 用户手机号, 获取失败是返回字符串
     * @author kam   @date 2019-09-16 18:31
     * @desc 获取手机号, 不加区号(暂时不考虑国外手机号的情况)
     */
    public static String getPhoneNumber(String encryptedData, String code, String iv) {
        //HttpUtil.getOpenid这个方法是我自己封装的方法，传入code后然后获取openid和session_key的，把他们封装到json里面
        AuthInfo authInfo = code2Session(code);
        String sessionKey = "B4hk0kfYnJ7BcZyFyc5ihw==";
        if (!StringUtils.isEmpty(sessionKey)) {
            // 被加密的数据
            byte[] dataByte = Base64.decode(encryptedData);
            // 加密秘钥
            byte[] keyByte = Base64.decode(sessionKey);
            // 偏移量
            byte[] ivByte = Base64.decode(iv);
            try {
                // 如果密钥不足16位，那么就补足.  这个if 中的内容很重要
                int base = 16;
                if (keyByte.length % base != 0) {
                    int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
                    byte[] temp = new byte[groups * base];
                    Arrays.fill(temp, (byte) 0);
                    System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
                    keyByte = temp;
                }
                // 初始化
                Security.addProvider(new BouncyCastleProvider());
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
                SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
                AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
                parameters.init(new IvParameterSpec(ivByte));
                cipher.init(Cipher.DECRYPT_MODE, spec, parameters);// 初始化
                byte[] resultByte = cipher.doFinal(dataByte);
                if (null != resultByte && resultByte.length > 0) {
                    String result = new String(resultByte, "UTF-8");
                    JSONObject resultJson = JSONObject.parseObject(result);
                    String phoneNumber = resultJson.getString("purePhoneNumber");
                    return phoneNumber;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return utils.StringUtils.EMPTY;
    }

    public static void main(String[] args) {
        String sessionKey = "B4hk0kfYnJ7BcZyFyc5ihw==";
        String enc = "0vfi9hKvoTxOYUgyzRnBieawmWwvIi8wTNgB0UNt+mOgryCFn3e6OBER6ju8arFidvFgfcvtwsUjABnhHuc0azN+yT0GCrz9ZsCeg9CKM8crRSeVVqbK8+NZC3Wsug5SgYsdg6wwsXfapCA5XiA0dp37G4pLaVm4dWKVZi078eJsWkjjnD/I4V6S4ZE9Paklq8/JQxiTT2NeKXEx8qj62Q==";
        String code = "033bYjpl1LXuKm0s8Vql1Eklpl1bYjp6";
        String iv = "pRnGUTbytp+H8oyCGfg4RQ==";
        String phoneNumber = getPhoneNumber(enc, code, iv);
        System.out.println(phoneNumber);
    }
}