package controllers.v1;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import controllers.api.API;
import controllers.api.interceptor.Secure;
import play.mvc.With;

import java.util.List;
import java.util.Map;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-06 下午2:40
 * @description
 */
@With({Secure.class})
public class SampleTypes extends API {

    private static Map<String, List<String>> sampleTypeMap = Maps.newHashMap();

    static {
        sampleTypeMap.put("pgs", Lists.newArrayList("细胞", "MDA扩增产物", "Surplex扩增产物", "文库"));
        sampleTypeMap.put("gdna", Lists.newArrayList("外周血", "流产组织", "绒毛", "gDNA", "文库"));
        sampleTypeMap.put("mdpgd", Lists.newArrayList("外周血", "gDNA", "MDA扩增产物", "Surplex扩增产物", "细胞", "PGS文库", "PGD文库"));
        sampleTypeMap.put("rtpgd", Lists.newArrayList("外周血", "gDNA", "MDA扩增产物", "Surplex扩增产物", "细胞", "PGS文库", "PGD文库"));
        sampleTypeMap.put("azf", Lists.newArrayList("外周血", "gDNA", "文库"));
    }

    public static void getSampleTypes(String type) {
        List<String> sampleTypes = sampleTypeMap.getOrDefault(type, Lists.newArrayList());
        renderJSON(sampleTypes);
    }

}
