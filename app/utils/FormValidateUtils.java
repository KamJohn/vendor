package utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import consts.Project;
import models.client.FormValidation;
import models.client.Mutation;
import models.client.Requisition;
import models.client.Sample;

import java.util.List;
import java.util.Map;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-25 下午8:05
 * @description 申请单表单校验
 */
public class FormValidateUtils {

    private static Map<String, FormValidation> requisitionValidationMap = Maps.newHashMap();

    private static List<String> commonBaseValidation = Lists.newArrayList("organization", "sender", "senderPhone", "sampleType", "laboratory", "projectType");

    private static List<String> commonSampleValidation = Lists.newArrayList("barCode", "number");

    //暂时去掉data中某些字段必填校验
    static {
        //孕前基因检测683
        requisitionValidationMap.put(Project.DRR_683.id(), FormValidation.with()
                .base(commonBaseValidation)
                .base("femaleName", "maleName", "femaleSamplingDate", "maleSamplingDate")
                .sample(commonSampleValidation)
                .sample("gender"));
        //孕前基因检测18
        requisitionValidationMap.put(Project.DRR_18.id(), FormValidation.with()
                .base(commonBaseValidation)
                .base("femaleName", "maleName", "femaleSamplingDate", "maleSamplingDate")
                .sample(commonSampleValidation)
                .sample("gender"));
        //AZF
        requisitionValidationMap.put(Project.AZF.id(), FormValidation.with()
                .base(commonBaseValidation)
                //.base("name", "age", "clinicNumber")
                .base("name", "age")
                .sample(commonSampleValidation)
                .sample("type", "relation"));
        //脆性X综合征
        requisitionValidationMap.put(Project.X_FMR1.id(), FormValidation.with()
                .base(commonBaseValidation)
                .base("name")
                .sample(commonSampleValidation)
                .sample("type", "relation"));
        //PGS
        requisitionValidationMap.put(Project.PGS.id(), FormValidation.with()
                .base(commonBaseValidation)
                //.base("femaleName", "femaleAge", "femaleResult", "sendDate", "clinicNumber")
                //.base("femaleName", "femaleResult", "sendDate")
                .sample(commonSampleValidation));
        //单病PGD
        requisitionValidationMap.put(Project.MD_PGD.id(), FormValidation.with()
                .base(commonBaseValidation)
                //.base("maleName", "maleAge", "femaleName", "hasProband", "detectionProjects", "sendDate", "diseases", "clinicNumber")
                .base("maleName", "femaleName", "hasProband", "detectionProjects", "sendDate", "diseases")
                .baseIgnore("laboratory")
                .sample(commonSampleValidation)
                .sample("type", "relation", "mutations"));
        //HLA
        requisitionValidationMap.put(Project.HLA_PGD.id(), FormValidation.with()
                .base(commonBaseValidation)
                //.base("maleName", "maleAge", "femaleName", "femaleAge", "hasProband", "detectionProjects", "sendDate", "clinicNumber")
                .base("maleName", "femaleName", "hasProband", "detectionProjects")
                .baseIgnore("laboratory")
                .sample(commonSampleValidation)
                .sample("type", "relation"));
        //罗氏易位染色体拷贝数PGD
        requisitionValidationMap.put(Project.RT_PGD.id(), FormValidation.with()
                .base(commonBaseValidation)
                //.base("maleName", "maleAge", "femaleName", "maleResult", "femaleResult", "sendDate","clinicNumber")
                .base("maleName", "femaleName", "maleResult", "femaleResult", "sendDate")
                .sample(commonSampleValidation));
        //罗氏易位携带者
        requisitionValidationMap.put(Project.S_PGD.id(), FormValidation.with()
                .base(commonBaseValidation)
                //.base("maleName", "maleAge", "femaleName", "femaleAge", "clinicNumber")
                .base("maleName", "femaleName")
                .baseIgnore("laboratory")
                .sample(commonSampleValidation)
                .sample("type", "relation", "analysisResult"));
        //平衡易位染色体拷贝数PGD
        requisitionValidationMap.put(Project.B_PGD.id(), FormValidation.with()
                .base(commonBaseValidation)
                //.base("maleName", "maleAge", "femaleName", "sendDate", "clinicNumber")
                .base("maleName", "femaleName", "sendDate")
                .sample(commonSampleValidation));
        //倒位染色体拷贝数PGD
        requisitionValidationMap.put(Project.R_PGD.id(), FormValidation.with()
                .base(commonBaseValidation)
                //.base("maleName", "maleAge", "femaleName", "maleResult", "femaleResult", "sendDate", "clinicNumber")
                .base("maleName", "femaleName", "maleResult", "femaleResult", "sendDate")
                .sample(commonSampleValidation));
        //平衡易位携带者Micro-Seq
        requisitionValidationMap.put(Project.B_MISEQ.id(), FormValidation.with()
                .base(commonBaseValidation)
                //.base("maleName", "femaleName", "detectionProjects", "femaleSamplingDate", "maleSamplingDate")
                .base("maleName", "femaleName", "detectionProjects", "femaleSamplingDate", "maleSamplingDate")
                .baseIgnore("laboratory")
                .sample(commonSampleValidation)
                .sample("type", "relation"));
        //无创产前DNA检测
        requisitionValidationMap.put(Project.NIPT.id(), FormValidation.with());
        //羊水组织细胞染色体非整倍体检测
        requisitionValidationMap.put(Project.AMNIOTIC_FLUID.id(), FormValidation.with()
                .base(commonBaseValidation)
                .base("femaleName")
                .sample(commonSampleValidation)
                .sample("type", "relation")
        );
        //g-DNA流产组织学
        requisitionValidationMap.put(Project.GDNA.id(), FormValidation.with()
                .base(commonBaseValidation)
                //.base("femaleName", "femaleAge", "sendDate", "clinicNumber")
                .base("femaleName", "sendDate")
                .sample(commonSampleValidation)
                .sample("type", "relation"));
        //染色体核型分析
        requisitionValidationMap.put(Project.CHR_KARYOTYPE.id(), FormValidation.with()
                .base(commonBaseValidation)
                .base("name", "gender", "sendDate")
                .sample(commonSampleValidation));
    }


    public static void validate(Requisition requisition) {
        Map data = requisition.data;
        if (data == null) {
            throw new IllegalArgumentException("data is required");
        }
        String projectId = requisition.projectId;
        if (org.apache.commons.lang3.StringUtils.isBlank(projectId)) {
            throw new IllegalArgumentException("projectId is required.");
        }
        FormValidation validateFields = requisitionValidationMap.get(requisition.projectId);
        if (validateFields == null) {
            throw new IllegalArgumentException("Illegal projectId");
        }
        List<String> requisitionFields = validateFields.getBase();
        Object detectModel = data.get("detectionModel");
        if (detectModel != null) {
            String value = String.valueOf(detectModel);
            if (value.equals("male")) {
                requisitionFields.removeAll(Lists.newArrayList("femaleName", "femaleSamplingDate"));
            } else if (value.equals("female")) {
                requisitionFields.removeAll(Lists.newArrayList("maleName", "maleSamplingDate"));
            }
        }
        requisitionFields.forEach(s -> {
            if (!data.containsKey(s)) {
                throw new IllegalArgumentException("data." + s + " is required");
            }
        });
        List<String> sampleFileds = validateFields.getSample();
        List<Sample> samples = JSON.parseArray(JSON.toJSONString(requisition.samples), Sample.class);
        if (samples == null || samples.isEmpty()) {
            throw new IllegalArgumentException("samples is required");
        }
        samples.forEach(sample -> {
            Map<String, Object> map = (Map) JSON.toJSON(sample);
            String relation = "";
            for (String s : sampleFileds) {
                Object value = map.get(s);
                if (s.equals("analysisResult") && relation.contains("子代胚胎")) {
                    continue;
                }
                if (value == null) {
                    throw new IllegalArgumentException("sample." + s + " is required");
                }
                if (value instanceof String) {
                    value = map.get(s);
                    if (org.apache.commons.lang3.StringUtils.isBlank((String) value)) {
                        throw new IllegalArgumentException("sample." + s + " is required");
                    }
                } else if (value instanceof List) {
                    List<Mutation> mutations = JSONArray.parseArray(JSON.toJSONString(value), Mutation.class);
                    if (relation.contains("子代胚胎")) {
                        continue;
                    } else {
                        if (mutations.isEmpty()) {
                            throw new IllegalArgumentException("请补全突变类型信息");
                        }
                        mutations.forEach(mutation -> {
                            if (StringUtils.isBlank(mutation.mutationType)) {
                                throw new IllegalArgumentException("疾病" + mutation.diseaseName + "的突变类型不能为空");
                            }
                        });
                    }
                }
                if (s.equals("relation")) {
                    relation = (String) map.get(s);
                }
            }
        });
    }
}
