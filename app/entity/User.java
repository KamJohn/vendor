package entity;

import javax.persistence.Entity;

@Entity(name = "t_user")
public class User extends MysqlBaseModel {

    /**
     * 微信用户唯一标识，仅当用户角色为sampler时有值
     */
    private String openId;

    /**
     * 用户手机号；当用户角色为收样员时，此值同用户微信号码绑定的手机号
     */
    private String phone;

    /**
     * 用户名(非微信昵称)
     */
    private String userName;

    /**
     * 用户登录名
     */
    private String userId;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 用户所属公司编码
     */
    private Integer companyCode;

    /**
     * 用户角色;管理员 admin;收养人员 sampler;实验室人员 worker
     */
    private String userRole;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLoginName() {
        return userId;
    }

    public void setLoginName(String loginName) {
        this.userId = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(Integer companyCode) {
        this.companyCode = companyCode;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
}
