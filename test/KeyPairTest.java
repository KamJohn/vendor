import models.KeyPair;
import models.api.Jsonable;
import models.api.ObjectId;
import org.jongo.MongoCollection;
import org.jongo.Oid;
import org.junit.Test;
import play.test.FunctionalTest;
import utils.MongoUtils;
import utils.SignUtil;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-04-26 下午6:23
 */
public class KeyPairTest extends FunctionalTest {

    private static MongoCollection keypair = MongoUtils.getKeyPairCollection();

    @Test
    public void testCreateKeyPair() {
        KeyPair keyPair = SignUtil.generateKeyPair(ObjectId.stringId(), "lims", null);
        keypair.save(keyPair);
    }

    @Test
    public void testGetKeyPair(){
        KeyPair keyPair = keypair.findOne(Oid.withOid("")).as(KeyPair.class);
        System.out.println(Jsonable.toPrettyJson(keyPair));
    }
}
