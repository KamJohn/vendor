package consts;


import play.Play;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-04-05 下午6:30
 */
public class MongoConsts {

    public static final String URI = "mongo.uri";

    public static String getUri() {
        return Play.configuration.getProperty(URI);
    }

}
