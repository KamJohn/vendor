package consts;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-27 下午3:05
 * @description 客户端常量
 */
public class ClientConsts {

    public static final String PGS = "pgs";

    public static final String GDNA = "gdna";

    public static final String MD_PGD = "mdpgd";

    public static final String RT_PGD = "rtpgd";

    public static final String AZF = "azf";

    public static final String CLIENT_REPORT_DIR = "reports/";
    public static final String CLIENT_IMAGE_DIR = "images/";

    public static final String CLIENT_NUMBER_INCR_KEY = "clientNumber:Incr";


    public enum Collection {
        REQUISITION("Requisition", "申请单"),
        SAMPLE("Sample", "样本"),
        KEYPAIR("KeyPair", "接口密钥信息"),
        DEPARTMENT("Department", "科室信息"),
        RELATION("Relation", "亲缘关系列表"),
        File("File", "文件"),
        SAMPLE_TYPE("SampleType", "样本类型列表"),
        REPORT("Report", "报告"),
        PROJECT("Project", "检测项目"),
        HOSPITAL("Hospital", "医院信息");

        private String value;

        private String desc;

        Collection(String value, String desc) {
            this.value = value;
            this.desc = desc;
        }

        public String value() {
            return value;
        }

        public String desc() {
            return desc;
        }

    }

}
