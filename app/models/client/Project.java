package models.client;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-20 下午7:08
 * @description
 */
public class Project {

    public String name;

    public String id;

    public String route;

    public Project(String name, String id, String route) {
        this.name = name;
        this.id = id;
        this.route = route;
    }
}
