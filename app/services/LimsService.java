package services;

import models.client.SampleStateList;
import models.client.UserInfo;
import models.dto.open.LimsPGDFamilySampleDTO;
import models.dto.open.LimsRelation;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017/6/5 下午12:16
 */
public interface LimsService {

    @GET("rest/RestfulService/personShip")
    Call<List<LimsRelation>> getRelations();

    /**
     * pgs分析结果通知
     *
     * @param id
     * @param type
     * @return
     */
    @POST("rest/RestfulService/biologinfo")
    Call<ResponseBody> notifyPGSResult(@Query("id") String id, @Query("type") String type);

    @POST("rest/RestfulService/downloadZip")
    Call<ResponseBody> notifyPGDResult(@Query("id") String id);

    /**
     * DRR申请单通知
     *
     * @param id
     * @return
     */
    @GET("rest/RestfulService/tz/{id}")
    Call<ResponseBody> notifyLims(@Path("id") String id);

    /**
     * 下载drr微信端报告
     *
     * @param fileName
     * @return
     */
    @GET("rest/RestfulService/download/{fileName}")
    Call<ResponseBody> getReport(@Path("fileName") String fileName);

    /**
     * 下载客户端报告
     *
     * @param fileName
     * @return
     */
    @GET("rest/RestfulService/download/{fileName}")
    Call<ResponseBody> getClientReport(@Path("fileName") String fileName);

    /**
     * 登录认证
     *
     * @param id
     * @param password
     * @return
     */
    @POST("rest/RestfulService/loginto")
    Call<ResponseBody> verifyUser(@Query("id") String id, @Query("password") String password);

    /**
     * 获取用户信息
     *
     * @param id
     * @param password
     * @return
     */
    @POST("rest/RestfulService/userInformation")
    Call<UserInfo> getUserInfo(@Query("id") String id, @Query("password") String password);

    @GET("rest/RestfulService/updateUser")
    Call<ResponseBody> updateAccount(@Query("id") String id, @Query("passwordid") String passwordId, @Query("password") String password);


    /**
     * 提交申请单
     *
     * @return
     */
    @POST("rest/RestfulService/applicationForm")
    Call<ResponseBody> submitRequisition(@Body RequestBody body);


    /**
     * 查询样本状态
     *
     * @param orderCode lims内部单号
     * @return
     */
    @GET("rest/RestfulService/stateQuery/{orderCode}")
    Call<List<SampleStateList>> querySampleState(@Path("orderCode") String orderCode);

    /**
     * 快递单号通知信息
     *
     * @param body
     * @return
     */
    @POST("rest/RestfulService/courier")
    Call<ResponseBody> notifySampleDelivery(@Body RequestBody body);

    /**
     * 查询pgd家系样本信息
     * @param familyName
     * @return
     */
    @GET("rest/RestfulService/getFamilyInfo/{familyName}")
    Call<LimsPGDFamilySampleDTO> queryPGDFamilySample(@Path("familyName") String familyName);

}
