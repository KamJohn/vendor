import com.google.common.collect.Maps;
import controllers.api.API;
import models.webhook.NotificationType;
import models.webhook.Webhook;
import org.hashids.Hashids;
import org.junit.Test;
import play.mvc.Http.Response;
import play.test.FunctionalTest;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class ApplicationTest extends FunctionalTest{

    @Test
    public void testThatIndexPageWorks() {
        Response response = GET("/");
        assertIsOk(response);
        assertContentType("text/html", response);
        assertCharset(play.Play.defaultWebEncoding, response);
    }

    @Test
    public void testWebhook() {
        Webhook webhook = new Webhook();
        webhook.notificationType = NotificationType.SAMPLE_RECEIVED.value();
        webhook.dataId = "sample";
        Response response = POST("/v1/webhook");
        assertIsOk(response);

    }

    @Test
    public void testUniqueString() {
        String codeAlphabet = "abcdefghijklmnpqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Map map = Maps.newHashMap();
        for (int i = 1; i <= 10000000; i++) {
            Hashids hashids = new Hashids(UUID.randomUUID().toString(), 10, codeAlphabet);
            String code = hashids.encode(ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));
            System.out.println("\ncode-->" + code + " " + i + "/100000000");
            if (map.get(code) != null) {
                System.out.println("duplicate code-->" + code);
                break;
            }
            map.put(code, 1);
        }
    }

}