#!/usr/bin/env bash
export PATH=$PATH:/root/tools/play-1.4.4/
cd ../
rm -fr precompiled
rm -fr web.zip
/root/tools/play-1.4.4/play clean
/root/tools/play-1.4.4/play deps --verbose
/root/tools/play-1.4.4/play precompile
sed -i -- 's/localhost:9000/devvendor.pgxcloud.com/g' docs/api-v1.yaml
sed -i -- 's/http/https/1' docs/api-v1.yaml
zip -r web.zip conf modules lib precompiled public-api docs
