package interceptors;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import play.Play;
import utils.Logs;
import utils.SignatureUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * @author zanxus
 * @date 2017-05-04 下午3:54
 * @description 拦截api请求加签名参数
 */
public class SigningInterceptor implements Interceptor {

    private String accessKey;

    private String accessSecret;

    private final String ACCESS_KEY_ID = "access_key_id";

    private final String SIGNATURE_VERSION = "signature_version";

    private final String SIGNATURE = "signature";

    private final String baseUrl;

    public SigningInterceptor(String baseUrl,String accessKey, String accessSecret) {
        this.baseUrl = baseUrl;
        this.accessKey = accessKey;
        this.accessSecret = accessSecret;
    }

    public String sign(HttpUrl url,String method, String path) {
        Set<String> queryParameterNames =  url.queryParameterNames();
        //query string
        Map<String, String> params = new HashMap<>();
        IntStream.range(0, queryParameterNames.size()).forEach(index -> params.put(url.queryParameterName(index), url.queryParameterValue(index)));
        params.put(ACCESS_KEY_ID, accessKey);
        params.put(SIGNATURE_VERSION, "1");
        //sign
        String signature = SignatureUtil.computeSignature(accessSecret, method, path, params);
        params.put(SIGNATURE, signature);
        String queryString = SignatureUtil.map2QueryString(params);
        String finalUrl = new StringBuilder(baseUrl).append(path).append("?").append(queryString).toString();
        return finalUrl;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        String path = request.url().encodedPath();
        HttpUrl url = request.url();
        String method = request.method();
        String signedUrl = sign(url,method, path);
        Request signedRequest = request.newBuilder()
                .url(signedUrl)
                .build();
        return chain.proceed(signedRequest);
    }
}
