package controllers.v1.open;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import controllers.api.API;
import interceptors.Signature;
import java.util.List;
import models.PGDFamilySample;
import models.dto.open.LimsPGDFamilySampleDTO;
import models.dto.open.PGDFamilySampleDTO;
import play.data.validation.Required;
import play.mvc.With;
import utils.Logs;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-04-25 下午5:06
 */
@With({Signature.class})
public class PGDFamilySamples extends API {

    public static void queryPGDFamilySample(@Required String familyName) throws Exception {
        LimsPGDFamilySampleDTO familySampleDTO = LimsApi.queryPGDFamilySample(familyName);
        Logs.info("query lims pgd family sample success.familyName:{},familySampleDTO:{}", familyName, JSON.toJSON(familySampleDTO));
        PGDFamilySampleDTO dto = new PGDFamilySampleDTO();
        if(familySampleDTO.id!=null){
            dto.familyName = familyName;
        }
        if(familySampleDTO.sampleInfos!=null){
            List<PGDFamilySample> pgdFamilySamples = Lists.newArrayList();
            familySampleDTO.sampleInfos.forEach(limsSample -> {
                PGDFamilySample familySample = new PGDFamilySample();
                familySample.sampleName = limsSample.sampleCode;
                familySample.libraryCode = limsSample.libraryCode;
                familySample.mutationType = limsSample.mutationType;
                familySample.relationId = limsSample.personShipId;
                familySample.relationName = limsSample.personShipName;
                familySample.remark = limsSample.name;
                familySample.outCode = limsSample.outCode;
                pgdFamilySamples.add(familySample);
            });
            dto.pgdFamilySamples = pgdFamilySamples;
        }
        Logs.info("query pgxcloud pgd family sample success.pGDFamilySampleDTO:{}",JSON.toJSONString(dto));
        renderJSON(dto);
    }

}
