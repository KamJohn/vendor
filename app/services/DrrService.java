package services;

import models.DrrRequisition;
import models.PGSResult;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-05-03 下午4:29
 */
public interface DrrService {

    @GET("/v1/open/pgs/results/{id}")
    Call<PGSResult> getPGSResult(@Path("id") String id);

    @GET("/v1/open/requisition/drr/{orderCode}")
    Call<DrrRequisition> getRequisition(@Path("orderCode") String orderCode);

    @Multipart
    @POST("/v1/open/reports/drr")
    Call<ResponseBody> uploadReport(@Query("barCode") String barCode, @Query("fileName") String fileName, @Part MultipartBody.Part file);

    @POST("/v1/open/webhook")
    Call<String> notifyPrecisource(@Body RequestBody body);


}
