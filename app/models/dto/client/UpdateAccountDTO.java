package models.dto.client;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-10-11 13:59
 * @description 更新用户账号DTO
 */
public class UpdateAccountDTO {

    public String id;

    public String originPassword;

    public String confirmPassword;

    public String newPassword;

}
