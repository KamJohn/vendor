package utils;

import com.qingstor.sdk.config.EvnContext;
import com.qingstor.sdk.exception.QSException;
import com.qingstor.sdk.service.Bucket;
import com.qingstor.sdk.service.QingStor;
import consts.QingCloudConsts;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import play.libs.MimeTypes;

import java.io.File;
import java.io.InputStream;

/**
 * @author <a href="mailto:fivesmallq@gmail.com">fivesmallq</a>
 * @version Revision: 1.0
 * @date 17/1/17 上午11:02
 */
public class QingstorUtils {
    private static final Logger logger = Logs.get();
    private static QingStor images;
    private static Bucket imageBucket;

    static {
        EvnContext uploadEvnContext = new EvnContext(QingCloudConsts.getStoreAccessKey(), QingCloudConsts.getStoreAccessSecret());
        images = new QingStor(uploadEvnContext, QingCloudConsts.getStoreBucketZone());
        imageBucket = images.getBucket(QingCloudConsts.getStoreBucketName(), QingCloudConsts.getStoreBucketZone());
    }


    public static InputStream getObject(String objectKey) throws QSException {
        StopWatch watch = new StopWatch();
        watch.start();
        Bucket.GetObjectOutput object = imageBucket.getObject(objectKey, null);
        watch.stop();
        logger.info("get object time :{}", watch.getTime());
        if (!object.getStatueCode().equals(200)) {
            throw new QSException("获取文件失败,文件路径: " + objectKey);
        }
        InputStream bodyInputStream = object.getBodyInputStream();
        return bodyInputStream;
    }


    /**
     * 上传到bucket下的指定目录， objectKey用来指定位置
     *
     * @param file      上传的文件
     * @param objectKey 在bucket中的存储目录
     * @return
     * @throws QSException
     */
    public static String upload(File file, String objectKey) throws QSException {
        Bucket.PutObjectInput input = new Bucket.PutObjectInput();
        input.setBodyInputFile(file);
        input.setContentType(MimeTypes.getContentType(file.getName()));
        input.setContentLength(file.length());
        Bucket.PutObjectOutput putObjectOutput;
        try {
            putObjectOutput = imageBucket.putObject(objectKey, input);
        } catch (QSException e) {
            logger.error("上传文件至青云对象存储失败,文件位置：{}", file.getAbsolutePath(), e);
            throw new QSException(e.getErrorMessage(), e);
        }
        return putObjectOutput.getUrl();
    }
}
