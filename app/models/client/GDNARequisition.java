package models.client;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.collect.Lists;
import play.data.validation.Required;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-26 上午11:01
 * @description gDNA申请单
 */
public class GDNARequisition extends PGXRequisition {

    /**
     * 受检者(男)
     */
    @Required
    private String maleName;

    /**
     * 男方年龄
     */
    private Integer maleAge;

    /**
     * 受检者核型结果(男)
     */
    @Required
    private String maleResult;

    /**
     * 受检者(女)
     */
    @Required
    private String femaleName;


    /**
     * 女方年龄
     */
    private Integer femaleAge;


    /**
     * 受检者核型结果(女)
     */
    @Required
    private String femaleResult;

    /**
     * 住院/门诊号
     */
    private String clinicNumber;

    /**
     * 送检日期
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String sendDate;


    /**
     * 样本类型  外周血/流产组织/绒毛/gDNA
     */
    @Required
    private List<String> sampleType;

    /**
     * 本人有无遗传病史 ture:有 false:无
     */
    private Boolean selfGeneticDisease = false;

    /**
     * 本人遗传病史描述
     */
    private String selfGeneticDiseaseDesc;

    /**
     * 亲属有无遗传病史 ture:有 false:无
     */
    private Boolean relativesGeneticDisease = false;

    /**
     * 亲属遗传病史描述
     */
    private String relativesGeneticDiseaseDesc;

    /**
     * 是否近亲结婚 true:是 false:否
     */
    @Required
    private Boolean intermarry = false;

    /**
     * 有无子女 true:有 false:无
     */
    @Required
    private Boolean hasChildren = false;

    /**
     * 子女个数
     */
    private Integer childrenCount;

    /**
     * 子女男孩个数
     */
    private Integer boysCount;

    /**
     * 子女女孩个数
     */
    private Integer girlsCount;

    /**
     * 子女是否健康  true:健康 false:不健康
     */
    private Boolean childrenHealthy = true;

    /**
     * 不健康子女诊断
     */
    private String childrenDiagnosis;

    /**
     * 有无流产史 true:有 false:无
     */
    @Required
    private Boolean hasAbortion = false;

    /**
     * 怀孕次数
     */
    private Integer pregnancyNumber;

    /**
     * 生产/分娩次数
     */
    private Integer birthNumber;

    /**
     * 本次第几胎
     */
    private Integer fetusNumber;

    /**
     * 孕期:第几周
     */
    private Integer pregnancyWeeks;

    /**
     * 孕期:孕周第几天
     */
    private Integer pregnancyDays;

    /**
     * 超声影像-NT值
     */
    @Required
    private Double ultrasoundNt;

    /**
     * 其他发育异常
     */
    private String otherDysplasia;

    /**
     * 有无染色体核型分析 true:有  false:无
     */
    @Required
    private Boolean hasKaryotypeAnalysis = false;

    /**
     * 染色体核型分析
     */
    private String karyotypeAnalysisDesc;

    /**
     * 样本信息集合
     */
    private List<Sample> samples = Lists.newArrayList();


    public String getMaleName() {
        return maleName;
    }

    public void setMaleName(String maleName) {
        this.maleName = maleName;
    }

    public Integer getMaleAge() {
        return maleAge;
    }

    public void setMaleAge(Integer maleAge) {
        this.maleAge = maleAge;
    }

    public String getMaleResult() {
        return maleResult;
    }

    public void setMaleResult(String maleResult) {
        this.maleResult = maleResult;
    }

    public String getFemaleName() {
        return femaleName;
    }

    public void setFemaleName(String femaleName) {
        this.femaleName = femaleName;
    }

    public Integer getFemaleAge() {
        return femaleAge;
    }

    public void setFemaleAge(Integer femaleAge) {
        this.femaleAge = femaleAge;
    }

    public String getFemaleResult() {
        return femaleResult;
    }

    public void setFemaleResult(String femaleResult) {
        this.femaleResult = femaleResult;
    }

    public String getClinicNumber() {
        return clinicNumber;
    }

    public void setClinicNumber(String clinicNumber) {
        this.clinicNumber = clinicNumber;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public List<String> getSampleType() {
        return sampleType;
    }

    public void setSampleType(List<String> sampleType) {
        this.sampleType = sampleType;
    }

    public Boolean getSelfGeneticDisease() {
        return selfGeneticDisease;
    }

    public void setSelfGeneticDisease(Boolean selfGeneticDisease) {
        this.selfGeneticDisease = selfGeneticDisease;
    }

    public String getSelfGeneticDiseaseDesc() {
        return selfGeneticDiseaseDesc;
    }

    public void setSelfGeneticDiseaseDesc(String selfGeneticDiseaseDesc) {
        this.selfGeneticDiseaseDesc = selfGeneticDiseaseDesc;
    }

    public Boolean getRelativesGeneticDisease() {
        return relativesGeneticDisease;
    }

    public void setRelativesGeneticDisease(Boolean relativesGeneticDisease) {
        this.relativesGeneticDisease = relativesGeneticDisease;
    }

    public String getRelativesGeneticDiseaseDesc() {
        return relativesGeneticDiseaseDesc;
    }

    public void setRelativesGeneticDiseaseDesc(String relativesGeneticDiseaseDesc) {
        this.relativesGeneticDiseaseDesc = relativesGeneticDiseaseDesc;
    }

    public Boolean getIntermarry() {
        return intermarry;
    }

    public void setIntermarry(Boolean intermarry) {
        this.intermarry = intermarry;
    }

    public Boolean getHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(Boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public Integer getChildrenCount() {
        return childrenCount;
    }

    public void setChildrenCount(Integer childrenCount) {
        this.childrenCount = childrenCount;
    }

    public Integer getBoysCount() {
        return boysCount;
    }

    public void setBoysCount(Integer boysCount) {
        this.boysCount = boysCount;
    }

    public Integer getGirlsCount() {
        return girlsCount;
    }

    public void setGirlsCount(Integer girlsCount) {
        this.girlsCount = girlsCount;
    }

    public Boolean getChildrenHealthy() {
        return childrenHealthy;
    }

    public void setChildrenHealthy(Boolean childrenHealthy) {
        this.childrenHealthy = childrenHealthy;
    }

    public String getChildrenDiagnosis() {
        return childrenDiagnosis;
    }

    public void setChildrenDiagnosis(String childrenDiagnosis) {
        this.childrenDiagnosis = childrenDiagnosis;
    }

    public Boolean getHasAbortion() {
        return hasAbortion;
    }

    public void setHasAbortion(Boolean hasAbortion) {
        this.hasAbortion = hasAbortion;
    }

    public Integer getPregnancyNumber() {
        return pregnancyNumber;
    }

    public void setPregnancyNumber(Integer pregnancyNumber) {
        this.pregnancyNumber = pregnancyNumber;
    }

    public Integer getBirthNumber() {
        return birthNumber;
    }

    public void setBirthNumber(Integer birthNumber) {
        this.birthNumber = birthNumber;
    }

    public Integer getFetusNumber() {
        return fetusNumber;
    }

    public void setFetusNumber(Integer fetusNumber) {
        this.fetusNumber = fetusNumber;
    }

    public Integer getPregnancyWeeks() {
        return pregnancyWeeks;
    }

    public void setPregnancyWeeks(Integer pregnancyWeeks) {
        this.pregnancyWeeks = pregnancyWeeks;
    }

    public Integer getPregnancyDays() {
        return pregnancyDays;
    }

    public void setPregnancyDays(Integer pregnancyDays) {
        this.pregnancyDays = pregnancyDays;
    }

    public Double getUltrasoundNt() {
        return ultrasoundNt;
    }

    public void setUltrasoundNt(Double ultrasoundNt) {
        this.ultrasoundNt = ultrasoundNt;
    }

    public String getOtherDysplasia() {
        return otherDysplasia;
    }

    public void setOtherDysplasia(String otherDysplasia) {
        this.otherDysplasia = otherDysplasia;
    }

    public Boolean getHasKaryotypeAnalysis() {
        return hasKaryotypeAnalysis;
    }

    public void setHasKaryotypeAnalysis(Boolean hasKaryotypeAnalysis) {
        this.hasKaryotypeAnalysis = hasKaryotypeAnalysis;
    }

    public String getKaryotypeAnalysisDesc() {
        return karyotypeAnalysisDesc;
    }

    public void setKaryotypeAnalysisDesc(String karyotypeAnalysisDesc) {
        this.karyotypeAnalysisDesc = karyotypeAnalysisDesc;
    }

    public List<Sample> getSamples() {
        return samples;
    }

    public void setSamples(List<Sample> samples) {
        this.samples = samples;
    }
}
