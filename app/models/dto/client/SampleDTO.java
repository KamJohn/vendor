package models.dto.client;

import com.google.common.collect.Lists;
import models.client.IdModel;
import models.client.Mutation;
import models.client.SampleState;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-26 上午11:04
 * @description 样本信息
 */
public class SampleDTO extends IdModel {

    /**
     * 申请单id
     */
    public String requisitionId;


    public String name;

    /**
     * 样本编号
     */
    public String number;

    /**
     * 样本条码
     */
    public String barCode;

    /**
     * 样本类型
     */
    public String type;

    /**
     * 快递单号
     */
    public String trackingNumber;

    /**
     * 亲缘关系
     */
    public String relation;

    /**
     * 亲缘关系id
     */
    public String relationId;

    /**
     * 申请单样本批次
     */
    public Integer batchNumber;

    /**
     * 样本状态变更集合
     */
    public List<SampleState> states = Lists.newArrayList();

    /**
     * 突变类型
     */
    public List<Mutation> mutations = Lists.newArrayList();

    /**
     * RTPGD 核型分析结果
     */
    public String analysisResult;


    public String r280;

    public String r230;

    /**
     * 浓度
     */
    public String density;

    /**
     * 体积
     */
    public String volume;

    /**
     * 当前状态
     */
    public SampleState currentState;

    public String gender;

    /**
     * 活检日期
     */
    public String biopsyDate;

    /**
     * 扩增日期
     */
    public String amplificationDate;

}
