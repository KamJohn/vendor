package controllers;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import controllers.api.API;
import play.Play;

import java.time.Instant;
import java.util.Map;


public class Application extends API {

    private static final String baseUrl = Play.configuration.getProperty("application.baseUrl");
    static String version = "1.0";

    public static void index() {
        redirect(baseUrl + "/public/index.html");
    }

    public static void status() {
        Map<String, String> status = Maps.newHashMap();
        status.put("status", "ok");
        status.put("version", version);
        status.put("server_time", Instant.now().toString());
        renderJSON(JSON.toJSONString(status, true));
    }

    public static void apis() {
        redirect("/public-api/");
    }

    public static void limsApis() {
        redirect("/public-api/?url=/docs/api-v1-lims.yaml?t=" + System.currentTimeMillis());
    }

}
