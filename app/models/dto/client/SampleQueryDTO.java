package models.dto.client;

import com.alibaba.fastjson.annotation.JSONField;
import models.client.IdModel;

import java.util.Date;
import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-26 上午11:04
 * @description 样本信息
 */
public class SampleQueryDTO extends IdModel {

    /**
     * 批次号
     */
    public Integer batchNumber;


    /**
     * 快递单号
     */
    public String trackingNumber;


    public String projectId;

    /**
     * 送检实验室
     */
    public String laboratory;

    /**
     * 检测项目
     */
    public List<String> detectionProjects;

    /**
     * 女方检测项目
     */
    public List<String> femaleDetectionProjects;

    /**
     * 男方检测项目
     */
    public List<String> maleDetectionProjects;

    public String detectionModel;


    /**
     * 检测项目-实验室集合
     */
    public List<SubLabDTO> subLaboratory;

    /**
     * 样本列表集合
     */
    public List<SampleDTO> samples;

    /**
     * 预计生成报告时间
     */
    @JSONField(format = "yyyy-MM-dd")
    public Date expectedReportTime;

    public boolean hasReport = false;


}
