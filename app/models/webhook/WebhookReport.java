package models.webhook;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-14 下午4:27
 * @description
 */
public class WebhookReport {
    /**
     * 报告唯一标识
     */
    private String identity;

    /**
     * 报告名称
     */
    private String fileName;

    /**
     * 报告类型 group:组报告 data:数据报告
     */
    private String type;

    /**
     * 数据样本对应的条码
     */
    private String barCode;

    /**
     * lims订单号
     */
    private String orderCode;

    /**
     * 检测项目ID
     */
    private String projectId;


    private String showName;

    private String clientNumber;

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
        this.showName = showName;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }


    public WebhookReport() {
    }

}
