import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import controllers.v1.open.LimsApi;
import exceptions.LimsException;
import models.api.Jsonable;
import models.client.*;
import models.dto.client.RequisitionDTO;
import okhttp3.ResponseBody;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.junit.Test;
import play.test.UnitTest;
import retrofit2.Response;
import services.LimsService;
import utils.Logs;
import utils.MongoUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static consts.Consts.limsRetrofitBuilder;
import static consts.Consts.retrofit;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-09 下午6:46
 * @description
 */
public class LimsTest extends UnitTest {

    private MongoCollection hospital = MongoUtils.getHospitalCollection();
    private MongoCollection department = MongoUtils.getDepartmentCollection();
    private MongoCollection relation = MongoUtils.getRelationCollection();
    private MongoCollection sampleType = MongoUtils.getSampleTypeCollection();
    private MongoCollection sampleCollection = MongoUtils.getSampleCollection();
    private MongoCursor cursor;


    public List<String> hospitals = Lists.newArrayList(
            "江苏省人民医院",
            "安徽省立医院",
            "南京鼓楼医院",
            "安徽医科大学第一附属医院",
            "苏州大学附属第一医院",
            "浙江大学",
            "上海长征医院",
            "连云港妇幼保健院",
            "江西妇幼保健院",
            "苏州市立医院",
            "沈阳市妇婴医院(遗传科)",
            "沈阳市妇婴医院(生殖科)",
            "沈阳204医院",
            "吉林大学第一医院",
            "第四军医大学附属唐都医院",
            "沈阳东方医疗集团菁华医院",
            "郑州大学第三附属医院",
            "山东省立医院",
            "烟台毓璜顶医院",
            "济南军区总医院",
            "河南省人民医院（生殖医院）",
            "天津医科大学",
            "朝阳医院",
            "北京301医院",
            "爱普益检验所",
            "河北民营医院",
            "成都锦江妇幼保健院",
            "中山大学第一附属医院",
            "重庆妇幼保健院",
            "海南大学附属第一医院",
            "湖北十堰医院",
            "湘潭市中心医院",
            "中信湘雅生殖与遗传专科医院",
            "长沙高新医院",
            "柬埔寨皇家生殖与遗传医院",
            "沈阳盛京医院",
            "武汉民营医院",
            "苏州民营医院",
            "浙江邵逸夫医院",
            "浙江妇保医院",
            "浙江温州附二",
            "上海国妇婴",
            "广州民营医院",
            "甘肃西北妇幼",
            "重庆坤和达康生物科技有限公司",
            "北京嘉宝医学检验实验室有限公司",
            "第三军医大学附属第一医院（重庆西南医院）"
    );

    public List<String> departments = Lists.newArrayList(
            "生殖中心",
            "遗传门诊",
            "检验科",
            "产前诊断中心",
            "新生儿科",
            "生殖男科",
            "妇科/妇产科",
            "PGD中心",
            "遗传科",
            "PGS（芯片组）",
            "北京临检",
            "分子实验室",
            "分子遗传室",
            "辅助生殖科",
            "泌尿外科",
            "生育技术科",
            "生殖科",
            "生殖内分泌科",
            "生殖遗传科",
            "生殖女科",
            "遗传实验室",
            "遗传中心"
    );

    public List<String> relations = Lists.newArrayList(
            "女方",
            "男方",
            "患儿",
            "女儿",
            "儿子",
            "女方父亲",
            "女方母亲",
            "男方父亲",
            "男方母亲",
            "女方兄弟",
            "女方姐妹",
            "男方兄弟",
            "男方姐妹",
            "女方侄子",
            "女方侄女",
            "男方侄子",
            "男方侄女",
            "女方外甥",
            "女方外甥女",
            "男方外甥",
            "男方外甥女",
            "子代胚胎"
    );

    public List<String> sampleTypes = Lists.newArrayList(
            "外周血",
            "gDNA",
            "流产组织",
            "唾液",
            "MDA扩增产物",
            "Surplex扩增产物",
            "绒毛",
            "羊水",
            "脐带血",
            "细胞",
            "文库"
    );


    @Test
    public void saveOrganizations() {

        List<Hospital> hospitalList = Lists.newArrayList();
        hospitals.forEach(s -> {
            Hospital hospital = new Hospital();
            hospital.name = s;
            hospitalList.add(hospital);
        });
        hospital.insert(hospitalList.toArray());

        List<Department> departmentList = Lists.newArrayList();
        departments.forEach(s -> {
            Department department = new Department();
            department.name = s;
            departmentList.add(department);
        });
        department.insert(departmentList.toArray());
    }

    @Test
    public void saveRelations() {

        List<Relation> relationList = Lists.newArrayList();
        relations.forEach(s -> {
            Relation relation = new Relation();
            relation.name = s;
            relationList.add(relation);
        });
        relation.insert(relationList.toArray());
    }

    @Test
    public void saveProjects() {
        Map<String, String> map = Maps.newHashMap();
        map.put("JC0001", "PGS");
        map.put("JC0002", "流产组织学分析");
        map.put("JC0004", "孕前基因");
        map.put("JC0006", "AZF");
        map.put("JC0007", "单基因病PGD总项目");
        map.put("JC0008", "罗氏易位PGD");
        map.put("JC0010", "PGM平台/");

    }

    @Test
    public void saveSampleTypes() {
        Map<String, String> map = Maps.newHashMap();
        map.put("C1705170001", "外周血");
        map.put("C1705170002", "gDNA");
        map.put("C1705170003", "流产组织");
        map.put("C1705170004", "唾液");
        map.put("C1705170005", "MDA扩增产物");
        map.put("C1705170006", "Surplex扩增产物");
        map.put("C1705170007", "绒毛");
        map.put("C1705170008", "羊水");
        map.put("C1705170009", "脐带血");
        map.put("C1705170010", "细胞");
        map.put("C", "文库");
        List<SampleType> sampleTypes = Lists.newArrayList();
        map.entrySet().forEach(entry -> {
            SampleType sampleType = new SampleType(entry.getKey(), entry.getValue());
            sampleTypes.add(sampleType);
        });
        sampleType.insert(sampleTypes.toArray());
    }

    @Test
    public void saveSampleStatus() {


    }

    @Test
    public void testGetReport() throws IOException, LimsException {
        String fileName = "MCL__171018__001_PGD-mdpgd";// SO__170531__001_JCA0002  SO__170531__001_JCA0002
        retrofit = limsRetrofitBuilder
                .build();
        Response<ResponseBody> limsResponse = retrofit
                .create(LimsService.class)
                .getReport(fileName)
                .execute();
        InputStream inputStream = limsResponse.body().byteStream();
        File file = new File("reports/" + ObjectId.get().toHexString());
        FileUtils.copyInputStreamToFile(inputStream, file);
        if (limsResponse.code() != 200) {
            Logs.error("failed to get report from lims:{}", limsResponse.errorBody().string());
            throw new LimsException(limsResponse.errorBody().string(), limsResponse.code());
        }
    }

    @Test
    public void testNotifyLims() throws Exception {
        retrofit = limsRetrofitBuilder
                .build();
        Response<ResponseBody> response = retrofit.create(LimsService.class)
                .notifyLims("drr-2017060917020745a9867e1f3")
                .execute();
        Logs.info("response code :{},response message:{}", response.code(), response.body().string());
        Logs.info("notify lims requisition success");
        if (!response.isSuccessful()) {
            throw new Exception(response.message());
        }

    }

    @Test
    public void testLogin() throws IOException {
        //医生用户：C0003    12345678  刘医生     yu.zheng   12345678   郑羽
        String id = "C0003";
        String password = "12345678";
        boolean success = false;
        try {
            success = LimsApi.verify(id, password);
        } catch (LimsException e) {
            Logs.error("Lims Server error:{}", e.getMessage());
        }
        Logs.info("login result-->{}", success);

    }

    @Test
    public void testGetUserInfo() throws IOException, LimsException {
        String id = "C0003";
        String password = "12345678";
        UserInfo userInfo = LimsApi.getUserInfo(id, password);
        Logs.info("user info string-->{}", JSON.toJSONString(userInfo));
    }

    @Test
    public void testSubmitRequisition() throws IOException, LimsException {
        Requisition requisition = new Requisition();
        requisition.id = ObjectId.get().toString();
        requisition.clientNumber = RandomStringUtils.randomAlphanumeric(10);
        requisition.marker = UUID.randomUUID().toString().replaceAll("-", "");
        Map data = Maps.newHashMap();
        data.put("organization", "江苏省人民医院");
        data.put("department", "检验科");
        data.put("physician", "张医生");
        data.put("phone", "13000000000");
        data.put("email", "1234@test.com");
        data.put("maleName", "Lilith");
        data.put("maleAge", 18);
        data.put("maleResult", "女方核型结果");
        data.put("femaleName", "John");
        data.put("femaleResult", "男方核型结果");
        data.put("sendDate", DateTime.now().toString("yyyy-MM-dd"));
        data.put("sampleType", "MDA扩增产物");
        data.put("conclusion", "指征描述");
        List<Sample> samples = Lists.newArrayList();
        IntStream.range(0, 6).forEach(i -> {
            Sample sample = new Sample();
            sample.barCode = RandomStringUtils.randomAlphanumeric(8);
            sample.requisitionId = requisition.id;
            sample.number = (RandomStringUtils.randomAlphanumeric(6));
            samples.add(sample);
        });
        requisition.samples = samples;
        requisition.data = data;
        Logs.info(Jsonable.toPrettyJson(requisition));
        LimsApi.submitRequisition(requisition);
    }

    @Test
    public void testSubmitMDPGD() throws IOException, LimsException {
        Requisition requisition = new Requisition();
        requisition.clientNumber = RandomStringUtils.randomAlphanumeric(10);
        requisition.id = new ObjectId().toString();
        requisition.project = "单基因病S-PGD™检测";
        requisition.projectId = "JCA0006";
        Map data = Maps.newHashMap();
        data.put("phone", "13000000000");
        data.put("organization", "中信湘雅");
        data.put("department", "检验科");
        data.put("physician", "张医生");
        data.put("email", "111@123.com");
        data.put("clinicNumber", "10010");
        data.put("clinicalManifestations", "https://devvendor.pgxcloud.com/v1/images/5acd0f2f42bf4c6cbfce0703c0893f87.jpeg");
        data.put("familyImage", "https://devvendor.pgxcloud.com/v1/images/5acd0f2f42bf4c6cbfce0703c0893f87.jpeg");
        data.put("femaleName", "张三");
        data.put("maleName", "李四");
        data.put("femaleNation", "汉族");
        data.put("maleNation", "汉族");
        data.put("maleAge", "男方年龄");
        data.put("femaleAge", "女方年龄");
        data.put("samplingDate", "2017-06-27");
        data.put("femaleBirthplace", "北京市朝阳区");
        data.put("maleBirthplace", "北京市朝阳区");
        data.put("probandName", "先证者");
        data.put("probandAge", 24);
        data.put("probandNation", "汉族");
        data.put("probandBirthplace", "北京市朝阳区");
        data.put("detectionProjects", Lists.newArrayList("JCA0005", "JCA0016"));
        data.put("diseases", Lists.newArrayList(new Disease("甲型血友病", "F8"), new Disease("杜氏肌营养不良", "DMD")));
        data.put("selfGeneticDisease", true);
        data.put("selfGeneticDiseaseDesc", "本人遗传病史描述");
        data.put("relativesGeneticDisease", true);
        data.put("relativesGeneticDiseaseDesc", "亲属遗传病史描述");
        data.put("exogenousTransfused", true);
        data.put("stemCellTransplanted", true);
        data.put("contactName", "联系人1");
        data.put("contactPhone", "18888888888");
        data.put("contactAddress", "北京市朝阳区");
        data.put("contactDetlAddress", "望京街道1号院");
        data.put("laboratory", "1号实验室");
        data.put("sendDate", "2017-07-13");
        data.put("pathogenicGenes", "突变基因");
        data.put("sampleType", Lists.newArrayList("细胞", "MDA扩增产物"));
        List<Sample> samples = Lists.newArrayList();
        IntStream.range(0, 5).forEach(i -> {
            Sample sample = new Sample();
            sample.barCode = RandomStringUtils.randomAlphanumeric(8);
            sample.requisitionId = requisition.id;
            sample.number = RandomStringUtils.randomAlphanumeric(6);
            sample.relation = relations.get(ThreadLocalRandom.current().nextInt(0, 15));
            sample.type = sampleTypes.get(ThreadLocalRandom.current().nextInt(0, 11));
            sample.mutations = Lists.newArrayList();
            samples.add(sample);
        });
        requisition.samples = samples;
        requisition.data = data;
        LimsApi.submitRequisition(requisition);
    }

    @Test
    public void testSubmitGDNA() throws IOException, LimsException {
        Requisition requisition = new Requisition();
        requisition.clientNumber = RandomStringUtils.randomAlphanumeric(10);
        requisition.id = ObjectId.get().toString();
        requisition.project = "流产组织学分析";
        requisition.projectId = "JCA0014";
        Map data = Maps.newHashMap();
        data.put("maleName", "张先生");
        data.put("maleAge", 29);
        data.put("maleResult", "男性核型分析结果");
        data.put("femaleName", "王女士");
        data.put("femaleResult", "女性核型分析结果");
        data.put("femaleAge", 25);
        data.put("selfGeneticDisease", true);
        data.put("selfGeneticDiseaseDesc", "本人遗传病史描述");
        data.put("relativesGeneticDisease", true);
        data.put("relativesGeneticDiseaseDesc", "亲属遗传病史描述");
        data.put("intermarry", true);
        data.put("hasChildren", true);
        data.put("childrenCount", 3);
        data.put("boysCount", 2);
        data.put("girlsCount", 1);
        data.put("childrenHealthy", true);
        data.put("childrenDiagnosis", "不健康子女诊断");
        data.put("hasAbortion", true);
        data.put("pregnancyNumber", 2);
        data.put("birthNumber", 4);
        data.put("fetusNumber", 2);
        data.put("pregnancyWeeks", 3);
        data.put("pregnancyDays", 2);
        data.put("ultrasoundNt", 0.435454D);
        data.put("otherDysplasia", "其他发育异常");
        data.put("hasKaryotypeAnalysis", true);
        data.put("karyotypeAnalysisDesc", "染色体核型分析");
        data.put("sampleType", Lists.newArrayList("流产组织", "gDNA", "文库"));
        data.put("organization", "江苏省人民医院");
        data.put("department", "遗传门诊");
        data.put("laboratory", "2号实验室");
        data.put("physician", "张医生");
        data.put("phone", "13000000000");
        data.put("email", "123@abc.com");
        data.put("sendDate", "2017-07-15");
        List<Sample> samples = Lists.newArrayList();
        IntStream.range(0, 10).forEach(i -> {
            Sample sample = new Sample();
            sample.barCode = RandomStringUtils.randomAlphanumeric(8);
            sample.requisitionId = requisition.id;
            sample.number = RandomStringUtils.randomAlphanumeric(6);
            sample.relation = relations.get(ThreadLocalRandom.current().nextInt(0, 15));
            sample.type = sampleTypes.get(ThreadLocalRandom.current().nextInt(0, 11));
            samples.add(sample);
        });
        requisition.samples = samples;
        requisition.data = data;
        LimsApi.submitRequisition(requisition);
    }

    @Test
    public void testSubmitRTPGD() throws IOException, LimsException {
        Requisition requisition = new Requisition();
        requisition.clientNumber = RandomStringUtils.randomAlphanumeric(10);
        requisition.id = ObjectId.get().toString();
        Map data = Maps.newHashMap();
        data.put("maleName", "Jim");
        data.put("femaleName", "Sara");
        data.put("sampleType", Lists.newArrayList("Surplex扩增产物", "gDNA", "PGS文库"));
        data.put("organization", "江苏省人民医院");
        data.put("department", "遗传门诊");
        data.put("sendDate", "2017-07-15");
        data.put("clinicNumber", "010");
        data.put("email", "aaa@111.com");
        data.put("phone", "13012345678");
        List<Sample> samples = Lists.newArrayList();
        IntStream.range(0, 10).forEach(i -> {
            Sample sample = new Sample();
            sample.barCode = RandomStringUtils.randomAlphanumeric(8);
            sample.requisitionId = requisition.id;
            sample.number = RandomStringUtils.randomAlphanumeric(6);
            sample.relation = "女方";
            sample.type = "Surplex扩增产物";
            sample.analysisResult = "核型分析结果";
            samples.add(sample);
        });
        requisition.samples = samples;
        requisition.data = data;
        Logs.info(JSON.toJSONString(requisition, SerializerFeature.PrettyFormat, SerializerFeature.WriteNullNumberAsZero, SerializerFeature.WriteNullStringAsEmpty));
        LimsApi.submitRequisition(requisition);
    }

    @Test
    public void testSubmitAZF() throws IOException, LimsException {
        Requisition requisition = new Requisition();
        requisition.clientNumber = RandomStringUtils.randomAlphanumeric(10);
        requisition.id = ObjectId.get().toString();
        Map data = Maps.newHashMap();
        data.put("name", "Gray");
        data.put("age", 25);
        data.put("clinicNumber", "010");
        data.put("samplingDate", "2017-07-14");
        data.put("hasChildren", true);
        data.put("childrenCount", 2);
        data.put("infertilityMonths", 3);
        data.put("hasAbortion", true);
        data.put("abortionNumber", 2);
        data.put("hasCryptorchidism", true);
        data.put("hasVaricocele", true);
        data.put("vasDeferensAbsent", true);
        data.put("sexualFunctionNormal", true);
        data.put("sexualOrgansNormal", true);
        data.put("testisUltrasound", "睾丸B超");
        data.put("semenVolume", 100D);
        data.put("liquefactionState", "333");
        data.put("semenPh", 6.5);
        data.put("viscosity", "粘稠度");
        data.put("spermAgglutination", "精子凝集");
        data.put("spermNumber", 100D);
        data.put("abnormalSpermRate", 0.3455D);
        data.put("immatureSpermRate", 35D);
        data.put("spermMotilityRate", 28D);
        data.put("follicleEstrogen", "促卵泡雌激素");
        data.put("luteinizingHormone", "促黄体生成素");
        data.put("estradiol", "雌二醇");
        data.put("spermDnaAnalysis", "精子DNA完整性分析结果");
        data.put("sampleType", Lists.newArrayList("gDNA", "外周血"));

        data.put("organization", "江苏省人民医院");
        data.put("laboratory", "3号实验室");
        data.put("department", "遗传门诊");
        data.put("email", "aaa@111.com");
        data.put("phone", "13012345678");

        List<Sample> samples = Lists.newArrayList();
        IntStream.range(0, 5).forEach(i -> {
            Sample sample = new Sample();
            sample.barCode = RandomStringUtils.randomAlphanumeric(8);
            sample.requisitionId = requisition.id;
            sample.number = RandomStringUtils.randomAlphanumeric(6);
            sample.relation = relations.get(ThreadLocalRandom.current().nextInt(0, 15));
            sample.type = sampleTypes.get(ThreadLocalRandom.current().nextInt(0, 11));
            samples.add(sample);
        });
        requisition.samples = samples;
        LimsApi.submitRequisition(requisition);
    }

    @Test
    public void testSampleDelivery() throws IOException, LimsException {
        /*SampleDelivery sampleDelivery = new SampleDelivery();
        sampleDelivery.limsNumber = "MCL__170713__020";
        sampleDelivery.trackingNumber = "1111111111111";
        List<Sample> samples = Lists.newArrayList();
        IntStream.range(0, 8).forEach(i -> {
            Sample sample = new Sample();
            sample.barCode = RandomStringUtils.randomAlphanumeric(8);
            sample.number = RandomStringUtils.randomAlphanumeric(6);
            sample.relation = "男方";
            sample.type = "gDNA";
            samples.add(sample);
        });
        sampleDelivery.samples = samples;
        Logs.info(JSON.toJSONString(sampleDelivery, SerializerFeature.PrettyFormat, SerializerFeature.WriteNullStringAsEmpty));
        LimsApi.notifySampleDelivery(sampleDelivery);*/
        RequisitionDTO requisitionDTO = new RequisitionDTO();
        requisitionDTO.trackingNumber = "569239875102098";
        String id = "598ad8638cdc3704e725de54";
        Request.Put("http://localhost:9010/v1/requisition/" + id + "/tracking")
                .bodyString(JSON.toJSONString(requisitionDTO), ContentType.APPLICATION_JSON)
                .execute();

    }

    @Test
    public void testQuerySampleStates() throws IOException, LimsException {
        List<SampleStateList> requisitionState = LimsApi.querySampleStates("MCL__180113__001");
        Logs.info(JSON.toJSONString(requisitionState, SerializerFeature.PrettyFormat));
    }

    @Test
    public void testUrl() throws MalformedURLException {
        URL url = new URL("http", "127.0.0.1", 9010, "/v1/images/WechatIMG27.jpeg");
        URI uri = new File("images/WechatIMG27.jpeg").toURI();
        Logs.info(url.toString());
        Logs.info(uri.toString());
    }

    @Test
    public void testNotifyPGSResult() throws Exception {
        org.apache.http.client.fluent.Response response = Request.Post("http://localhost:9010/v1/open/pgs/results/d724e027579f4c5ea82253bb492ca6a9")
                .execute();
        Logs.info("status code -->{}", String.valueOf(response.returnResponse().getStatusLine().getStatusCode()));
    }

    @Test
    public void testNotifyPGDResult() throws IOException {
        org.apache.http.client.fluent.Response response = Request.Post("http://localhost:9010/v1/open/pgd/results/532704e420414317821b62416f3e6e28")
                .execute();
        Logs.info("status code -->{}", String.valueOf(response.returnResponse().getStatusLine().getStatusCode()));
    }

    @Test
    public void testJson() throws IOException {
        Logs.info(JSON.toJSONString(LimsApi.getFullFormData("JCA0001")));
    }

    @Test
    public void testGetRelations() throws IOException, LimsException {
        Logs.info(JSON.toJSONString(LimsApi.getRelations(), true));
    }

    @Test
    public void mockSampleStates() {
        String json = "[\n" +
                "\t{\n" +
                "\t\t\"barCode\":\"GWz643uz\",\n" +
                "\t\t\"sampleStates\":[\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"endDate\":\"2017-07-11 18:30:47\",\n" +
                "\t\t\t\t\"sampleState\":\"上机测序-已完成\"\n" +
                "\t\t\t,\"qualityNot\":\"质检失败\"" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"endDate\":\"2017-07-10 16:05:40\",\n" +
                "\t\t\t\t\"sampleState\":\"文库构建-已完成\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"endDate\":\"2017-07-10 16:02:35\",\n" +
                "\t\t\t\t\"sampleState\":\"分管平台\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"endDate\":\"2017-07-10 16:02:35\",\n" +
                "\t\t\t\t\"sampleState\":\"分管平台\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"endDate\":\"2017-07-10 16:02:35\",\n" +
                "\t\t\t\t\"sampleState\":\"分管平台\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"endDate\":\"2017-07-10 16:00:06\",\n" +
                "\t\t\t\t\"sampleState\":\"核酸提取/WGA-已完成\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"endDate\":\"2017-07-10 16:00:06\",\n" +
                "\t\t\t\t\"sampleState\":\"核酸提取/WGA-已完成\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"endDate\":\"2017-07-10 14:57:03\",\n" +
                "\t\t\t\t\"sampleState\":\"样本接收-已到实验并接收完成\"\n" +
                "\t\t\t},\n" +
                "\t\t\t{\n" +
                "\t\t\t\t\"endDate\":\"2017-07-10 14:46:50\",\n" +
                "\t\t\t\t\"sampleState\":\"样本接收-已到实验并接收\"\n" +
                "\t\t\t}\n" +
                "\t\t]\n" +
                "\t}\n" +
                "]";

        List<SampleStateList> requisitionState = JSONArray.parseArray(json, SampleStateList.class);
        cursor = sampleCollection.find("{barCode:#}", "GWz643uz").as(Sample.class);
        List<Sample> samples = Lists.newArrayList(cursor.iterator());
        List<SampleStateList> sampleStateList = requisitionState;
        ListMultimap<String, List<SampleState>> multimap = ArrayListMultimap.create();
        if (!sampleStateList.isEmpty()) {
            sampleStateList.forEach(sampleState -> multimap.put(sampleState.barCode, sampleState.sampleStates));
        }
        samples.forEach(sample -> {
            Logs.info("Updating sample states.Sample Id:{}.Original states:{}, New states:{}", sample.id, JSON.toJSONString(sample.states), JSON.toJSONString(multimap.get(sample.barCode)));
            sample.states = multimap.get(sample.barCode);
            sampleCollection.save(sample);
        });

    }


}
