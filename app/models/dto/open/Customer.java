package models.dto.open;


/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-05 下午5:25
 * @description
 */
public class Customer {
    public String barCode;
    public String name;
    public String gender;
    public String birthday;
    public String ethnic;
    /**
     * 地址编码
     */
    public String birthplace;
    /**
     * 完整地址
     */
    public String birthplaceFull;
    public String collectionDate;
    public String sampleType;
    public String geneticDiseaseSelf;
    public String geneticDiseaseSelfDesc;
    public String geneticDiseaseParent;
    public String geneticDiseaseParentDesc;
    public String intermarry;
    public String hadChildren;
    public Integer childrenCount;
    public String childrenGender;
    public String childrenHealthy;
    public String childrenDiagnosisDesc;
    public String abortion;
    public String diagnosisType;
    public Integer pregnancyNumber;
    public Integer birthNumber;
    public String gestationalWeeks;
    public String lastMensesDate;
    public String ultrasoundNt;
    public String otherDysplasia;
    public String karyotypeAnalysis;
    public String karyotypeAnalysisDesc;
    public String healthy;
    public String diagnosisDesc;
}
