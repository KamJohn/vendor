package utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import models.api.Jsonable;

/**
 * @author zanxus
 * @version ${VERSION}
 * @date 2017/7/17 上午10:42
 */
public interface JSONFormatter extends Jsonable {

    static String toNullFormatPrettyJson(Object o) {
        return JSON.toJSONString(o, SerializerFeature.PrettyFormat, SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteNullNumberAsZero);
    }
}
