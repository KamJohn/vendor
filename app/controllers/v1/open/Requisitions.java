package controllers.v1.open;

import controllers.api.API;
import interceptors.Signature;
import models.DrrRequisition;
import okhttp3.MediaType;
import play.data.validation.Required;
import play.mvc.With;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;
import services.DrrService;
import utils.Logs;

import java.io.IOException;

import static consts.Consts.drrRetrofitBuilder;
import static consts.Consts.retrofit;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-04-27 上午10:55
 * @description 对lims接口申请单
 */
@With({Signature.class})
public class Requisitions extends API {

    /**
     * 获取drr申请单信息
     */
    public static void getDrrRequisition(@Required String orderCode) throws IOException {
        Logs.info("received lims requisition get request.orderCode:{}", orderCode);
        retrofit = drrRetrofitBuilder
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Response<DrrRequisition> apiResponse = retrofit.create(DrrService.class)
                .getRequisition(orderCode)
                .execute();
        DrrRequisition drrRequisition;
        if (apiResponse.code() == 200) {
            drrRequisition = apiResponse.body();
            if (drrRequisition != null) {
                renderJSON(drrRequisition);
            }
        } else {
            response.status = apiResponse.code();
            response.out.write((apiResponse.errorBody().create(MediaType.parse("application/json"), "drr-" + apiResponse.errorBody().string())).bytes());
        }
    }


}
