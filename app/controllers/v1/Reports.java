package controllers.v1;

import com.google.common.collect.Lists;
import com.mongodb.QueryBuilder;
import com.mongodb.WriteResult;
import com.qingstor.sdk.exception.QSException;
import controllers.api.API;
import controllers.api.interceptor.Secure;
import controllers.api.validation.MongoId;
import models.client.Report;
import models.dto.client.RequisitionDTO;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import play.data.validation.Required;
import play.libs.MimeTypes;
import play.mvc.With;
import utils.*;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import static consts.ClientConsts.CLIENT_REPORT_DIR;

/**
 * @author cerpengxi
 * @date 17/7/6 下午4:44
 */
@With({Secure.class})
public class Reports extends API {
    private static MongoCollection requisitionCollection;

    private static MongoCollection sampleCollection;

    private static MongoCollection reportCollection;

    private static MongoCursor cursor;


    private static final String DOWNLOAD_TMP_DIR = "downloads/";

    static {
        requisitionCollection = MongoUtils.getRequisitionCollection();
        sampleCollection = MongoUtils.getSampleCollection();
        reportCollection = MongoUtils.getReportCollection();
    }

    /**
     * 报告列表
     *
     * @param limit
     * @param offset
     * @param projectIds
     * @param keywords
     * @param start
     * @param end
     */
    public static void query(int limit, int offset, String projectIds, String keywords, String start, String end) {
        limit = Pages.safeLimit(limit);
        offset = Pages.safeOffset(offset);
        String userId = currentUserId();
        QueryBuilder queryBuilder = Samples.queryBuilder(userId, projectIds, keywords, start, end);
        MongoCursor<RequisitionDTO> requisitionMongoCursor = requisitionCollection.find("#", queryBuilder.and("editable").is(false).get()).limit(limit).skip(offset).sort("{updateAt: -1}").as(RequisitionDTO.class);
        List<RequisitionDTO> requisitionDTOS = Lists.newArrayList(requisitionMongoCursor.iterator());
        if (requisitionDTOS.isEmpty()) {
            renderEmptyList();
        }
        setTotalCount(requisitionCollection.count("#", queryBuilder.and("editable").is(false).get()));
        for (RequisitionDTO requisitionDTO : requisitionDTOS) {
            List<String> customers = Lists.newArrayList(String.valueOf(requisitionDTO.data.get("name")), String.valueOf(requisitionDTO.data.get("femaleName")), String.valueOf(requisitionDTO.data.get("maleName")));
            customers.removeAll(Collections.singleton("null"));
            String customer = StringUtils.join(customers.toArray(), ",");
            Object detectionModel = requisitionDTO.data.get("detectionModel");
            if (detectionModel != null) {
                requisitionDTO.detectionModel = String.valueOf(detectionModel);
            }
            requisitionDTO.customers = customer;
            requisitionDTO.organization = String.valueOf(requisitionDTO.data.get("organization"));
            if (StringUtils.isNullOrEmpty(requisitionDTO.report) || StringUtils.isNotNullOrEmpty(requisitionDTO.preTestReport)) {
                requisitionDTO.hasReport = true;
            }
        }
        renderJSON(requisitionDTOS);
    }

    /**
     * 获取单个申请单的报告列表
     *
     * @param requisitionId 申请单id
     */
    public static void get(@Required @MongoId String requisitionId) {
        String userId = currentUserId();
        cursor = reportCollection.find("{requisitionId:#,userId:#}", requisitionId, userId).as(Report.class);
        WriteResult writeResult = requisitionCollection.update(new ObjectId(requisitionId)).with("{$set:{showTips:#}}", false);//取消提示
        if (!writeResult.isUpdateOfExisting()) {
            Logs.warn("申请单中数据不存在, requisitionId :{}", requisitionId);
        }
        List<Report> reports = Lists.newArrayList(cursor.iterator());
        renderJSON(reports);
    }


    /**
     * 查看报告
     *
     * @param id 报告id
     */
    public static void view(@Required String id) {
        String userId = currentUserId();
        Report report = reportCollection.findOne("{_id:#,userId:#}", new ObjectId(id), userId).as(Report.class);
        if (report != null) {
            String fileName = report.name;
            try {
                InputStream inputStream = QingstorUtils.getObject(CLIENT_REPORT_DIR + fileName);
                renderBinary(inputStream, fileName, MimeTypes.getContentType(fileName), true);
            } catch (QSException e) {
                Logs.error("get report error! path:{}", fileName, e);
            }
        }
        badRequest("Report " + id + " not found");
    }


}
