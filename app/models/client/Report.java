package models.client;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-11 上午10:43
 * @description
 */
public class Report extends BaseModel {

    /**
     * 关联的申请单id
     */
    public String requisitionId;

    /**
     * 关联的样本条码（如果是组报告该字段为空）
     */
    public String barCode;

    /**
     * 样本编号
     */
    public String sampleNumber;

    public String projectId;

    /**
     * lims报告唯一标识
     */
    public String identity;

    /**
     * 报告名称
     */
    public String name;

    /**
     * 报告原始文件名
     */
    public String originalName;

    public String showName;


    /**
     * 报告在服务器存储目录
     */
    public String path;

    /**
     * 报告检测项目
     */
    public String detectionProject;

    /**
     * 报告类型 组报告/数据报告
     */
    public String type;

    public String userId;

    public String clientNumber;

}
