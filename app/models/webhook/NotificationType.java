package models.webhook;

/**
 * @author zanxus
 * @version ${VERSION}
 * @date 2017/4/28 下午3:10
 */
public enum NotificationType {

    REQUISITION("requisition", "申请单通知"), REPORT_GENERATED("report_generated", "报告通知"), SAMPLE_RECEIVED("sample_received", "收样通知"), SAMPLE_STATE_CHANGED("sample_state_changed", "样本状态变更通知");

    private String value;
    private String desc;

    NotificationType(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String value() {
        return value;
    }

    public String desc() {
        return desc;
    }
}
