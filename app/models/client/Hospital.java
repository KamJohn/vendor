package models.client;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-05 下午5:20
 * @description
 */
public class Hospital extends IdModel {

    /**
     * lims医院id
     */
    public String limsId;

    /**
     * 医院名称
     */
    public String name;

}
