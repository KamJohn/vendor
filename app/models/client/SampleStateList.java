package models.client;

import java.util.List;

/**
 * @author zanxus
 * @date 2017-07-07 下午3:17
 * @description
 */
public class SampleStateList {

    public String barCode;
    public List<SampleState> sampleStates;
}
