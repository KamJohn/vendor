package enums;

import org.springframework.http.HttpStatus;

/**
 * @desc: 返回结果枚举类
 * @createTime: 2018/11/22 17:00 @author: Kam
 */
public enum ResultEnum {

    /**
     * 接口请求成功状态码和提示
     */
    SUCCESS(0, "成功"),

    /**
     * 接口请求失败状态码和提示
     */
    FAIL(1, "失败"),

    /**
     * 无效操作
     */
    INVALID_OPERATION(103, "无效操作"),

    /**
     * 参数无效 100
     */
    PARAM_ERROR(100, "参数无效,请检查"),

    /**
     * 服务器异常 500
     */
    SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "服务器异常,请稍后重试"),

    /**
     * 请求方式不正确 405
     */
    METHOD_NOT_ALLOWED(HttpStatus.METHOD_NOT_ALLOWED.value(), "请求方式无效,请检查"),

    /**
     * 请求方式不正确 400
     */
    BAD_REQUEST(HttpStatus.BAD_REQUEST.value(), "无效请求,请重试"),

    /**
     * 接口地址不存在 404
     */
    NOT_FONUD(HttpStatus.NOT_FOUND.value(), "请求地址无效,请检查"),

    /**
     * 服务器异常 412
     */
    REQUEST_FAILED(HttpStatus.PRECONDITION_FAILED.value(), "请求失败,请检查参数"),

    /**
     * 服务器异常 413
     */
    USER_INVALID(413, "用户已被禁用"),

    /**
     * 用户未授权 415
     */
    USER_UNAUTHORIZED(415, "用户未授权");


    public int code;
    public String msg;

    ResultEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}