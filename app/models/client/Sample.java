package models.client;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-26 上午11:04
 * @description 样本信息
 */
public class Sample extends BaseModel {

    /**
     * 申请单id
     */
    public String requisitionId;

    public String name;

    /**
     * 样本编号
     */
    public String number;

    /**
     * 样本条码
     */
    public String barCode;

    /**
     * 样本类型
     */
    public String type;

    public String r280;

    public String r230;

    /**
     * 浓度
     */
    public String density;

    /**
     * 体积
     */
    public String volume;

    /**
     * 快递单号
     */
    public String trackingNumber;

    /**
     * 亲缘关系
     */
    public String relation;

    /**
     * 亲缘关系id
     */
    public String relationId;

    /**
     * 申请单样本批次
     */
    public Integer batchNumber;

    /**
     * 样本状态变更集合 存储为list中的list是因为同一个样本可能在实验过程中分裂为多个样本
     * 因此可能有多个状态集合
     */
    public List<List<SampleState>> states = Lists.newArrayList();

    /**
     * MDPGD 突变类型
     */
    public List<Mutation> mutations = Lists.newArrayList();

    /**
     * RTPGD 核型分析结果
     */
    public String analysisResult;

    /**
     * drr样本性别
     */
    public String gender;

    public String marker;

    /**
     * 送检实验室
     */
    public String laboratory;

    /**
     * 活检日期
     */
    public String biopsyDate;

    /**
     * 扩增日期
     */
    public String amplificationDate;

    /**
     * 检测项目
     */
    public List<String> detectionProjects;

    /**
     * 检测项目
     */
    public List<String> maleDetectionProjects;

    /**
     * 检测项目
     */
    public List<String> femaleDetectionProjects;

    /**
     * 检测项目-实验室集合
     */
    public List<Map<String, String>> subLaboratory;

}
