package models.dto.client;

import play.data.validation.Required;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2018-03-14 11:44
 * @description
 */
public class BatchTrackingNumber {

    @Required
    public String id;

    @Required
    public String trackingNumber;

}
