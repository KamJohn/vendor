package models.client;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.collect.Lists;
import play.data.validation.Required;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-26 下午3:13
 * @description 罗氏易位PGD申请单 （Robertsonian translocation）
 */
public class RTPGDRequisition extends PGXRequisition {

    /**
     * 受检者(男)
     */
    @Required
    private String maleName;

    /**
     * 男方年龄
     */
    private Integer maleAge;


    /**
     * 受检者(女)
     */
    @Required
    private String femaleName;


    /**
     * 女方年龄
     */
    private Integer femaleAge;


    /**
     * 住院/门诊号
     */
    private String clinicNumber;

    /**
     * 送检日期
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String sendDate;

    /**
     * 样本类型  外周血/gDNA/MDA扩增产物/Sureplex扩增产物/细胞
     */
    @Required
    private List<String> sampleType;

    /**
     * 样本信息集合
     */
    private List<Sample> samples = Lists.newArrayList();

    public String getMaleName() {
        return maleName;
    }

    public void setMaleName(String maleName) {
        this.maleName = maleName;
    }

    public Integer getMaleAge() {
        return maleAge;
    }

    public void setMaleAge(Integer maleAge) {
        this.maleAge = maleAge;
    }

    public String getFemaleName() {
        return femaleName;
    }

    public void setFemaleName(String femaleName) {
        this.femaleName = femaleName;
    }

    public Integer getFemaleAge() {
        return femaleAge;
    }

    public void setFemaleAge(Integer femaleAge) {
        this.femaleAge = femaleAge;
    }

    public String getClinicNumber() {
        return clinicNumber;
    }

    public void setClinicNumber(String clinicNumber) {
        this.clinicNumber = clinicNumber;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public List<String> getSampleType() {
        return sampleType;
    }

    public void setSampleType(List<String> sampleType) {
        this.sampleType = sampleType;
    }

    public List<Sample> getSamples() {
        return samples;
    }

    public void setSamples(List<Sample> samples) {
        this.samples = samples;
    }
}
