import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import play.test.FunctionalTest;
import utils.Logs;
import utils.SignatureUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-04-27 下午6:47
 * @description
 */
public class SignatureTest extends FunctionalTest {

    public static final String SECRET_KEY = "15377b5f37dc0b2e9a66bafcd5e06584bcf71678b";
    public static final String ACCESS_KEY_ID = "1P3KM8z9MJQ6KHtIvpmA4pA==";
    public static final String CLIENT_ID = "590ac658b4a4cb6f4097abd8";

    public void reportNotify() throws Exception {
        //query string
        Map<String, String> params = new HashMap<>();
        params.put("access_key_id", ACCESS_KEY_ID);
        params.put("signature_version", "1");
        //path
        String accountNo = "1";
        String path = "/v1/results/59007242f8e2ab14d49449bd";
        //sign
        String signature = SignatureUtil.computeSignature(SECRET_KEY, "POST", path, params);
        params.put("signature", signature);

        String queryString = SignatureUtil.map2QueryString(params);
        String finalUrl = path + "?" + queryString;

        Logs.get().info("最终URL:{}", finalUrl);
        Response response = Request.Post(finalUrl).bodyString("", ContentType.APPLICATION_JSON).execute();
        System.out.println(response.returnContent().asString());
    }
}
