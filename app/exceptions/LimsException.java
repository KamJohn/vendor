package exceptions;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-13 下午5:26
 * @description
 */
public class LimsException extends Exception {

    private int code;

    public LimsException(String message, int code) {
        super(message);
        this.code = code;
    }

    public LimsException(String message, Throwable cause, int code) {
        super(message, cause);
        this.code = code;
    }

    public LimsException() {
    }

    public LimsException(String message) {
        super(message);
    }

    public LimsException(String message, Throwable cause) {
        super(message, cause);
    }

    public LimsException(Throwable cause) {
        super(cause);
    }

    public LimsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
