package jobs;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import consts.RelationConsts;
import controllers.v1.open.LimsApi;
import exceptions.LimsException;
import models.ObjectId;
import models.client.Relation;
import models.dto.open.LimsRelation;
import org.jongo.MongoCollection;
import play.jobs.Every;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import utils.Logs;
import utils.MongoUtils;
import utils.RedisUtils;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-12-19 18:42
 * @description
 */
@Every("30mn")
@OnApplicationStart
public class RelationSyncJob extends Job {

    private static MongoCollection relationCollection = MongoUtils.getRelationCollection();

    @Override
    public void doJob() {
        // syncRelations();
    }

    public static void syncRelations() {
        Logs.info("Begin getting relations from lims");
        List<LimsRelation> limsRelations = Lists.newArrayList();
        try {
            limsRelations = LimsApi.getRelations();
            Logs.info("Get relations from lims success");
        } catch (IOException e) {
            Logs.error("IOException when connect to lims server", e);
        } catch (LimsException e) {
            Logs.error("Lims server error when get relations", e);
        }
        Logs.info("总亲缘关系条数:{}", limsRelations.size());
        try {
            List<Relation> relations = Lists.newArrayList();
            limsRelations.forEach(limsRelation -> {
                Relation relation = new Relation();
                relation.name = limsRelation.name;
                relation.id = ObjectId.get().toHexString();
                relation.limsId = limsRelation.id;
                relation.updateAt = new Date();
                relations.add(relation);
            });
            relationCollection.remove();
            relationCollection.insert(relations.toArray());
            RedisUtils.execute(jedis -> jedis.set(RelationConsts.RELATION_KEY, JSON.toJSONString(relations)));
        } catch (Exception e) {
            Logs.error("Exception occurred when sync relations.", e);
        }
    }
}
