import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.JWTVerifyException;
import entity.User;
import models.api.ObjectId;
import org.junit.Test;
import play.test.UnitTest;
import utils.JWTUtils;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.List;
import java.util.Map;

public class BasicTest extends UnitTest {

    @Test
    public void aVeryImportantThingToTest() {
        assertEquals(2, 1 + 1);
    }

    @Test
    public void jwtTest() {
        String openId = JWTUtils.sign("wx1234556667adssa");
        System.out.println(openId);
        try {
            Map verify = JWTUtils.verify(openId);
            String result = (String) verify.get("aud");
            System.out.println(JSONObject.toJSONString(verify, true));
            System.out.println(result);
        } catch (SignatureException | NoSuchAlgorithmException | JWTVerifyException | InvalidKeyException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void sqlTest() {
        List<User> users = User.findAll();
        System.out.println(JSONObject.toJSONString(users, true));
        System.out.println(users.get(0).id);
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            System.out.println(ObjectId.stringId());
        }
    }

}
