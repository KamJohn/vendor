package consts;

import interceptors.SigningInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import play.Play;
import retrofit2.Retrofit;

import java.util.concurrent.TimeUnit;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-05 下午6:01
 * @description
 */
public class Consts {

    public static final String COMMA = ",";

    public static final int RECORD_STATE_VALID = 0;

    public static final int RECORD_STATE_INVALID = 1;

    public static final String DRR_BASEURL = Play.configuration.getProperty("drr.baseUrl");

    public static final String DRR_ACCESS_KEY = Play.configuration.getProperty("drr.access.key");

    public static final String DRR_ACCESS_SECRET = Play.configuration.getProperty("drr.access.secret");

    public static final Long TIMEOUT = Long.valueOf(Play.configuration.getProperty("drr.http.timeout"));

    public static final String PGX_BASEURL = Play.configuration.getProperty("pgxcloud.baseUrl");

    public static final String PGX_ACCESS_KEY = Play.configuration.getProperty("pgxcloud.access.key");

    public static final String PGX_ACCESS_SECRET = Play.configuration.getProperty("pgxcloud.access.secret");

    public static final Long PGX_TIMEOUT = Long.valueOf(Play.configuration.getProperty("pgxcloud.http.timeout"));

    public static final String LIMS_BASEURL = Play.configuration.getProperty("lims.baseUrl");

    public static final String LIMS_ACCESS_KEY = Play.configuration.getProperty("lims.access.key");

    public static OkHttpClient drrClient = new OkHttpClient().newBuilder()
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(new SigningInterceptor(DRR_BASEURL, DRR_ACCESS_KEY, DRR_ACCESS_SECRET))
            .build();
    public static OkHttpClient limsClient = new OkHttpClient().newBuilder()
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(chain -> {
                Request request = chain.request().newBuilder().addHeader("accessKey", LIMS_ACCESS_KEY).build();
                return chain.proceed(request);
            })
            .build();
    public static OkHttpClient pgxCloudClient = new OkHttpClient().newBuilder()
            .connectTimeout(PGX_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(PGX_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(PGX_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(new SigningInterceptor(PGX_BASEURL, PGX_ACCESS_KEY, PGX_ACCESS_SECRET))
            .build();

    public static Retrofit.Builder pgxRetrofitBuilder = new Retrofit.Builder()
            .baseUrl(PGX_BASEURL)
            .client(pgxCloudClient);

    public static Retrofit.Builder drrRetrofitBuilder = new Retrofit.Builder()
            .baseUrl(DRR_BASEURL)
            .client(drrClient);

    public static Retrofit.Builder limsRetrofitBuilder = new Retrofit.Builder()
            .baseUrl(LIMS_BASEURL)
            .client(limsClient);
    public static Retrofit retrofit;
}
