package models.client;

import play.data.validation.Required;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-21 上午11:29
 * @description
 */
public class Disease {

    @Required
    public String diseaseName;

    @Required
    public String pathogenicGenes;

    public Disease(String diseaseName, String pathogenicGenes) {
        this.diseaseName = diseaseName;
        this.pathogenicGenes = pathogenicGenes;
    }
}
