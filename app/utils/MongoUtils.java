package utils;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import consts.ClientConsts;
import consts.MongoConsts;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.slf4j.Logger;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * mongo工具类.
 *
 * @author <a href="mailto:wuzhiqiang@novacloud.com">wuzq</a>
 * @version Revision: 1.1
 * @date 2014-7-28下午4:22:06
 */
public class MongoUtils {
    private static final Logger logger = Logs.get();

    private static String uri = MongoConsts.getUri();
    private static Jongo jongo;

    static {
        try {
            MongoClientURI mongoClientURI = new MongoClientURI(uri);
            DB database = new MongoClient(mongoClientURI).getDB(mongoClientURI.getDatabase());
            jongo = new Jongo(database);
        } catch (Exception e) {
            logger.error("Init mongo util error!", e);
        }
    }

    public static MongoCollection getCollection(String collectionName) {
        return jongo.getCollection(collectionName);
    }

    public static MongoCollection getRequisitionCollection() {
        return getCollection(ClientConsts.Collection.REQUISITION.value());
    }

    public static MongoCollection getSampleCollection() {
        return getCollection(ClientConsts.Collection.SAMPLE.value());
    }

    public static MongoCollection getKeyPairCollection() {
        return getCollection(ClientConsts.Collection.KEYPAIR.value());
    }

    public static MongoCollection getHospitalCollection() {
        return getCollection(ClientConsts.Collection.HOSPITAL.value());
    }

    public static MongoCollection getDepartmentCollection() {
        return getCollection(ClientConsts.Collection.DEPARTMENT.value());
    }

    public static MongoCollection getRelationCollection() {
        return getCollection(ClientConsts.Collection.RELATION.value());
    }

    public static MongoCollection getSampleTypeCollection() {
        return getCollection(ClientConsts.Collection.SAMPLE_TYPE.value());
    }

    public static MongoCollection getFileCollection() {
        return getCollection(ClientConsts.Collection.File.value());
    }

    public static MongoCollection getReportCollection() {
        return getCollection(ClientConsts.Collection.REPORT.value());
    }

    public static MongoCollection getProjectCollection() {
        return getCollection(ClientConsts.Collection.PROJECT.value());
    }

    public static <T> List<T> toList(MongoCursor<T> cursor) {
        return StreamSupport.stream(cursor.spliterator(), false).collect(Collectors.toList());
    }

}
