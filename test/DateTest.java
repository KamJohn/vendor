import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Test;

import java.util.Date;

import static jobs.SampleStatusQueryJob.predicateReportTime;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2018-08-02 18:46
 * @description
 */
public class DateTest extends BasicTest {

    @Test
    public void testDate() {
        String dateStr = "2018-12-06 12:00:00";
        Date date = DateTime.parse(dateStr, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        System.out.println();
        Date predicateDate = predicateReportTime(date, 20);
        System.out.println(new DateTime(predicateDate).toString("yyyy-MM-dd"));
    }
}
