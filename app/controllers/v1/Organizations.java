package controllers.v1;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import controllers.api.API;
import controllers.api.interceptor.Secure;
import models.client.Department;
import models.client.Hospital;
import org.jongo.MongoCollection;
import play.mvc.With;
import utils.MongoUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-05 下午4:35
 * @description
 */
@With({Secure.class})
public class Organizations extends API {

    private static MongoCollection hospitalCollection = MongoUtils.getHospitalCollection();

    private static MongoCollection departmentCollection = MongoUtils.getDepartmentCollection();

    public static void getOrganizations() {
        List<Hospital> hospitals = Lists.newArrayList(hospitalCollection.find().as(Hospital.class).iterator());
        List<Department> departments = Lists.newArrayList(departmentCollection.find().as(Department.class).iterator());
        List<String> hospitalStrs = hospitals.stream()
                .map(hospital -> hospital.name)
                .collect(Collectors.toList());
        List<String> departmentStrs = departments.stream()
                .map(department -> department.name)
                .collect(Collectors.toList());
        JSONObject object = new JSONObject();
        object.put("hospitals", hospitalStrs);
        object.put("departments", departmentStrs);
        renderJSON(object);
    }
}
