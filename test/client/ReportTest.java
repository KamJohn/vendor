package client;

import models.client.Report;
import org.jongo.MongoCollection;
import org.junit.Test;
import play.test.FunctionalTest;
import utils.MongoUtils;

import java.util.Date;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-11 下午4:16
 * @description
 */
public class ReportTest extends FunctionalTest {


    private static MongoCollection reportCollection = MongoUtils.getReportCollection();

    @Test
    public void mockReport() {
        Report report = new Report();
        report.requisitionId = "596369badf7be886844a0633";
        report.barCode = "xxxxx";
        report.detectionProject = "PGS";
        report.name = "pgs报告1";
        report.path = "reports/获取申请单方法示例0.9.1.pdf";
        report.userId = "C0003";
        report.createAt = new Date();
        report.type = "group";
        reportCollection.save(report);

    }
}
