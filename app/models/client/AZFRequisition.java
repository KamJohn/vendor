package models.client;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-27 上午10:43
 * @description Y染色体AZF区域微缺失检测申请单
 */
public class AZFRequisition extends PGXRequisition {

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 住院/门诊号
     */
    private String clinicNumber;

    /**
     * 采样日期
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String samplingDate;

    /**
     * 样本类型
     */
    private List<String> sampleType;


    /**
     * 有无子女 true:有 false:无
     */
    private Boolean hasChildren = false;

    /**
     * 子女个数
     */
    private Integer childrenCount;

    /**
     * 不孕症时间 几个月
     */
    private Integer infertilityMonths;

    /**
     * 有无流产史 true:有 false:无
     */
    private Boolean hasAbortion = false;

    /**
     * 流产次数
     */
    private Integer abortionNumber;

    /**
     * 有无隐睾 true:有  false:无
     */
    private Boolean hasCryptorchidism = false;

    /**
     * 有无精索静脉曲张 true:有 false:无
     */
    private Boolean hasVaricocele = false;

    /**
     * 有无双侧输精管缺如 true:有  false:无
     */
    private Boolean vasDeferensAbsent = false;

    /**
     * 性功能是否正常 true:正常 false:异常
     */
    private Boolean sexualFunctionNormal = true;

    /**
     * 性器官发育是否正常 true:正常 false:异常
     */
    private Boolean sexualOrgansNormal = true;

    /**
     * 睾丸B超情况
     */
    private String testisUltrasound;

    /**
     * 精液量  单位:（ml）
     */
    private Double semenVolume;

    /**
     * 液化状态
     */
    private String liquefactionState;

    /**
     * 精液PH值
     */
    private Double semenPh;

    /**
     * 粘稠度
     */
    private String viscosity;

    /**
     * 精子凝集
     */
    private String spermAgglutination;

    /**
     * 精子总数 单位:  10^6个
     */
    private Double spermNumber;

    /**
     * 畸形精子率 (%)
     */
    private Double abnormalSpermRate;

    /**
     * 不成熟精子率 （%）
     */
    private Double immatureSpermRate;

    /**
     * 精子活动率 （%）
     */
    private Double spermMotilityRate;

    /**
     * 促卵泡雌激素
     */
    private String follicleEstrogen;

    /**
     * 促黄体生成素
     */
    private String luteinizingHormone;

    /**
     * 雌二醇
     */
    private String estradiol;

    /**
     * 精子DNA完整性分析结果
     */
    private String spermDnaAnalysis;

    /**
     * 其他
     */
    private String other;

    /**
     * 样本信息集合
     */
    private List<Sample> samples = Lists.newArrayList();


    public List<String> getSampleType() {
        return sampleType;
    }

    public void setSampleType(List<String> sampleType) {
        this.sampleType = sampleType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getClinicNumber() {
        return clinicNumber;
    }

    public void setClinicNumber(String clinicNumber) {
        this.clinicNumber = clinicNumber;
    }

    public String getSamplingDate() {
        return samplingDate;
    }

    public void setSamplingDate(String samplingDate) {
        this.samplingDate = samplingDate;
    }

    public Boolean getHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(Boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public Integer getChildrenCount() {
        return childrenCount;
    }

    public void setChildrenCount(Integer childrenCount) {
        this.childrenCount = childrenCount;
    }

    public Integer getInfertilityMonths() {
        return infertilityMonths;
    }

    public void setInfertilityMonths(Integer infertilityMonths) {
        this.infertilityMonths = infertilityMonths;
    }

    public Boolean getHasAbortion() {
        return hasAbortion;
    }

    public void setHasAbortion(Boolean hasAbortion) {
        this.hasAbortion = hasAbortion;
    }

    public Integer getAbortionNumber() {
        return abortionNumber;
    }

    public void setAbortionNumber(Integer abortionNumber) {
        this.abortionNumber = abortionNumber;
    }

    public Boolean getHasCryptorchidism() {
        return hasCryptorchidism;
    }

    public void setHasCryptorchidism(Boolean hasCryptorchidism) {
        this.hasCryptorchidism = hasCryptorchidism;
    }

    public Boolean getHasVaricocele() {
        return hasVaricocele;
    }

    public void setHasVaricocele(Boolean hasVaricocele) {
        this.hasVaricocele = hasVaricocele;
    }

    public Boolean getVasDeferensAbsent() {
        return vasDeferensAbsent;
    }

    public void setVasDeferensAbsent(Boolean vasDeferensAbsent) {
        this.vasDeferensAbsent = vasDeferensAbsent;
    }

    public Boolean getSexualFunctionNormal() {
        return sexualFunctionNormal;
    }

    public void setSexualFunctionNormal(Boolean sexualFunctionNormal) {
        this.sexualFunctionNormal = sexualFunctionNormal;
    }

    public Boolean getSexualOrgansNormal() {
        return sexualOrgansNormal;
    }

    public void setSexualOrgansNormal(Boolean sexualOrgansNormal) {
        this.sexualOrgansNormal = sexualOrgansNormal;
    }

    public String getTestisUltrasound() {
        return testisUltrasound;
    }

    public void setTestisUltrasound(String testisUltrasound) {
        this.testisUltrasound = testisUltrasound;
    }

    public Double getSemenVolume() {
        return semenVolume;
    }

    public void setSemenVolume(Double semenVolume) {
        this.semenVolume = semenVolume;
    }

    public String getLiquefactionState() {
        return liquefactionState;
    }

    public void setLiquefactionState(String liquefactionState) {
        this.liquefactionState = liquefactionState;
    }

    public Double getSemenPh() {
        return semenPh;
    }

    public void setSemenPh(Double semenPh) {
        this.semenPh = semenPh;
    }

    public String getViscosity() {
        return viscosity;
    }

    public void setViscosity(String viscosity) {
        this.viscosity = viscosity;
    }

    public String getSpermAgglutination() {
        return spermAgglutination;
    }

    public void setSpermAgglutination(String spermAgglutination) {
        this.spermAgglutination = spermAgglutination;
    }

    public Double getSpermNumber() {
        return spermNumber;
    }

    public void setSpermNumber(Double spermNumber) {
        this.spermNumber = spermNumber;
    }

    public Double getAbnormalSpermRate() {
        return abnormalSpermRate;
    }

    public void setAbnormalSpermRate(Double abnormalSpermRate) {
        this.abnormalSpermRate = abnormalSpermRate;
    }

    public Double getImmatureSpermRate() {
        return immatureSpermRate;
    }

    public void setImmatureSpermRate(Double immatureSpermRate) {
        this.immatureSpermRate = immatureSpermRate;
    }

    public Double getSpermMotilityRate() {
        return spermMotilityRate;
    }

    public void setSpermMotilityRate(Double spermMotilityRate) {
        this.spermMotilityRate = spermMotilityRate;
    }

    public String getFollicleEstrogen() {
        return follicleEstrogen;
    }

    public void setFollicleEstrogen(String follicleEstrogen) {
        this.follicleEstrogen = follicleEstrogen;
    }

    public String getLuteinizingHormone() {
        return luteinizingHormone;
    }

    public void setLuteinizingHormone(String luteinizingHormone) {
        this.luteinizingHormone = luteinizingHormone;
    }

    public String getEstradiol() {
        return estradiol;
    }

    public void setEstradiol(String estradiol) {
        this.estradiol = estradiol;
    }

    public String getSpermDnaAnalysis() {
        return spermDnaAnalysis;
    }

    public void setSpermDnaAnalysis(String spermDnaAnalysis) {
        this.spermDnaAnalysis = spermDnaAnalysis;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public List<Sample> getSamples() {
        return samples;
    }

    public void setSamples(List<Sample> samples) {
        this.samples = samples;
    }
}
