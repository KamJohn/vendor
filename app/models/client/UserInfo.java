package models.client;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-05 下午6:31
 * @description
 */
public class UserInfo {

    /**
     * 送检单位
     */
    public String crmCustomer;

    /**
     * 送检科室
     */
    public String ksname;

    /**
     * 科室id
     */
    public String ksid;

    /**
     * 申请医师
     */
    public String doctor;

    /**
     * 报告接收邮箱
     */
    public String doctormail;

    /**
     * 联系电话
     */
    public String doctorphone;

    public String sender;

    public String senderPhone;

    /**
     * 响应信息
     */
    public String message;

    /**
     * 用户角色
     */
    public String role;

}
