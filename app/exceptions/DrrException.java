package exceptions;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-13 下午5:26
 * @description
 */
public class DrrException extends Exception {

    private int code;

    public DrrException(String message, int code) {
        super(message);
        this.code = code;
    }

    public DrrException(String message, Throwable cause, int code) {
        super(message, cause);
        this.code = code;
    }

    public DrrException() {
    }

    public DrrException(String message) {
        super(message);
    }

    public DrrException(String message, Throwable cause) {
        super(message, cause);
    }

    public DrrException(Throwable cause) {
        super(cause);
    }

    public DrrException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
