package controllers.v1;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import controllers.api.API;
import models.client.Disease;

import java.util.List;
import java.util.Map;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-21 上午11:28
 * @description
 */
public class Diseases extends API {

    public static Map<String, String> map = Maps.newHashMap();

    static {
        map.put("杜氏肌营养不良", "DMD");
        map.put("α地贫", "HBA");
        map.put("β地贫", "HBB");
        map.put("成人型多囊肾Ⅰ型", "PKD1");
        map.put("脊肌萎缩症", "SMN1");
        map.put("甲型血友病", "F8");
        map.put("神经纤维瘤", "NF1");
        map.put("非综合征耳聋", "SLC26A4");
        map.put("苯丙酮尿症", "PAH");
        map.put("成骨不全", "COL1A2");
        map.put("脊髓小脑共济失调SCA3型", "ATXN3");
        map.put("鸟氨酸氨甲酰基转移酶缺失症", "OTC");
        map.put("少汗或无汗型外胚层发育不全", "EDA");
        map.put("常染色体显性多囊肾Ⅱ型", "PKD2");
        map.put("软骨发育不全", "FGFR3");
        map.put("感音神经性耳聋", "GJB2");
        map.put("肝豆状核变性", "ATP7B");
        map.put("甲基丙二酸尿症", "MUT");
        map.put("马凡氏综合征", "FBN1");
        map.put("X连锁遗传型Alport综合征", "COL4A5");
    }


    public static void get() {
        List<Disease> diseases = Lists.newArrayList();
        map.entrySet().forEach(entry -> {
            Disease disease = new Disease(entry.getKey(), entry.getValue());
            diseases.add(disease);
        });
        renderJSON(diseases);
    }
}
