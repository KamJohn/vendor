package controllers.v1.open;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import consts.Project;
import exceptions.LimsException;
import models.api.Jsonable;
import models.client.*;
import models.dto.client.SubLabDTO;
import models.dto.client.UpdateAccountDTO;
import models.dto.open.LimsPGDFamilySampleDTO;
import models.dto.open.LimsRelation;
import models.dto.open.PGDFamilySampleDTO;
import models.dto.open.SampleDelivery;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.apache.commons.codec.Charsets;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;
import services.LimsService;
import utils.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static consts.Consts.limsRetrofitBuilder;
import static consts.Consts.retrofit;
import static controllers.v1.Requisitions.processDetectionProjects;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-05 下午3:33
 * @description lims接口
 */
public class LimsApi {


    private static MongoCollection requisitionCollection = MongoUtils.getRequisitionCollection();

    public static List<LimsRelation> getRelations() throws IOException, LimsException {
        retrofit = limsRetrofitBuilder
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        StopWatch stopWatch = StopWatch.createStarted();
        Response<List<LimsRelation>> response = retrofit
                .create(LimsService.class)
                .getRelations()
                .execute();
        stopWatch.stop();
        Logs.info(" GET relations cost {} milliseconds", stopWatch.getTime(TimeUnit.MILLISECONDS));
        String respString = response.body() != null ? Jsonable.toPrettyJson(response.body()) : response.errorBody().string();
        Logs.info("GET relations Info response from lims-->{}", respString);
        if (response.code() >= 500) {
            Logs.error("Lims server error when get relations.error msg:{}", respString);
            throw new LimsException();
        }
        return response.body();
    }

    /**
     * 用户登录认证
     *
     * @param id
     * @param password
     * @return
     */
    public static boolean verify(String id, String password) throws IOException, LimsException {
        boolean success = false;
        retrofit = limsRetrofitBuilder.build();
        StopWatch stopWatch = StopWatch.createStarted();
        Response<ResponseBody> response = retrofit
                .create(LimsService.class)
                .verifyUser(id, password)
                .execute();
        stopWatch.stop();
        Logs.info("Verify User:{} cost {} milliseconds", id, stopWatch.getTime(TimeUnit.MILLISECONDS));
        String respString = response.body() != null ? response.body().string() : response.errorBody().string();
        Logs.info("Verify User:{} response from lims->{}", id, respString);
        if (response.code() == 200) {
            success = true;
        } else if (response.code() >= 500) {
            Logs.error("Lims server error when verify user:{},error msg:{}", id, respString);
            throw new LimsException();
        }
        return success;
    }

    /**
     * 获取用户信息
     *
     * @param id
     * @param password
     * @return
     * @throws IOException
     */
    public static UserInfo getUserInfo(String id, String password) throws IOException, LimsException {
        retrofit = limsRetrofitBuilder
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Response<UserInfo> response;
        StopWatch stopWatch = StopWatch.createStarted();
        response = retrofit
                .create(LimsService.class)
                .getUserInfo(id, password)
                .execute();
        stopWatch.stop();
        Logs.info("Get User:{} Info cost {} milliseconds", id, stopWatch.getTime(TimeUnit.MILLISECONDS));
        String respString = response.body() != null ? Jsonable.toPrettyJson(response.body()) : response.errorBody().string();
        Logs.info("Get User:{} Info response from lims-->{}", id, respString);
        if (response.code() >= 500) {
            Logs.error("Lims server error when get user:{} info.error msg:{}", id, respString);
            throw new LimsException();
        }
        return response.body();
    }

    public static int updateUserAccount(UpdateAccountDTO updateInfo) throws IOException {
        retrofit = limsRetrofitBuilder.build();
        StopWatch stopWatch = StopWatch.createStarted();
        Response<ResponseBody> response = retrofit
                .create(LimsService.class)
                .updateAccount(updateInfo.id, updateInfo.originPassword, updateInfo.newPassword)
                .execute();
        stopWatch.stop();
        Logs.info("Update user:{} passsword  cost {} milliseconds", updateInfo.id, stopWatch.getTime(TimeUnit.MILLISECONDS));
        String respString = response.body() != null ? Jsonable.toPrettyJson(response.body()) : response.errorBody().string();
        Logs.info("Update user:{} passsword  response from lims-->{}", updateInfo.id, respString);
        return response.code();
    }


    /**
     * 提交
     *
     * @param requisition
     * @throws IOException
     */
    public static void submitRequisition(Requisition requisition) throws IOException, LimsException {
        List<models.client.File> clinicalManifestations = JSON.parseArray(JSON.toJSONString(requisition.data.get("clinicalManifestations")), models.client.File.class);
        List<models.client.File> familyImage = JSON.parseArray(JSON.toJSONString(requisition.data.get("familyImage")), models.client.File.class);
        List<models.client.File> geneticDiagnosisReport = JSON.parseArray(JSON.toJSONString(requisition.data.get("geneticDiagnosisReport")), models.client.File.class);
        List<models.client.File> nucleusDiagnosisReport = JSON.parseArray(JSON.toJSONString(requisition.data.get("nucleusDiagnosisReport")), models.client.File.class);
        List<models.client.File> semenBaseDiagnosisReport = JSON.parseArray(JSON.toJSONString(requisition.data.get("semenBaseDiagnosisReport")), models.client.File.class);
        processImageField(requisition, clinicalManifestations, "clinicalManifestations", Lists.newArrayList());
        processImageField(requisition, familyImage, "familyImage", Lists.newArrayList());
        processImageField(requisition, geneticDiagnosisReport, "geneticDiagnosisReport", Lists.newArrayList());
        processImageField(requisition, nucleusDiagnosisReport, "nucleusDiagnosisReport", Lists.newArrayList());
        processImageField(requisition, semenBaseDiagnosisReport, "semenBaseDiagnosisReport", Lists.newArrayList());
        String fullMaleBirthPlace = RegionUtils.getFullRegionName(String.valueOf(requisition.data.get("maleBirthplace")));
        String fullFemaleBirthPlace = RegionUtils.getFullRegionName(String.valueOf(requisition.data.get("femaleBirthplace")));
        String fullProbandBirthPlace = RegionUtils.getFullRegionName(String.valueOf(requisition.data.get("probandBirthplace")));
        String fullContactAddress = RegionUtils.getFullRegionName(String.valueOf(requisition.data.get("contactAddress")));
        if (StringUtils.isNotBlank(fullMaleBirthPlace)) {
            requisition.data.put("maleBirthplace", fullMaleBirthPlace);
        }
        if (StringUtils.isNotBlank(fullFemaleBirthPlace)) {
            requisition.data.put("femaleBirthplace", fullFemaleBirthPlace);
        }
        if (StringUtils.isNotBlank(fullProbandBirthPlace)) {
            requisition.data.put("probandBirthplace", fullProbandBirthPlace);
        }
        if (StringUtils.isNotBlank(fullContactAddress)) {
            requisition.data.put("contactAddress", fullContactAddress);
        }
        Object sampleType = requisition.data.get("sampleType");
        Object laboratory = requisition.data.get("laboratory");
        List<String> maleDetectionProjectIds = Lists.newArrayList();
        List<String> femaleDetectionProjectIds = Lists.newArrayList();
        List<String> projectIds = Lists.newArrayList();
        if (CommonUtils.isDrr(requisition.projectId)) {
            maleDetectionProjectIds = processDetectionProjects(requisition.data.get("maleDetectionProjects"), requisition.projectId);
            requisition.data.put("maleDetectionProjects", maleDetectionProjectIds);
            femaleDetectionProjectIds = processDetectionProjects(requisition.data.get("femaleDetectionProjects"), requisition.projectId);
            requisition.data.put("femaleDetectionProjects", femaleDetectionProjectIds);
        } else {
            requisition.data.put("detectionProjects", processDetectionProjects(requisition.data.get("detectionProjects"), requisition.projectId));
        }
        for (Sample sample : requisition.samples) {//只有样本类型为单选即String类型时，样本级别没有设置样本类型，统一使用单子里的样本类型
            //由于孕前单子分男女方检测，所以单子里的样本类型分男女方了
            if (CommonUtils.isDrr(requisition.projectId)) {
                if ("男方".equals(sample.gender)) {
                    sample.type = String.valueOf(requisition.data.get("maleSampleType"));
                    sample.maleDetectionProjects = maleDetectionProjectIds;
                } else if ("女方".equals(sample.gender)) {
                    sample.type = String.valueOf(requisition.data.get("femaleSampleType"));
                    sample.femaleDetectionProjects = femaleDetectionProjectIds;
                }
            } else {
                sample.detectionProjects = projectIds;
            }
            if (sampleType instanceof String) {
                sample.type = String.valueOf(sampleType);
            }
            sample.laboratory = laboratory != null ? String.valueOf(laboratory) : "";
            sample.subLaboratory = processSublab(requisition.data);
        }
        Map<String, Object> submitData = Maps.newHashMap(requisition.data);
        submitData.put("clientNumber", requisition.clientNumber);
        if (StringUtils.isNotBlank(requisition.limsNumber)) {
            submitData.put("limsNumber", requisition.limsNumber);
        }
        submitData.put("samples", requisition.samples);
        submitData.put("projectId", requisition.projectId);
        submitData.put("project", requisition.project);
        List<Disease> diseases = JSONArray.parseArray(JSON.toJSONString(submitData.get("diseases")), Disease.class);
        if (diseases != null && !diseases.isEmpty()) {
            List<Disease> filteredDiseases = Lists.newArrayList();
            diseases.forEach(disease -> {
                if (disease.diseaseName != null && disease.pathogenicGenes != null) {
                    filteredDiseases.add(disease);
                }
            });
            submitData.put("diseases", filteredDiseases);
        }
        submitData.put("subLaboratory", processSublab(submitData));
        //新增的脆性X和羊水组织申请单比之前的单子在样本类型处新加了其他项
        List<String> specialProjectIds = Lists.newArrayList(Project.X_FMR1.id(), Project.AMNIOTIC_FLUID.id());
        //处理脆性X和羊水组织申请单样本类型有选择'其他'的情况，由于前端需求，把'其他'传入了sampleType数组中
        //这里提交的时候要处理一下，把'其他'去掉，把其他项的实际输入值即otherSampleType的值传入sampleType中
        if (specialProjectIds.contains(requisition.projectId)) {
            Object otherSampleType = submitData.get("otherSampleType");
            if (otherSampleType != null) {
                List<String> sampleTypes = JSONArray.parseArray(JSON.toJSONString(submitData.get("sampleType")), String.class);
                if (sampleTypes.contains("其他")) {
                    sampleTypes.remove("其他");
                }
                sampleTypes.add(String.valueOf(otherSampleType));
                submitData.put("sampleType", sampleTypes);
            }
        }
        JSONObject fullFormData = getFullFormData(requisition.projectId);
        submitData.entrySet().forEach(entry -> fullFormData.put(entry.getKey(), entry.getValue()));
        String submitJsonStr = JSONFormatter.toNullFormatPrettyJson(fullFormData);
        Logs.info("Submit requisition content:\n{}", submitJsonStr);
        retrofit = limsRetrofitBuilder.build();
        StopWatch stopWatch = StopWatch.createStarted();
        Response<ResponseBody> response = retrofit
                .create(LimsService.class)
                .submitRequisition(RequestBody.create(MediaType.parse("application/json"), submitJsonStr))
                .execute();
        stopWatch.stop();
        Logs.info("Submit Requisition:{} cost {} milliseconds", requisition.id, stopWatch.getTime(TimeUnit.MILLISECONDS));
        String respString = response.body() != null ? response.body().string() : response.errorBody().string();
        Logs.info("Submit Requisition:{} response from lims-->{}", requisition.id, respString);
        if (response.code() == 200) {
            if (StringUtils.isBlank(requisition.limsNumber)) {
                JSONObject object = JSON.parseObject(respString);
                String sampleCode = object.getString("sampleCode");
                requisitionCollection.update(new ObjectId(requisition.id)).with("{$set:{limsNumber:#}}", sampleCode);
            }
        } else if (response.code() >= 400 && response.code() < 500) {
            Logs.error("Failed to submit requisition.Requisition Id:{},data:{},error msg:{}", requisition.id, submitJsonStr, respString);
            throw new LimsException();
        } else if (response.code() >= 500) {
            Logs.error("Lims server error when submit requisition.Requisition Id:{},data:{},error msg:{}", requisition.id, submitJsonStr, respString);
            throw new LimsException();
        }
    }


    /**
     * 查询样本状态
     *
     * @param orderCode lims订单号
     * @return
     * @throws IOException
     */
    public static List<SampleStateList> querySampleStates(String orderCode) throws IOException, LimsException {
        retrofit = limsRetrofitBuilder
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        StopWatch stopWatch = StopWatch.createStarted();
        Response<List<SampleStateList>> response = retrofit
                .create(LimsService.class)
                .querySampleState(orderCode)
                .execute();
        stopWatch.stop();
        Logs.info("Query limsNumber:{} SampleStates cost {} milliseconds", orderCode, stopWatch.getTime(TimeUnit.MILLISECONDS));
        List<SampleStateList> requisitionState = response.body();
        Logs.info("Query limsNumber:{} SampleStates response from lims-->{}", orderCode, requisitionState != null ? Jsonable.toPrettyJson(requisitionState) : response.errorBody().string());
        if (response.code() >= 500) {
            Logs.error("Lims server error when query sample states.LimsNumber:{},error msg:{}", orderCode, response.errorBody().string());
            throw new LimsException();
        }
        return requisitionState;
    }


    /**
     * 推送快递信息给lims
     *
     * @param sampleDelivery
     * @return
     * @throws IOException
     */
    public static void notifySampleDelivery(SampleDelivery sampleDelivery) throws IOException, LimsException {
        retrofit = limsRetrofitBuilder.build();
        StopWatch stopWatch = StopWatch.createStarted();
        Response<ResponseBody> response = retrofit
                .create(LimsService.class)
                .notifySampleDelivery(RequestBody.create(MediaType.parse("application/json"), JSONFormatter.toNullFormatPrettyJson(sampleDelivery)))
                .execute();
        stopWatch.stop();
        Logs.info("Notify limsNumber:{} SampleDelivery cost {} milliseconds", sampleDelivery.limsNumber, stopWatch.getTime(TimeUnit.MILLISECONDS));
        String respString = response.body() != null ? response.body().string() : response.errorBody().string();
        Logs.info("Notify limsNumber:{},SampleDelivery:{} SampleDelivery response from lims-->{}", sampleDelivery.limsNumber, JSONFormatter.toNullFormatPrettyJson(sampleDelivery), respString);
        if (response.code() == 200) {
            Logs.info("Notify lims sample  delivery success. limsNumber:{}", sampleDelivery.limsNumber);
        } else if (response.code() >= 400 && response.code() < 500) {
            Logs.error("Failed to notify lims sample delivery.limsNumber:{},error msg:{}", sampleDelivery.limsNumber, respString);
        } else {
            if (response.code() >= 500) {
                Logs.error("Lims server error when notify sample delivery.limsNumber:{},sampleDelivery:{},error msg:{}", sampleDelivery.limsNumber, JSONFormatter.toNullFormatPrettyJson(sampleDelivery), respString);
                throw new LimsException();
            }
        }
    }

    public static LimsPGDFamilySampleDTO queryPGDFamilySample(String familyName) throws IOException, LimsException {
        retrofit = limsRetrofitBuilder
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Response<LimsPGDFamilySampleDTO> response = retrofit
                .create(LimsService.class)
                .queryPGDFamilySample(familyName)
                .execute();
        if (response.code() != 200) {
            Logs.error("query lims PGD family sample fail,msg:{}", response.errorBody());
            throw new LimsException("query lims PGD family sample fail,msg:{}" + response.errorBody());
        }
        LimsPGDFamilySampleDTO dto = response.body();
        return dto;
    }


    private static List<Map<String, String>> processSublab(Map<String, Object> data) {
        List<SubLabDTO> subLabs = JSONArray.parseArray(JSON.toJSONString(data.get("subLaboratory")), SubLabDTO.class);
        List<Map<String, String>> labMaps = Lists.newArrayList();
        if (subLabs != null && !subLabs.isEmpty()) {
            subLabs.forEach(subLab -> {
                Map<String, String> map = Maps.newHashMap();
                map.put(subLab.name, subLab.value);
                labMaps.add(map);
            });
        }
        return labMaps;
    }

    public static JSONObject getFullFormData(String projectId) throws IOException {
        String jsonFileName = "conf/model/" + projectId + ".json";
        File file = new File(jsonFileName);
        List<String> lines = Files.readLines(file, Charsets.UTF_8);
        String jsonStr = lines.stream().collect(Collectors.joining("\n"));
        return JSON.parseObject(jsonStr);
    }

    private static void processImageField(Requisition requisition, List<models.client.File> files, String fieldName, List<String> imageUrls) {
        if (files != null && !files.isEmpty()) {
            files.forEach(file -> {
                String url = file.url;
                if (StringUtils.isNotBlank(url)) {
                    imageUrls.add(url);
                }
            });
            requisition.data.put(fieldName, JSON.toJSONString(imageUrls));
        }
    }

}
