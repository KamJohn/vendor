package models.dto.client;

import play.data.validation.Required;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-06 下午12:05
 * @description
 */
public class InputUserDTO {

    @Required
    public String id;

    @Required
    public String password;

    @Required
    public String platForm;
}
