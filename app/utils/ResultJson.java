package utils;

import enums.ResultEnum;

/**
 * @desc: 返回结果工具类
 * @createTime: 2018/11/22 19:00 @author: Kam
 */
public class ResultJson {

    /**
     * 状态码
     */
    private int code;

    /**
     * 枚举定义的消息
     */
    private String msg;

    /**
     * 返回数据
     */
    private Object data;

    /**
     * @desc: 部分参数构造函数
     * @param: [code, msg]
     * @createTime: 2018/11/22 19:05 @author: Kam
     */
    private ResultJson(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private ResultJson(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 只返回成功,不带数据的
     *
     * @return ResponseWrapper
     */
    public static ResultJson success() {
        return new ResultJson(ResultEnum.SUCCESS.code, ResultEnum.SUCCESS.msg);
    }

    /**
     * 返回成功，带数据的
     *
     * @return ResponseWrapper
     */
    public static ResultJson success(Object data) {
        return new ResultJson(ResultEnum.SUCCESS.code, ResultEnum.SUCCESS.msg, data);
    }

    /**
     * 自定义成功消息，并且带数据的(自定义指的是从封装好的枚举里选择)
     *
     * @return ResponseWrapper
     */
    public static ResultJson success(ResultEnum status, Object data) {
        return new ResultJson(status.code, status.msg, data);
    }

    /**
     * 返回失败，错误消息自己定义(自定义指的是从封装好的枚举里选择)
     *
     * @return ResponseWrapper
     */
    public static ResultJson error(ResultEnum status) {
        return new ResultJson(status.code, status.msg);
    }

    /**
     * 返回失败，错误消息自己定义
     *
     * @return ResponseWrapper
     */
    public static ResultJson error(ResultEnum status, String msg) {
        return new ResultJson(status.code, msg);
    }

    /**
     * 返回失败，自定义消息、错误码、数据
     *
     * @return ResponseWrapper
     */
    public static ResultJson error(ResultEnum status, String msg, Object data) {
        return new ResultJson(status.code, msg, data);
    }

    /**
     * 返回失败，错误消息统一(服务器故障，请稍后重试)
     *
     * @return ResponseWrapper
     */
    public static ResultJson error() {
        return new ResultJson(ResultEnum.SERVER_ERROR.code, ResultEnum.SERVER_ERROR.msg);
    }

    /**
     * 返回失败，错误消息统一(服务器故障，请稍后重试)
     *
     * @return ResponseWrapper
     */
    public static ResultJson failed() {
        return new ResultJson(ResultEnum.REQUEST_FAILED.code, ResultEnum.REQUEST_FAILED.msg);
    }

    /**
     * 返回失败，错误消息统一(服务器故障，请稍后重试)
     *
     * @return ResponseWrapper
     */
    public static ResultJson failed(ResultEnum status) {
        return new ResultJson(status.code, status.msg);
    }

    /**
     * 返回失败，错误消息统一(服务器故障，请稍后重试)
     *
     * @return ResponseWrapper
     */
    public static ResultJson failed(String msg) {
        return new ResultJson(ResultEnum.REQUEST_FAILED.code, msg);
    }
}