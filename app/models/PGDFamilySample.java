package models;

/**
 * @Author: wangdongpeng
 * @Date: 2018/12/28 下午2:22
 * @Description
 * @Version 1.0
 */
public class PGDFamilySample {
    /**
     * 样本名
     */
    public String sampleName;
    /**
     * 亲缘关系id
     */
    public String relationId;
    /**
     * 亲缘关系名称
     */
    public String relationName;
    /**
     * 文库号
     */
    public String libraryCode;
    /**
     * 样本备注
     */
    public String remark;
    /**
     * 突变类型
     */
    public String mutationType;
    /**
     * 外部样本编码
     */
    public String outCode;
}
