package utils;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import controllers.api.API;
import controllers.api.interceptor.*;
import models.KeyPair;
import models.ObjectId;
import models.api.Error;
import models.api.ErrorCode;
import org.joda.time.DateTime;
import org.jongo.MongoCollection;
import play.Logger;
import play.Play;
import play.data.validation.Required;
import play.exceptions.UnexpectedException;
import play.libs.Codec;
import play.libs.Crypto;
import play.mvc.With;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.List;

import static models.api.ErrorCode.SERVER_RESOURCE_LIMIT;

@With({APIRateLimiter.class, RequestLog.class, ExceptionCatcher.class, APIResponseWrapper.class, Gzip.class})
public class SignUtil extends API {
    final static String SEPARATOR = "&";
    final static String VERSION = "1";
    final static int DEFAULT_EXPIRE = 10;// 1 year
    private static MongoCollection keypair = MongoUtils.getKeyPairCollection();


    /**
     * 生成secretKey与accessKey对<br>
     * {@literal secretKey rule:  version+hmac( uuid  +  separator  +  accessKey + separator +expiredate  + separator + clientId)}
     * <br>
     * <pre><code> accessKey rule 1.0:  version+aes( clientId+byte(yyMM))} ,长度为25; </code></pre>
     * <pre><code> accessKey rule 2.0:  version+aes( clientId  +  separator  +  expiredate </code></pre>
     * <b>注：java的AES实现中，当明文小于等于15byte时，密文为16byte .其中，ObjectId为12 Byte，将expire date的yyMM转换为short,为2Byte</b>
     * <p>
     * 参考了如下文章中的实现思路：
     * {@literal http://stackoverflow.com/questions/1626575/best-practices-around-generating-oauth-tokens}
     * 与
     * {@literal https://github.com/stepanowon/OAuth_20/blob/master/oauth2provider/src/com/multi/oauth2/provider/util/OAuth2AccessTokenService.java}
     * </p>
     *
     * @param client_id   对于1.0版本, clientId为member id且必须是objectId
     * @param client_name 第三方名称
     * @param expire_date 默认为当前日期后10年
     */
    public static KeyPair generateKeyPair(@Required String client_id, String client_name, String expire_date) {
        if (keypair.count("{clientId:#}", client_id) >= 2) {
            Error error = new Error();
            error.setCodeMsg(SERVER_RESOURCE_LIMIT, "exceed max key pair count 2");
            badRequest(error);
        }
        KeyPair keyPair = _generateKeyPair(client_id, client_name, expire_date);
//        renderJSON(keyPair.toJson());
        return keyPair;
    }

    private static KeyPair _generateKeyPair(String clientId, String clientName, String expireDate) {
        //todo: validate expireDate date format ,must be yyMM
        if (expireDate == null || expireDate.length() < 1) {
            expireDate = DateTime.now().plusYears(DEFAULT_EXPIRE)
                    .toString("yyMM");
        }
        ByteBuffer byteBuffer = ByteBuffer.allocate(15).put(new ObjectId(clientId).toByteArray()).putShort(12, Short.parseShort(expireDate));
        String accessKey = VERSION
                + encryptAES(byteBuffer.array());
        String secretKey = VERSION
                + Crypto.sign(Codec.UUID() + SEPARATOR + accessKey
                + SEPARATOR + expireDate + SEPARATOR + clientId);

        KeyPair keyPair = new KeyPair();
        keyPair.setAccessKey(accessKey);
        keyPair.setClientId(clientId);
        keyPair.setExpireDate(new Date(DateTime.parse(expireDate).getMillis()));
        keyPair.setSecretKey(secretKey);
        keyPair.setClientName(clientName);
        keypair.save(keyPair);
        return keyPair;
    }

    /**
     * encrypt aes
     *
     * @param text
     * @return base64 encoded cipher
     */
    private static String encryptAES(byte[] text) {
        try {
            byte[] ex = Play.configuration.getProperty("application.open.secret").substring(0, 16).getBytes("UTF-8");
            SecretKeySpec skeySpec = new SecretKeySpec(ex, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] value = cipher.doFinal(text);
            return Codec.encodeBASE64(value);
        } catch (Exception var5) {
            throw new UnexpectedException(var5);
        }
    }

    /**
     * decryptAES
     *
     * @param value base64 encoded cipher
     * @return
     */
    public static byte[] decryptAES(String value) {
        try {
            byte[] ex = Play.configuration.getProperty("application.open.secret").substring(0, 16).getBytes("UTF-8");
            SecretKeySpec skeySpec = new SecretKeySpec(ex, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(2, skeySpec);
            return cipher.doFinal(Codec.decodeBASE64(value));
        } catch (Exception e) {
            Logger.error("decrypt AES exception", e);
            return null;
        }
    }

    /**
     * 解密objectId
     *
     * @param value
     * @return
     */
    private static String decryptObjectId(String value) {
        try {
            byte[] ex = Play.configuration.getProperty("application.open.secret").substring(0, 16).getBytes("UTF-8");
            SecretKeySpec skeySpec = new SecretKeySpec(ex, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(2, skeySpec);

            return new ObjectId(cipher.doFinal(Codec.decodeBASE64(value))).toString();
        } catch (Exception var5) {
            throw new UnexpectedException(var5);
        }
    }

    /**
     * 使用AES加密objectId,并将结果进行Base64编码
     *
     * @param objectId
     * @return
     */
    private static String encryptObjectId(String objectId) {
        try {
            byte[] ex = Play.configuration.getProperty("application.open.secret").substring(0, 16).getBytes("UTF-8");
            SecretKeySpec skeySpec = new SecretKeySpec(ex, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(1, skeySpec);
            return Codec.encodeBASE64(cipher.doFinal(new ObjectId(objectId).toByteArray()));
        } catch (Exception var5) {
            throw new UnexpectedException(var5);
        }
    }


    /**
     * 获取用户的key pairs <br>
     *
     * @param client_id
     */
    public static void getKeyPairs(@Required String client_id) {

        if (!ObjectId.isValid(client_id))
            badRequest("client_id");


        List<KeyPair> keyPairs = Lists.newArrayList(keypair.find("{clientId:#}", client_id).as(KeyPair.class).iterator());
        if (!keyPairs.isEmpty()) {
            renderJSON(JSON.toJSONString(keyPairs));
        } else {
            notFoundBy(client_id);
        }
    }


    public static void validateAccessKey(String client_id, String access_key) {
        Error error = new Error();
        try {
            byte[] text = decryptAES(access_key.substring(1));
            if (text == null) {
                error.setCodeWithDefaultMsg(ErrorCode.CLIENT_AUTH_ERROR);
                unauthorized(error);
            }
            assert text != null;
            String objectId = new ObjectId(ByteBuffer.allocate(12).put(text, 0, 12).array()).toString();
            short expireDate = ByteBuffer.wrap(text).getShort(12);

            error.setCodeWithDefaultMsg(ErrorCode.SUCCESS);
            if (!objectId.equalsIgnoreCase(client_id)) {
                error.setCodeWithDefaultMsg(ErrorCode.CLIENT_AUTH_ERROR);
                unauthorized(error);
            }
            if (expireDate < Short.parseShort(DateTime.now().toString("yyMM"))) {
                error.setCodeWithDefaultMsg(ErrorCode.CLIENT_AUTH_TOKEN_EXPIRED);
                unauthorized(error);
            }
            renderJSON(error.toJson());
        } catch (Exception e) {
            Logger.info("validateAccessKey Exception", e);
            error.setCodeWithDefaultMsg(ErrorCode.CLIENT_AUTH_ERROR);
            unauthorized(error);
        }
    }

}
