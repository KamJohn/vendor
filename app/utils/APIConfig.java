package utils;

import play.Play;

/**
 * @author <a href="mailto:fivesmallq@gmail.com">fivesmallq</a>
 * @version Revision: 1.0
 * @date 17/1/13 下午4:28
 */
public class APIConfig {
    private static String configPath = "api.properties";
    private static PropertiesParser prop = null;

    static {
        try {
            if (Play.mode.isDev()) {
                prop = new PropertiesParser("conf/" + configPath);

            } else {
                prop = new PropertiesParser(configPath);
            }
        } catch (Exception e) {
            Logs.error("init api config error!", e);
        }
    }

    public static PropertiesParser get() {
        return prop;
    }

    public static String get(String key, String def) {
        return prop.getStringProperty(key, def);
    }

    public static String get(String key) {
        return prop.getStringProperty(key);
    }
}
