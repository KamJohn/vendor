package models.client;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-06 下午2:23
 * @description 样本类型
 */
public class SampleType extends IdModel {

    public String name;

    public String limsId;

    public SampleType(String limsId, String name) {
        this.name = name;
        this.limsId = limsId;
    }
}
