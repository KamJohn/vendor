package controllers.v1.open;

import controllers.api.API;
import interceptors.Signature;
import models.webhook.Webhook;
import play.mvc.With;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-04-26 下午2:52
 */
@With({Signature.class})
public class Webhooks extends API {


    private static ExecutorService executor = Executors.newWorkStealingPool();

    public static void handle() {
        Webhook webhook = readBody(Webhook.class);
        executor.submit(new WebhookHandler(webhook));
        ok();
    }





}
