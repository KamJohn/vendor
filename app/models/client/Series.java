package models.client;

import java.util.List;

/**
 * @author wangdpwin
 * @version 1.0.0
 * @date 2018-10-09
 * @description
 */
public class Series {

    public String name;

    public String type;

    public List<Project> projects;

    public Series(String name, String type, List<Project> projects) {
        this.name = name;
        this.type = type;
        this.projects = projects;
    }
}
