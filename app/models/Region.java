package models;

import com.opencsv.bean.CsvBindByPosition;

/**
 * 省市县三级联动数据
 */
public class Region {

    //regionId
    @CsvBindByPosition(position = 0)
    public Integer regionId;

    //regionCode
    @CsvBindByPosition(position = 1)
    public String regionCode;

    //regionName
    @CsvBindByPosition(position = 2)
    public String regionName;

    //parentId
    @CsvBindByPosition(position = 3)
    public Integer parentId;

    //regionLevel
    @CsvBindByPosition(position = 4)
    public Integer regionLevel;

    //regionOrder
    @CsvBindByPosition(position = 5)
    public Integer regionOrder;

    //regionNameEn
    @CsvBindByPosition(position = 6)
    public String regionNameEn;

    //regionShortnameEn
    @CsvBindByPosition(position = 7)
    public String regionShortnameEn;


}
