package models.client;

import models.api.Jsonable;
import play.data.validation.Required;

/**
 * @author <a href="mailto:fivesmallq@gmail.com">fivesmallq</a>
 * @version Revision: 1.0
 * @date 16/7/24 下午7:04
 */
public class Token implements Jsonable {
    public String userId;
    public String displayName;
    public String token;
    @Required
    public String refreshToken;

    public Boolean keepLogin;

    public Token(String token) {
        this.token = token;
    }

    public Token(String userId, String token, String refreshToken) {
        this.userId = userId;
        this.token = token;
        this.refreshToken = refreshToken;
    }
}
