package controllers.v1;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mongodb.QueryBuilder;
import controllers.api.API;
import controllers.api.interceptor.Secure;
import jobs.SampleStatusQueryJob;
import models.client.Requisition;
import models.client.Sample;
import models.client.SampleState;
import models.dto.client.RequisitionDTO;
import models.dto.client.SampleDTO;
import models.dto.client.SampleQueryDTO;
import models.dto.client.SubLabDTO;
import org.apache.commons.lang3.time.DateUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import play.data.validation.Required;
import play.mvc.With;
import utils.Logs;
import utils.MongoUtils;
import utils.Pages;
import utils.StringUtils;

import java.text.ParseException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static jobs.SampleStatusQueryJob.*;

/**
 * @author cerpengxi
 * @date 17/7/6 下午3:50
 */
@With({Secure.class})
public class Samples extends API {


    private static MongoCollection requisitionCollection;

    private static MongoCollection sampleCollection;


    private static MongoCursor cursor;

    static {
        requisitionCollection = MongoUtils.getRequisitionCollection();
        sampleCollection = MongoUtils.getSampleCollection();
    }

    public static void query(int limit, int offset, String projectIds, String keywords, String start, String end) {
        limit = Pages.safeLimit(limit);
        offset = Pages.safeOffset(offset);
        String userId = currentUserId();
        QueryBuilder queryBuilder = queryBuilder(userId, projectIds, keywords, start, end);
        MongoCursor<RequisitionDTO> requisitionMongoCursor = requisitionCollection.find("#", queryBuilder.and("editable").is(false).get()).limit(limit).skip(offset).sort("{updateAt: -1}").as(RequisitionDTO.class);
        List<RequisitionDTO> requisitionDTOS = Lists.newArrayList(requisitionMongoCursor.iterator());
        if (requisitionDTOS.isEmpty()) {
            renderEmptyList();
        }
        setTotalCount(requisitionCollection.count("#", queryBuilder.and("editable").is(false).get()));
        renderJSON(setDto(requisitionDTOS));
    }

    public static void queryByRequisitionId(@Required String requisitionId) {
        String userId = currentUserId();
        Requisition requisition = requisitionCollection.findOne("{_id:#,userId:#}", new ObjectId(requisitionId), userId).as(Requisition.class);
        badRequestIfNull(requisition, "申请单不存在,申请单Id:" + requisitionId);
        String marker = requisition.marker;
        cursor = requisitionCollection.find("{marker:#}", marker).as(Requisition.class);
        List<Requisition> requisitions = Lists.newArrayList(cursor.iterator());
        List<String> requisitionIds = requisitions.stream()
                .map(pgxRequisition -> pgxRequisition.id)
                .collect(Collectors.toList());
        Map<Integer, Requisition> requisitionMap = Maps.newHashMap();
        requisitions.forEach(r -> requisitionMap.put(r.batchNumber, r));
        MongoCursor<Sample> cursor = sampleCollection.find("{userId:#,requisitionId:{$in:#}}", userId, requisitionIds).sort("{number: 1}").as(Sample.class);
        List<Sample> samples = Lists.newArrayList(cursor.iterator());
        List<SampleQueryDTO> sampleQueryDTOList = Lists.newArrayList();
        Map<Integer, List<SampleDTO>> map = Maps.newHashMap();
        for (Sample sample : samples) {
            List<SampleDTO> list = map.get(sample.batchNumber);
            if (list == null) {
                list = Lists.newArrayList();
            }
            List<List<SampleState>> sampleStates = sample.states;
            if (sampleStates != null && !sampleStates.isEmpty()) {
                for (List<SampleState> sampleState : sampleStates) {
                    if (!sampleState.isEmpty()) {
                        sampleState.sort(Comparator.comparing(SampleState::getEndDate).reversed());
                        SampleDTO sampleDTO = mapDto(sample, SampleDTO.class);
                        sampleDTO.currentState = sampleState.get(0);
                        sampleDTO.states = sampleState;
                        list.add(sampleDTO);
                    }
                }
            } else {
                SampleDTO sampleDTO = mapDto(sample, SampleDTO.class);
                list.add(sampleDTO);
            }
            map.put(sample.batchNumber, list);
        }
        map.entrySet()
                .stream()
                .forEach(entry -> {
                    if (entry != null && !entry.getValue().isEmpty()) {
                        SampleQueryDTO sampleQueryDTO = new SampleQueryDTO();
                        Requisition batchRequisition = requisitionMap.get(entry.getKey());
                        if (batchRequisition != null) {
                            Object laboratory = batchRequisition.data.get("laboratory");
                            Object detectionProjects = batchRequisition.data.get("detectionProjects");
                            Object subLaboratory = batchRequisition.data.get("subLaboratory");
                            Object detectionModel = batchRequisition.data.get("detectionModel");
                            Object maleDetectionProjects = batchRequisition.data.get("maleDetectionProjects");
                            Object femaleDetectionProjects = batchRequisition.data.get("femaleDetectionProjects");
                            sampleQueryDTO.laboratory = laboratory != null ? String.valueOf(laboratory) : null;
                            sampleQueryDTO.detectionProjects = detectionProjects != null ? JSONArray.parseArray(JSON.toJSONString(detectionProjects), String.class) : null;
                            sampleQueryDTO.subLaboratory = subLaboratory != null ? JSONArray.parseArray(JSON.toJSONString(subLaboratory), SubLabDTO.class) : null;
                            sampleQueryDTO.detectionModel = detectionModel != null ? String.valueOf(detectionModel) : null;
                            sampleQueryDTO.maleDetectionProjects = maleDetectionProjects != null ? JSONArray.parseArray(JSON.toJSONString(maleDetectionProjects), String.class) : null;
                            sampleQueryDTO.femaleDetectionProjects = femaleDetectionProjects != null ? JSONArray.parseArray(JSON.toJSONString(femaleDetectionProjects), String.class) : null;
                        }
                        sampleQueryDTO.batchNumber = entry.getKey();
                        sampleQueryDTO.samples = entry.getValue();
                        sampleQueryDTO.projectId = batchRequisition.projectId;
                        sampleQueryDTO.trackingNumber = entry.getValue().get(0).trackingNumber;
                        sampleQueryDTO.hasReport = batchRequisition.hasReport;
                        sampleQueryDTO.expectedReportTime = batchRequisition.expectedReportTime;
                        sampleQueryDTOList.add(sampleQueryDTO);
                    }
                });
        if (sampleQueryDTOList.isEmpty()) {
            renderEmptyList();
        }
        //处理老单子的报告生成时间
        sampleQueryDTOList.forEach(sampleQueryDTO -> {
            if (sampleQueryDTO.expectedReportTime == null) {
                try {
                    DateTime dateTime = getSampleReceiveDate(sampleQueryDTO.samples);
                    if (dateTime != null) {
                        sampleQueryDTO.expectedReportTime = SampleStatusQueryJob.predicateReportTime(dateTime.toDate(), projectCycleMap.get(sampleQueryDTO.projectId));
                    }
                } catch (Exception e) {
                    Logs.warn("Error while processing old requisition expectedReportTime.requisitionId :{}", requisitionId, e);
                }
            }
        });
        renderJSON(JSON.toJSONString(sampleQueryDTOList, SerializerFeature.DisableCircularReferenceDetect));
    }


    /**
     * 根据marker获取全部批次样本信息
     *
     * @param marker
     */
    public static void queryAllBatchSamples(@Required String marker) {
        String userId = currentUserId();
        cursor = requisitionCollection.find("{marker:#}", marker).as(Requisition.class);
        List<Requisition> requisitions = Lists.newArrayList(cursor.iterator());
        if (requisitions == null || requisitions.isEmpty()) {
            badRequest("Requisitions not found");
        }
        List<String> requisitionIds = requisitions.stream()
                .map(requisition -> requisition.id)
                .collect(Collectors.toList());
        cursor = sampleCollection.find("{requisitionId: {$in:#},userId:#}", requisitionIds, userId).as(Sample.class);
        List<Sample> samples = Lists.newArrayList(cursor.iterator());
        Map<Integer, List<Sample>> sampleMap = Maps.newHashMap();
        samples.forEach(sample -> {
            List<Sample> samplesList = sampleMap.get(sample.batchNumber);
            if (samplesList == null) {
                samplesList = Lists.newArrayList();
            }
            samplesList.add(sample);
            sampleMap.put(sample.batchNumber, samplesList);
        });
        List<Map<String, Object>> sampleList = Lists.newArrayList();
        sampleMap.entrySet()
                .stream()
                .forEach(entry -> {
                    if (entry != null && !entry.getValue().isEmpty()) {
                        Map<String, Object> sample = Maps.newHashMap();
                        sample.put("batchNumber", entry.getKey());
                        sample.put("samples", entry.getValue());
                        sample.put("trackingNumber", entry.getValue().get(0).trackingNumber);
                        sampleList.add(sample);
                    }
                });
        renderJSON(sampleList);
    }

    public static void queryByMarker(@Required String marker) {
        String userId = currentUserId();
        MongoCursor<RequisitionDTO> cursor = requisitionCollection.find("{marker:# ,batchNumber: {$ne:#},userId:#}", marker, 0, userId).sort("{createAt: 1}").as(RequisitionDTO.class);
        List<RequisitionDTO> requisitionDTOS = Lists.newArrayList(cursor.iterator());
        if (requisitionDTOS.isEmpty()) {
            renderEmptyList();
        }
        renderJSON(setDto(requisitionDTOS));
    }

    public static void queryByBatchNumber(@Required String requisitionId, @Required Integer batchNumber) {
        String userId = currentUserId();
        MongoCursor<Sample> cursor = sampleCollection.find("{userId:# ,requisitionId:# ,batchNumber:#}", userId, requisitionId, batchNumber).sort("{number: 1}").as(Sample.class);
        List<Sample> samples = Lists.newArrayList(cursor.iterator());
        if (samples.isEmpty()) {
            renderEmptyList();
        }
        renderJSON(samples);
    }

    private static List<RequisitionDTO> setDto(List<RequisitionDTO> requisitionDTOS) {
        for (RequisitionDTO requisitionDTO : requisitionDTOS) {
            List<String> customers = Lists.newArrayList(String.valueOf(requisitionDTO.data.get("name")), String.valueOf(requisitionDTO.data.get("femaleName")), String.valueOf(requisitionDTO.data.get("maleName")));
            customers.removeAll(Collections.singleton("null"));
            String customer = StringUtils.join(customers.toArray(), ",");
            requisitionDTO.customers = customer;
            Object detectionModel = requisitionDTO.data.get("detectionModel");
            if (detectionModel != null) {
                requisitionDTO.detectionModel = String.valueOf(detectionModel);
            }
            requisitionDTO.organization = String.valueOf(requisitionDTO.data.get("organization"));
            if (StringUtils.isNotNullOrEmpty(requisitionDTO.trackingNumber)) {
                requisitionDTO.hasTrackingNumber = true;
            }
        }
        return requisitionDTOS;
    }

    public static QueryBuilder queryBuilder(String userId, String projectIds, String keywords, String start, String end) {
        QueryBuilder queryBuilder = QueryBuilder.start().and("batchNumber").is(0).and("userId").is(userId);
        if (StringUtils.isNotNullOrEmpty(projectIds)) {
            List<String> pids = Splitter.on(',')
                    .trimResults()
                    .omitEmptyStrings()
                    .splitToList(projectIds);
            queryBuilder.and("projectId").in(pids);
        }
        if (StringUtils.isNotNullOrEmpty(start)) {
            Date date = null;
            try {
                date = DateUtils.parseDate(start, "yyyy-MM-dd");
            } catch (ParseException e) {
                badRequest("start date error:" + start);
            }
            queryBuilder.and("createAt").greaterThanEquals(date);
        }
        if (StringUtils.isNotNullOrEmpty(end)) {
            Date date = null;
            try {
                date = DateUtils.parseDate(end, "yyyy-MM-dd");
            } catch (ParseException e) {
                badRequest("end date error:" + end);
            }
            queryBuilder.and("createAt").lessThanEquals(date);
        }
        if (StringUtils.isNotNullOrEmpty(keywords)) {
            Pattern regExp = Pattern.compile(".*?" + keywords + ".*?");
            MongoCursor<Sample> cursor = sampleCollection.find("{$or:[ {barCode: {$regex:#}},{number:{$regex:#}}] }", keywords, keywords).as(Sample.class);
            List<Sample> samples = Lists.newArrayList(cursor.iterator());
            Set<String> markers = Sets.newHashSet();
            for (Sample sample : samples) {
                markers.add(sample.marker);
            }
            queryBuilder.or(queryBuilder.start("clientNumber").regex(regExp).get(), queryBuilder.start("data.femaleName").regex(regExp).get(), queryBuilder.start("data.name").regex(regExp).get(), queryBuilder.start("data.maleName").regex(regExp).get());
            if (!samples.isEmpty()) {
                queryBuilder.or(queryBuilder.start("marker").in(markers).get());
            }
        }
        return queryBuilder;
    }


    public static void syncStatus() {
        new SampleStatusQueryJob().now();
    }

    /**
     * 判断是否包含样本接收状态
     *
     * @param sampleDTOS
     * @return
     */
    private static DateTime getSampleReceiveDate(List<SampleDTO> sampleDTOS) {
        DateTime dateTime = null;
        if (sampleDTOS == null || sampleDTOS.isEmpty()) {
            return dateTime;
        }
        for (SampleDTO sampleDTO : sampleDTOS) {
            List<SampleState> states = sampleDTO.states;
            if (states != null && !states.isEmpty()) {
                for (SampleState sampleState : states) {
                    //如果该批样本中有状态为已完成的样本，则不再计算预计时间
                    if (SAMPLE_REPORT_GENERATED.equals(sampleState.sampleState)) {
                        return null;
                    }
                    //如果没有已完成状态的样本，并且有接收成功状态的样本，则计算预计时间
                    if (SAMPLE_RECEIVED.equals(sampleState.sampleState)) {
                        try {
                            dateTime = DateTime.parse(sampleState.endDate, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
                        } catch (Exception e) {
                            Logs.error("Parse state change date error.sample barcode:{} state:{} date:{}", sampleDTO.barCode, sampleState.sampleState, sampleState.endDate, e);
                            return null;
                        }
                    }
                }
            }
        }
        return dateTime;
    }
}
