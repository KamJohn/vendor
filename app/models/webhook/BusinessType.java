package models.webhook;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017/5/3 下午2:47
 */
public enum BusinessType {

    PGS("pgs", "pgs"), DRR("drr", "优孕安"), CLIENT("client", "客户端");

    private String value;
    private String desc;

    BusinessType(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String value() {
        return value;
    }

    public String desc() {
        return desc;
    }
}
