package models.client;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-20 下午6:34
 * @description
 */
public class Product extends IdModel {

    public String type;

    public List<Series> seriesList;

    public Product(String type, List<Series> seriesList) {
        this.type = type;
        this.seriesList = seriesList;
    }
}
