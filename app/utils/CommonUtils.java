package utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Lists;
import consts.Project;

import java.util.List;
import java.util.Map;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2018-05-11 19:43
 * @description
 */
public class CommonUtils {

    public static boolean isDrr(String projectId) {
        return Lists.newArrayList(Project.DRR_683.id(), Project.DRR_18.id()).contains(projectId);
    }

    public static List<String> getAllDetectionProjects(Map<String, Object> data) {
        List<String> maleDetectionProjects = JSONArray.parseArray(JSON.toJSONString(data.get("maleDetectionProjects")), String.class);
        List<String> femaleDetectionProjects = JSONArray.parseArray(JSON.toJSONString(data.get("femaleDetectionProjects")), String.class);
        List<String> detectionProjects = JSONArray.parseArray(JSON.toJSONString(data.get("detectionProjects")), String.class);
        List<String> projects = Lists.newArrayList();
        if (maleDetectionProjects != null) {
            projects.addAll(maleDetectionProjects);
        }
        if (femaleDetectionProjects != null) {
            projects.addAll(femaleDetectionProjects);
        }
        if (detectionProjects != null) {
            projects.addAll(detectionProjects);
        }
        return projects;
    }

}
