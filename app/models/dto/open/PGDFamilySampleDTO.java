package models.dto.open;

import java.util.List;
import models.PGDFamilySample;
/**
 * @Author: wangdongpeng
 * @Date: 2018/12/28 下午2:24
 * @Description
 * @Version 1.0
 */
public class PGDFamilySampleDTO {

    public String familyName;

    public List<PGDFamilySample> pgdFamilySamples;

}
