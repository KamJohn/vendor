package models.dto.open;

import models.client.Sample;
import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-05 下午2:43
 * @description 传递给lims的快递信息
 */
public class SampleDelivery {

    /**
     * lims内部单号
     */
    public String limsNumber;

    /**
     * 快递单号
     */
    public String trackingNumber;

    /**
     * 快递公司名称
     */
    public String deliveryCompany;

    /**
     * 样本集合
     */
    public List<Sample> samples;

}
