package models;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author <a href="mailto:fivesmallq@gmail.com">fivesmallq</a>
 * @version Revision: 1.0
 * @date 16/8/6 下午3:20
 */
public class Chromosome {
    @JSONField(ordinal = 0)
    public String chromosome;
    @JSONField(ordinal = 1)
    public String average;
    @JSONField(ordinal = 2)
    public String aneuploidy;
    @JSONField(ordinal = 3)
    public String position;
}
