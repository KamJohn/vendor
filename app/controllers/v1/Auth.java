package controllers.v1;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.JWTVerifyException;
import com.google.common.collect.Maps;
import consts.Consts;
import consts.UserConsts;
import controllers.api.API;
import controllers.v1.open.LimsApi;
import entity.User;
import enums.ResultEnum;
import exceptions.LimsException;
import models.api.Error;
import models.api.ErrorCode;
import models.client.Token;
import models.client.UserInfo;
import models.dto.client.InputUserDTO;
import models.wechat.AuthInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Play;
import play.mvc.Http;
import utils.*;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Map;
import java.util.UUID;

/**
 * @author cerpengxi
 * @date 17/5/18 下午4:49
 */
public class Auth extends API {

    private static String refreshTokenExp = Play.configuration.getProperty("refresh.token.exp", "86400");

    private static String refreshTokenKey = "refresh-token:";

    private static String wxToken = "wx.authentication";

    private static Logger logger = LoggerFactory.getLogger(Auth.class);

    /**
     * @param code [ 微信用户登录code ]
     * @author kam   @date 2019-09-16 12:12
     * @desc 微信授权登录, 需要将用户openId加密写入到header中以标识用户
     */
    public static void code2session() {
        Map map = readBody(Map.class);
        String code = (String) map.get("code");
        AuthInfo authInfo = WechatUtil.code2Session(code);
        if (authInfo.getErrCode() != 0) {
            logger.error("用户登录失败,code:{},错误码:{},错误信息:{}", code, authInfo.getErrCode(), authInfo.getErrMsg());
            renderJSON(ResultJson.failed());
        } else {
            logger.info("用户登录成功,登录信息:{}", JSONObject.toJSONString(authInfo));
            String tokenString = JWTUtils.sign(authInfo.getOpenId());
            // 将用户唯一标示opend加密之后添加到头信息中,用于绑定用户手机号
            response.setHeader(wxToken, tokenString);
            renderJSON(ResultJson.success(authInfo));
        }
    }

    /**
     * @param phone [ 用户手机号 ]
     * @author kam   @date 2019-09-16 12:13
     * @desc 绑定手机号.根据header中用户openId关联绑定
     */
    public static void bindUser() {
        Http.Header token = request.headers.get(wxToken);

        // 请求头中没有用户信息时需要小程序重新授权的登录
        if (token == null || StringUtils.isBlank(token.value())) {
            renderJSON(ResultJson.failed("请重新授权登录小程序"));
        }

        Map map = readBody(Map.class);
        String phone = (String) map.get("phone");

        // 手机号验证
        if (StringUtils.isBlank(phone) || !TextValidator.isMobileExact(phone)) {
            renderJSON(ResultJson.failed("手机号不合法,请检查"));
        }

        // 用户不存在
        User user = User.find("phone = ?", phone).first();
        if (user == null) {
            renderJSON(ResultJson.failed(ResultEnum.USER_UNAUTHORIZED));
        }

        // 用户被禁用
        if (user.recordState == Consts.RECORD_STATE_INVALID) {
            renderJSON(ResultJson.failed(ResultEnum.USER_INVALID));
        }
        try {
            Map claims = JWTUtils.verify(token.value());
            if (claims != null) {
                String openId = (String) claims.get("aud");
                // TODO 将手机号和openId绑定保存 完成度: %0
                user.setOpenId(openId);
                user.setPhone(phone);
                user.save();
                renderJSON(ResultJson.success("用户信息绑定成功"));
            }
        } catch (SignatureException | NoSuchAlgorithmException | JWTVerifyException | InvalidKeyException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void auth() {
        InputUserDTO input = readBody(InputUserDTO.class);
        String userId = input.id.toUpperCase();
        String password = input.password;
        boolean success;
        UserInfo userInfo = null;
        try {
            success = LimsApi.verify(userId, password);
            if (!success) {
                Logs.error("Incorrect id or password");
                badRequest("用户名或密码错误");
            }
            userInfo = LimsApi.getUserInfo(userId, password);
            if (userInfo == null) {
                Logs.error("Failed to get user info from lims.UserId-->{}", userId);
                badRequest("获取用户信息失败");
            }
        } catch (IOException e) {
            Logs.error("Failed to request to lims server");
            badRequest("Failed to request to lims server");
        } catch (LimsException e) {
            error("服务器内部错误，请联系技术支持");
        }
        String tokenString = JWTUtils.sign(userId);
        String newRefreshToken = UUID.randomUUID().toString().replace("-", "");
        RedisUtils.execute(jedis -> {
            jedis.set(refreshTokenKey + newRefreshToken, tokenString);
            jedis.expire(refreshTokenKey + newRefreshToken, Integer.valueOf(refreshTokenExp));
        });
        Token newToken = new Token(userId, tokenString, newRefreshToken);
        newToken.displayName = userInfo.doctor;
        String userKey = UserConsts.makeUserInfoKey(userId);
        userInfo.message = null;
        String redisValue = JSON.toJSONString(userInfo);
        RedisUtils.execute(jedis -> {
            jedis.set(userKey, redisValue);
        });
        setJWTCookie(tokenString, "1d");
        renderJSON(JSON.toJSONString(newToken, true));
    }

    public static void refresh() {
        Token input = readBody(Token.class);
        validate(input);
        Error error = new Error();
        error.setCodeWithDefaultMsg(ErrorCode.CLIENT_AUTH_TOKEN_EXPIRED);
        Map claims = Maps.newHashMap();
        try {
            //XXX 这里还需要校验之前的token吗？如果是过期的时候触发的refresh呢
            claims = JWTUtils.verify(input.token);
        } catch (Exception e) {
            error.setCodeMsg(ErrorCode.CLIENT_AUTH_ERROR, "access token错误");
            unauthorized(error);
        }
        String userId = String.valueOf(claims.get("aud"));
        if (StringUtils.isNullOrEmpty(userId)) {
            error.setCodeMsg(ErrorCode.CLIENT_AUTH_TOKEN_EXPIRED, "refresh token失效");
            unauthorized(error);
        }
        Logs.info("Refresh token [{}]", userId);
        String accessToken = RedisUtils.executeResult(jedis -> jedis.get(refreshTokenKey + input.refreshToken));
        //refresh token过期
        if (StringUtils.isNullOrEmpty(accessToken)) {
            error.setCodeMsg(ErrorCode.CLIENT_AUTH_TOKEN_EXPIRED, "refresh token失效");
            unauthorized(error);
        }
        String tokenString = JWTUtils.sign(input.userId);
        String newRefreshToken = UUID.randomUUID().toString().replace("-", "");
        RedisUtils.execute(jedis -> {
            jedis.expire(refreshTokenKey + newRefreshToken, Integer.valueOf(refreshTokenExp));
            jedis.set(refreshTokenKey + newRefreshToken, tokenString);
        });
        Token newToken = new Token(userId, tokenString, newRefreshToken);
        newToken.displayName = input.displayName;
        //XXX 暂时不直接删除之前的refresh token,用户多个tab页面的时候，会造成冲突，变成30秒后删除
        RedisUtils.execute(jedis -> jedis.expire(refreshTokenKey + input.refreshToken, 30));
        renderJSON(newToken.toPrettyJson());
    }


}
