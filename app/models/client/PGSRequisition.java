package models.client;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.collect.Lists;
import play.data.validation.Required;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-26 上午10:30
 * @description 染色体拷贝数分析检测申请单
 */
public class PGSRequisition extends PGXRequisition {

    /**
     * 受检者(男)
     */
    @Required
    private String maleName;

    /**
     * 男方年龄
     */
    @Required
    private Integer maleAge;

    /**
     * 受检者核型结果(男)
     */
    private String maleResult;

    /**
     * 受检者(女)
     */
    @Required
    private String femaleName;


    /**
     * 女方年龄
     */
    @Required
    private Integer femaleAge;


    /**
     * 受检者核型结果(女)
     */
    private String femaleResult;

    /**
     * 住院/门诊号
     */
    private String clinicNumber;

    /**
     * 送检日期
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String sendDate;

    /**
     * 样本信息集合
     */
    private List<Sample> samples = Lists.newArrayList();


    /**
     * 样本类型  细胞/MDA扩增产物/Sureplex扩增产物
     */
    private String sampleType;

    /**
     * 检测项目(单选) 胚胎植入前遗传学筛查（PGS）/罗氏易位PGD/平衡易位PGD/倒位PGD
     */
    @Required
    private String detectionProjects;

    /**
     * 指征
     */
    @Required
    private String conclusion;


    public String getDetectionProjects() {
        return detectionProjects;
    }

    public void setDetectionProjects(String detectionProjects) {
        this.detectionProjects = detectionProjects;
    }

    public String getMaleName() {
        return maleName;
    }

    public void setMaleName(String maleName) {
        this.maleName = maleName;
    }

    public Integer getMaleAge() {
        return maleAge;
    }

    public void setMaleAge(Integer maleAge) {
        this.maleAge = maleAge;
    }

    public String getMaleResult() {
        return maleResult;
    }

    public void setMaleResult(String maleResult) {
        this.maleResult = maleResult;
    }

    public String getFemaleName() {
        return femaleName;
    }

    public void setFemaleName(String femaleName) {
        this.femaleName = femaleName;
    }

    public Integer getFemaleAge() {
        return femaleAge;
    }

    public void setFemaleAge(Integer femaleAge) {
        this.femaleAge = femaleAge;
    }

    public String getFemaleResult() {
        return femaleResult;
    }

    public void setFemaleResult(String femaleResult) {
        this.femaleResult = femaleResult;
    }

    public String getClinicNumber() {
        return clinicNumber;
    }

    public void setClinicNumber(String clinicNumber) {
        this.clinicNumber = clinicNumber;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getSampleType() {
        return sampleType;
    }

    public void setSampleType(String sampleType) {
        this.sampleType = sampleType;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public List<Sample> getSamples() {
        return samples;
    }

    public void setSamples(List<Sample> samples) {
        this.samples = samples;
    }
}
