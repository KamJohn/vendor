package entity;

import com.alibaba.fastjson.annotation.JSONField;
import consts.Consts;
import models.api.Jsonable;
import play.db.jpa.GenericModel;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-08-30 11:22
 * @description
 */
@MappedSuperclass
public class MysqlBaseModel extends GenericModel implements Jsonable {

    @Id
    @JSONField
    public String id;


    /**
     * 记录状态标识 0有效 1删除，默认为0
     */
    @JSONField(serialize = false)
    public Integer recordState = Consts.RECORD_STATE_VALID;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    public Date createTime = new Date();

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    public Date updateTime = new Date();

}
