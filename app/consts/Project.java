package consts;/**
 * @author zanxus
 * @date 2018/5/2 11:32
 * @version 1.0.0
 */

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2018-05-02 11:32
 * @description
 */
public enum Project {

    DRR_683("JCA0001", "孕前遗传病基因检测（683种）"),
    DRR_18("JCA0002", "孕前遗传病基因检测（18种）"),
    AZF("JCA0003", "Y染色体AZF微缺失检测"),
    X_FMR1("JCA0025", "脆性X综合征FMR1基因检测"),
    PGS("JCA0018", "胚胎植入前遗传学筛查（PGS）"),
    MD_PGD("JCA0006", "单基因病S-PGD™检测"),
    HLA_PGD("JCA0007", "HLA配型S-PGD™检测"),
    RT_PGD("JCA0008", "罗氏易位染色体拷贝数分析PGD检测"),
    S_PGD("JCA0009", "罗氏易位携带者S-PGD™检测"),
    B_PGD("JCA0010", "平衡易位染色体拷贝数分析PGD检测"),
    R_PGD("JCA0011", "倒位染色体拷贝数分析PGD检测"),
    B_MISEQ("JCA0012", "平衡易位携带者Micro-Seq™检测"),
    NIPT("JCA0013", "无创产前DNA检测（NIPT）"),
    GDNA("JCA0014", "流产组织学分析"),
    AMNIOTIC_FLUID("JCA0021", "羊水组织细胞染色体非整倍体检测"),
    MD_PGD_PRE("JCA0015", "单基因病PGD预实验"),
    HLA_PGD_PRE("JCA0016", "HLA配型预实验"),
    SANGER("JCA0017", "Sanger验证"),
    FMR1_FILTER("JCA0022", "FMR1筛查"),
    BREAKPOINT("JCA0019", "断点分析"),
    FIND_CAUSE("JCA0023", "查找病因"),
    CARRIER_FILTER("JCA0024", "携带者筛查"),
    CHR_KARYOTYPE("JCA0028","染色体核型分析");


    private String id;

    private String description;


    Project(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public String id() {
        return id;
    }

    public String description() {
        return description;
    }
}
