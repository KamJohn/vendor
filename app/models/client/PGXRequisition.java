package models.client;

import play.data.validation.Required;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-26 上午10:22
 * @description pgx客户端申请单-基类
 */
public class PGXRequisition extends BaseModel {

    /**
     * 客户端申请单号
     */
    private String clientNumber;

    /**
     * 用来关联同一申请单下的不同样本批次
     */
    private String marker;

    /**
     * 申请单样本批次
     */
    private Integer batchNumber = 0;

    /**
     * lims内部单号
     */
    private String limsNumber;

    /**
     * 申请单类型 pgs/gdna/mdpgd/rtpgd/azf
     */
    private String type;

    /**
     * 申请单检测类型 与tyep对应的汉字表示
     * 胚胎植入前遗传学筛查（PGS）检测
     * 流产组织学分析检测
     * 单基因病PGD检测
     * 罗氏易位携带者PGD检测
     * Y染色体AZF区域微缺失检测
     */
    private String project;


    /**
     * 送检单位
     */
    @Required
    private String organization;

    /**
     * 科室
     */
    @Required
    private String department;

    /**
     * 申请医师
     */
    private String physician;

    /**
     * 联系电话
     */
    @Required
    private String phone;

    /**
     * 报告接收邮箱
     */
    private String email;

    /**
     * 申请单是否可编辑  true:是 false:否
     */
    private Boolean editable = true;

    private Integer sampleCount;
    /**
     * 快递单号
     */
    private String trackingNumber;

    private boolean stateFinished;

    /**
     * 是否显示提示
     */
    private boolean showTips;

    public boolean isShowTips() {
        return showTips;
    }

    public void setShowTips(boolean showTips) {
        this.showTips = showTips;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public boolean isStateFinished() {
        return stateFinished;
    }

    public void setStateFinished(boolean stateFinished) {
        this.stateFinished = stateFinished;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public Integer getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Integer batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getLimsNumber() {
        return limsNumber;
    }

    public void setLimsNumber(String limsNumber) {
        this.limsNumber = limsNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPhysician() {
        return physician;
    }

    public void setPhysician(String physician) {
        this.physician = physician;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public Integer getSampleCount() {
        return sampleCount;
    }

    public void setSampleCount(Integer sampleCount) {
        this.sampleCount = sampleCount;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }
}
