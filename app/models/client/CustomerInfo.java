package models.client;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-14 下午2:56
 * @description 受检信息
 */
public class CustomerInfo {
    /**
     * 送检编号
     */
    private String serialNumber;

    /**
     * 受检者(男)
     */
    private String maleName;


    /**
     * 受检者核型结果(男)
     */
    private String maleResult;

    /**
     * 受检者(女)
     */
    private String femaleName;


    /**
     * 受检者核型结果(女)
     */
    private String femaleResult;

    /**
     * 住院/门诊号
     */
    private String clinicNumber;

    /**
     * 指征
     */
    private String conclusion;

    /**
     * 家系编号
     */
    private String familyNumber;

    /**
     * 样本类型
     */
    private String sampleType;

    /**
     * 送检医院
     */
    private String hospital;

    /**
     * 送检日期
     */
    private String sendDate;

    /**
     * 接收日期
     */
    private String receiveDate;

    /**
     * 送检医生
     */
    private String doctor;

}
