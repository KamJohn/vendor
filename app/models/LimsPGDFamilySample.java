package models;

/**
 * @Author: wangdongpeng
 * @Date: 2018/12/28 下午2:22
 * @Description
 * @Version 1.0
 */
public class LimsPGDFamilySample {

    /**
     * 样本编码
     */
    public String sampleCode;
    /**
     * 外部样本编码
     */
    public String outCode;
    /**
     * lims家系关系id
     */
    public String personShipId;
    /**
     * 家系关系名称
     */
    public String personShipName;
    /**
     * 文库号
     */
    public String libraryCode;
    /**
     * 样本备注
     */
    public String name;
    /**
     * 突变类型
     */
    public String mutationType;
}
