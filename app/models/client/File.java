package models.client;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-07 上午11:15
 * @description
 */
public class File extends IdModel {

    public String originalName;

    public String path;

    public String finalName;

    public String url;

    public String contentType;

    public boolean valid = true;

}
