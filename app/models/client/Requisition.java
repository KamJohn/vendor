package models.client;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.collect.Lists;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-18 下午2:16
 * @description 申请单模型
 */
public class Requisition extends BaseModel {
    /**
     * 客户端申请单号
     */
    public String clientNumber;

    /**
     * 用来关联同一申请单下的不同样本批次
     */
    public String marker;

    /**
     * 申请单样本批次
     */
    public Integer batchNumber = 0;

    /**
     * lims内部单号
     */
    public String limsNumber;

    /**
     * 检测项目
     */
    public String project;

    /**
     * 检测项目id
     */
    public String projectId;

    /**
     * 申请单是否可编辑  true:是 false:否
     */
    public Boolean editable = true;

    /**
     * 申请单样所有批次样本数量总和
     */
    public Integer sampleCount;

    /**
     * 总批次数
     */
    public Integer totalBatch = 1;

    /**
     * 快递单号
     */
    public String trackingNumber;

    /**
     * 申请单所有样本流程是否已全部完成
     */
    public boolean stateFinished = false;

    public boolean hasReport = false;

    /**
     * 是否显示提示
     */
    public boolean showTips = false;

    /**
     * 申请单输入数据
     */
    public Map<String, Object> data;

    /**
     * 预计生成报告时间
     */
    @JSONField(format = "yyyy-MM-dd")
    public Date expectedReportTime;


    /**
     * 样本列表
     */
    public List<Sample> samples = Lists.newArrayList();


}
