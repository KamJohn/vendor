package models.client;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-04 下午2:12
 * @description
 */
public class BaseModel extends IdModel {

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    public Date createAt = new Date();

    /**
     * 更新时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    public Date updateAt = new Date();


    /**
     * lims用户id
     */
    public String userId;

    public int recordState;

}
