package consts;

import static consts.RedisConsts.SEPARATOR;

/**
 * 用户常量
 */
public class UserConsts {
    public static final String BIZ_TYPE_LOGIN = "login";
    public static String TOKEN_KEY_PRIFIX = "user";
    public static String TOKEN_ERROR_COUNT_KEY_PRIFIX = "error";
    public static final int LOGIN_ERROR_COUNT = 60;
    public static final int TEMP_DISABLE = 2;
    public static final String USER_KEY_PREFIX = "users";

    public static String makeErrorCountKey(String type, String userName) {
        return TOKEN_KEY_PRIFIX + SEPARATOR + TOKEN_ERROR_COUNT_KEY_PRIFIX + SEPARATOR + type + SEPARATOR + userName;
    }

    public static String makeUserInfoKey(String userId) {
        return USER_KEY_PREFIX + SEPARATOR + userId;
    }
}
