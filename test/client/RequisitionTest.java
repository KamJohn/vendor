package client;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import models.api.ObjectId;
import models.client.PGSRequisition;
import models.client.Sample;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.jongo.MongoCollection;
import org.junit.Test;
import play.test.FunctionalTest;
import utils.MongoUtils;

import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-04 下午1:33
 * @description
 */
public class RequisitionTest extends FunctionalTest {

    private static MongoCollection requisitionCollection = MongoUtils.getRequisitionCollection();

    private static MongoCollection sampleCollection = MongoUtils.getSampleCollection();


    @Test
    public void testSavePGS() throws IOException {
        PGSRequisition requisition = new PGSRequisition();
        requisition.id = ObjectId.stringId();
        requisition.userId = "C0003";
        requisition.setConclusion("指征描述");
        requisition.setFemaleName("Sahara");
        requisition.setFemaleAge(22);
        requisition.setFemaleResult("核型分析结果女");
        requisition.setMaleName("John");
        requisition.setMaleAge(24);
        requisition.setMaleResult("核型分析结果男");
        requisition.setOrganization("中信湘雅");
        requisition.setDepartment("生殖中心");
        requisition.setPhysician("张医生");
        requisition.setPhone("13000000000");
        requisition.setEmail("1111@abc.com");
        requisition.setSampleType("MDA扩增产物");
        List<Sample> samples = Lists.newArrayList();
        IntStream.range(0, 6).forEach(i -> {
            Sample sample = new Sample();
            sample.barCode = (RandomStringUtils.randomAlphanumeric(8));
            sample.requisitionId = requisition.id;
            sample.number = RandomStringUtils.randomAlphanumeric(6);
            samples.add(sample);
        });
        requisition.setSamples(samples);
        requisition.setDetectionProjects("胚胎植入前遗传学筛查（PGS）");
        requisition.setSampleCount(samples.size());
        Response response = Request.Post("http://localhost:9010/v1/requisition/pgs")
                .bodyString(JSON.toJSONString(requisition), ContentType.APPLICATION_JSON)
                .execute();
        System.out.println(response.returnContent().asString());
    }

}
