package models;

import models.dto.open.Contact;
import models.dto.open.Customer;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-05 下午3:36
 * @description
 */
public class DrrRequisition {

    public String orderCode;
    public String productName;
    public String productId;
    public List<Customer> customers;
    public Contact contact;
}
