package controllers.v1;

import com.alibaba.fastjson.JSON;
import consts.UserConsts;
import controllers.api.API;
import controllers.api.interceptor.Secure;
import controllers.v1.open.LimsApi;
import models.client.UserInfo;
import models.dto.client.UpdateAccountDTO;
import models.dto.client.UserInfoDTO;
import play.mvc.With;
import utils.Logs;
import utils.RedisUtils;
import utils.StringUtils;

import java.io.IOException;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-10 下午7:35
 * @description
 */
@With({Secure.class})
public class Users extends API {


    /**
     * 获取用户信息
     */
    public static void getUserInfo() {
        String userId = currentUserId();
        String userStr = RedisUtils.executeResult(jedis -> jedis.get(UserConsts.makeUserInfoKey(userId)));
        if (StringUtils.isBlank(userStr)) {
            badRequest("Failed to get user info from redis.UserId:" + userId);
        }
        UserInfo userInfo = JSON.parseObject(userStr, UserInfo.class);
        badRequestIfNull(userInfo, "Failed to get user info from redis.UserId:" + userId);
        UserInfoDTO userInfoDTO = new UserInfoDTO();
        userInfoDTO.organization = userInfo.crmCustomer;
        userInfoDTO.department = userInfo.ksname;
        userInfoDTO.email = userInfo.doctormail;
        userInfoDTO.phone = userInfo.doctorphone;
        userInfoDTO.name = userInfo.doctor;
        userInfoDTO.id = userId;
        userInfoDTO.sender = userInfo.sender;
        userInfoDTO.senderPhone = userInfo.senderPhone;
        renderJSON(userInfoDTO);
    }


    /**
     * 更新账号信息
     */
    public static void updateAccount() {
        String userId = currentUserId();
        UpdateAccountDTO accountDTO = readBody(UpdateAccountDTO.class);
        if (!accountDTO.confirmPassword.equals(accountDTO.newPassword)) {
            badRequest("两次输入的密码不一致，请重新确认");
        }
        if (accountDTO.newPassword.equals(accountDTO.originPassword)) {
            badRequest("新密码不能与原密码相同");
        }
        accountDTO.id = userId;
        try {
            int code = LimsApi.updateUserAccount(accountDTO);
            if (code == 200) {
                Logs.info("Update user:{} password success", userId);
            } else if (code == 400) {
                Logs.info("Update user:{} password fail,origin password incorrect", userId);
                badRequest("原密码错误");
            } else if (code >= 500) {
                Logs.info("Update user:{} password fail,lims server error", userId);
                error("服务器内部错误，请联系技术支持");
            }
        } catch (IOException e) {
            Logs.error("Failed to update user password.UserId:{}", userId);
            badRequest("网络异常，修改用户密码失败，请稍后重试");
        }
        ok();
    }


}
