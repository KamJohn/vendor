import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.junit.Test;
import play.mvc.Http;
import play.test.FunctionalTest;
import utils.Logs;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-04-26 下午5:10
 */
public class PGSResultTest extends FunctionalTest {


    @Test
    public void testReport() {
        Http.Response response = GET("/v1/reports?limit=10&offset=0");
        byte b[] = response.out.toByteArray();
        for (int x = 0; x < b.length; x++) {
            // printing the characters
            System.out.print((char) b[x]);
        }
    }

    @Test
    public void testGetResult() throws IOException {
        Response response = Request.Get("http://localhost:9000/v1/open/pgs/results/58413335c286203d88b99eb4")
                .execute();
        Logs.info(response.returnContent().asString());
    }

    @Test
    public void testGetPGS() throws IOException {
        Response response = Request.Get("http://localhost:9010/v1/open/pgs/results/58413335c286203d88b99eb4")
                .execute();
        Logs.info(response.returnContent().asString());
    }

    @Test
    public void testHttpGet() throws IOException, URISyntaxException {
//        String result = APIRequest.getReport("http://127.0.0.1:9000/v1/results/59007242f8e2ab14d49449bd");
//        System.out.println(result);
    }

    @Test
    public void testHttpsGet() throws IOException, URISyntaxException {
//        String result = APIRequest.getReportHttps("https://www.google.com/search?q=apache+httpcomponents+example&oq=apache+httpcomponents+ex&aqs=chrome.0.0j69i57j0l4.8752j0j8&sourceid=chrome&ie=UTF-8#q=java+https+get+post");
//        System.out.println(result);

    }
}
