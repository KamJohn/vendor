package client;

import com.google.common.collect.Lists;
import models.client.Sample;
import models.client.SampleState;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;
import org.junit.Test;
import play.test.FunctionalTest;
import utils.MongoUtils;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-11 下午3:59
 * @description
 */
public class SampleStateTest extends FunctionalTest {


    private MongoCollection sampleCollection = MongoUtils.getSampleCollection();

    @Test
    public void testMockState() {
        Sample sample = sampleCollection.findOne(new ObjectId("599397b10723e3043878cd0a")).as(Sample.class);
        List<SampleState> sampleStates = Lists.newArrayList();
        SampleState sampleState = new SampleState();
        sampleState.sampleState = "已到实验并接收";
        sampleState.endDate = "2017-06-10 12:00:00";
        SampleState sampleState1 = new SampleState();
        sampleState1.sampleState = "核酸提取/WGA-正在检测";
        sampleState1.endDate = "2017-06-11 12:00:00";
        sampleState1.qualityNot = "质检失败";
        sampleStates.add(sampleState);
        sampleStates.add(sampleState1);
        List<List<SampleState>> list = Lists.newArrayList();
        list.add(sampleStates);
        sample.states = list;
        sampleCollection.save(sample);
    }


}
