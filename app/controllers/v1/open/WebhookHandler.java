package controllers.v1.open;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.qingstor.sdk.exception.QSException;
import consts.ClientConsts;
import exceptions.DrrException;
import exceptions.LimsException;
import jobs.RelationSyncJob;
import jobs.SampleStatusQueryJob;
import models.client.*;
import models.webhook.BusinessType;
import models.webhook.Webhook;
import models.webhook.WebhookReport;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.FilenameUtils;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.jongo.Oid;
import retrofit2.Response;
import services.DrrService;
import services.LimsService;

import utils.*;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import static consts.Consts.*;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-04-28 下午3:55
 * @description 处理接到通知后的业务逻辑
 */
public class WebhookHandler implements Callable {

    private static final String REQUISITION = "requisition";

    private static final String REPORT_GENERATED = "report_generated";

    private static final String SAMPLE_RECEIVED = "sample_received";

    private static final String RELATION_CHANGED = "relation_changed";

    private static final String SAMPLE_STATE_CHANGED = "sample_state_changed";

    private static final String REPORT_TYPE_GROUP = "group";

    private static final String REPORT_TYPE_DATA = "data";

    private static MongoCollection requisitionCollecetion = MongoUtils.getRequisitionCollection();

    private static MongoCollection reportCollection = MongoUtils.getReportCollection();

    private static MongoCollection sampleCollection = MongoUtils.getSampleCollection();

    private static MongoCollection projectCollection = MongoUtils.getProjectCollection();

    private static MongoCollection relationCollection = MongoUtils.getRelationCollection();


    private Webhook webhook;

    public WebhookHandler(Webhook webhook) {
        this.webhook = webhook;
    }

    @Override
    public Object call() throws Exception {
        Logs.info(JSON.toJSONString(webhook));
        switch (webhook.notificationType) {
            case REPORT_GENERATED:
                //处理报告通知
                handleReport(webhook);
                break;
            case REQUISITION:
                handleRequisition(webhook);
                break;
            case RELATION_CHANGED:
                handleRelation();
                break;
            case SAMPLE_STATE_CHANGED:
                handleSample(webhook);
                break;
            default:
                break;
        }
        return null;
    }

    private void handleRelation() {
        Logs.info("Received lims notification that relations changed.");
        RelationSyncJob.syncRelations();
    }

    /**
     * 接收Drr的申请单通知，然后通知lims
     *
     * @param webhook
     * @throws Exception
     */
    private void handleRequisition(Webhook webhook) throws LimsException, IOException {
        Logs.info("received drr requisition notification. orderCode:{}", webhook.dataId);
        Logs.info("start notify lims requisition info");
        retrofit = limsRetrofitBuilder
                .build();
        Response<ResponseBody> response = retrofit.create(LimsService.class)
                .notifyLims("drr-" + webhook.dataId)
                .execute();
        Logs.info("response code :{},response message:{}", response.code(), response.body().string());
        Logs.info("notify lims requisition success");
        if (!response.isSuccessful()) {
            Logs.error("failed to notify lims.{}", response.errorBody().string());
            throw new LimsException(response.errorBody().string(), response.code());
        }
    }


    /**
     * 收到通知后根据lims订单号去lims获取样本状态数据
     *
     * @param webhook
     */
    private void handleSample(Webhook webhook) {
        String limsNumber = webhook.dataId;
        List<Requisition> requisitions = MongoUtils.toList(requisitionCollecetion.find("{stateFinished:#,limsNumber:#}", false, limsNumber).as(Requisition.class));
        requisitions.forEach(requisition -> SampleStatusQueryJob.syncSampleStates(requisition));
    }

    /**
     * 收到报告通知后去lims下载报告,然后根据业务类型进行处理
     *
     * @param webhook
     * @throws IOException
     */
    private void handleReport(Webhook webhook) throws IOException, DrrException, LimsException {
        Logs.info("received lims report generated notification.fileName:{}", webhook.dataId);
        Logs.info("start get report  file from lims.report fileName:{}", webhook.dataId);
        if (webhook.businessType.equals(BusinessType.DRR.value())) {
            String barCode = String.valueOf(webhook.data.get("barCode"));
            String fileName = webhook.dataId;
            retrofit = limsRetrofitBuilder
                    .build();
            Response<ResponseBody> limsResponse = retrofit
                    .create(LimsService.class)
                    .getReport(fileName)
                    .execute();
            InputStream inputStream = limsResponse.body().byteStream();
            if (limsResponse.code() != 200) {
                Logs.error("failed to get report from lims:{}", limsResponse.errorBody().string());
                throw new LimsException(limsResponse.errorBody().string(), limsResponse.code());
            }
            if (inputStream != null) {
                Logs.info("get drr report from lims success.fileName:{}", fileName);
                File file = new File(fileName);
                if (!file.exists()) {
                    file.createNewFile();
                }
                Files.copy(inputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
                retrofit = drrRetrofitBuilder
                        .build();
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("file", fileName, requestFile);
                Response<ResponseBody> drrResponse = retrofit
                        .create(DrrService.class)
                        .uploadReport(barCode, fileName, body)
                        .execute();
                if (drrResponse.code() == 200) {
                    Files.deleteIfExists(file.toPath());
                } else {
                    Logs.error("failed to upload report to drr:{}", drrResponse.errorBody().string());
                    throw new DrrException(drrResponse.errorBody().string(), drrResponse.code());
                }
            } else {
                Logs.error("failed to download report from lims. fileName:{}", fileName);
                throw new LimsException(limsResponse.errorBody().string(), limsResponse.code());
            }
        } else if (webhook.businessType.equals(BusinessType.CLIENT.value())) {
            String fileName = webhook.dataId;
            WebhookReport webhookReport = new WebhookReport();
            try {
                BeanUtils.populate(webhookReport, webhook.data);
            } catch (IllegalAccessException | InvocationTargetException e) {
                Logs.error("Failed to get information about report.");
            }
            retrofit = limsRetrofitBuilder
                    .build();
            Response<ResponseBody> limsResponse = retrofit
                    .create(LimsService.class)
                    .getClientReport(fileName)
                    .execute();
            InputStream inputStream = limsResponse.body().byteStream();
            if (limsResponse.code() != 200) {
                Logs.error("failed to get report from lims:{}", limsResponse.errorBody().string());
                throw new LimsException(limsResponse.errorBody().string(), limsResponse.code());
            }
            if (inputStream != null) {
                Logs.info("get client report from lims success.fileName:{}", fileName);
                String extentsion = FilenameUtils.getExtension(webhookReport.getShowName());
                String newFileName;
                if (StringUtils.isBlank(extentsion)) {
                    newFileName = ObjectId.get().toHexString();
                } else {
                    newFileName = ObjectId.get().toHexString() + "." + extentsion;
                }
                File reportDir = new File(ClientConsts.CLIENT_REPORT_DIR);
                if (!reportDir.exists()) {
                    reportDir.mkdir();
                    Logs.info("created directory reports/");
                }
                File file = new File(ClientConsts.CLIENT_REPORT_DIR + newFileName);
                Files.copy(inputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
                try {
                    QingstorUtils.upload(file, ClientConsts.CLIENT_REPORT_DIR + newFileName);
                    Logs.info("save file {} to qinstor success. newName:{}", fileName, newFileName);
                } catch (QSException e) {
                    Logs.error("save file to qingstor error! file:{} newName:{}", fileName, newFileName, e);
                }
                Logs.info("save file {} to local success.", file.getName());

                Requisition requisition = requisitionCollecetion.findOne("{limsNumber:#,batchNumber:#}", webhookReport.getOrderCode(), 0).as(Requisition.class);
                if (requisition != null) {
                    Report report = new Report();
                    report.path = file.getPath();
                    report.originalName = fileName;
                    report.type = webhookReport.getType();
                    report.name = newFileName;
                    report.projectId = webhookReport.getProjectId();
                    report.showName = webhookReport.getShowName();
                    report.clientNumber = webhookReport.getClientNumber();
                    List<String> projects = CommonUtils.getAllDetectionProjects(requisition.data);
                    if (projects != null && !projects.isEmpty()) {
                        report.detectionProject = projects.stream().collect(Collectors.joining(","));
                    } else {
                        report.detectionProject = getProjectNameById(webhookReport.getProjectId());
                    }
                    report.identity = webhookReport.getIdentity();
                    report.requisitionId = requisition.id;
                    report.userId = requisition.userId;
                    if (REPORT_TYPE_DATA.equalsIgnoreCase(webhookReport.getType())) {
                        report.barCode = webhookReport.getBarCode();
                        Sample sample = sampleCollection.findOne("{barCode:#}", webhookReport.getBarCode()).as(Sample.class);
                        if (sample != null) {
                            report.sampleNumber = sample.number;
                        }
                    }
                    reportCollection.save(report);
                    requisitionCollecetion.update("{$or:[ {_id:#},{clientNumber:#}]}", Oid.withOid(requisition.id), report.clientNumber).with("{$set:{showTips:#,updateAt:#,hasReport:#}}", true, new Date(), true);
                    Logs.info("save report information to mongo success.report id:{}", report.id);
                } else {
                    Logs.error("Can not find related requisition by limsNumber:{}", webhookReport.getOrderCode());
                }
            } else {
                Logs.error("failed to download client report from lims. fileName:{}", fileName);
                throw new LimsException(limsResponse.errorBody().string(), limsResponse.code());
            }

        }
    }

    /**
     * 根据检测项目id获取检测项目名称
     *
     * @param projectId
     * @return
     */
    private String getProjectNameById(String projectId) {
        MongoCursor cursor = projectCollection.find().as(Product.class);
        List<Product> products = Lists.newArrayList(cursor.iterator());
        List<Series> seriesList = Lists.newArrayList();
        List<Project> projects = Lists.newArrayList();
        products.forEach(product -> {
            if(product.seriesList!=null){
                seriesList.addAll(product.seriesList);
            }
        });
        seriesList.forEach(series -> projects.addAll(series.projects));
        String projectName = "";
        for (Project project : projects) {
            if (project.id.equalsIgnoreCase(projectId)) {
                projectName = project.name;
                break;
            }
        }
        return projectName;
    }
}
