package models.client;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.collect.Lists;
import play.data.validation.Required;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-26 下午3:12
 * @description 单基因病PGD申请单 （monogenic disorder）
 */
public class MDPGDRequisition extends PGXRequisition {

    /**
     * 受检者(男)
     */
    @Required
    private String maleName;

    /**
     * 男方年龄
     */
    private Integer maleAge;

    /**
     * 男方民族
     */
    private String maleNation;

    /**
     * 男方出生地
     */
    private String maleBirthplace;

    /**
     * 受检者(女)
     */
    @Required
    private String femaleName;


    /**
     * 女方年龄
     */
    private Integer femaleAge;

    /**
     * 女方民族
     */
    private String femaleNation;

    /**
     * 女方出生地
     */
    private String femaleBirthplace;

    /**
     * 先证者姓名
     */
    @Required
    private String probandName;

    /**
     * 先证者年龄
     */
    private Integer probandAge;

    /**
     * 先证者民族
     */
    private String probandNation;

    /**
     * 先证者出生地
     */
    private String probandBirthplace;

    /**
     * 住院/门诊号
     */
    private String clinicNumber;

    /**
     * 送检日期
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String sendDate;


    /**
     * 样本类型  外周血/gDNA/MDA扩增产物/Sureplex扩增产物/细胞
     */
    private List<String> sampleType;

    /**
     * 检测项目  PGS/单基因PGD预实验/单基因病PGD/HLA配型预实验/HLA配型/Sanger验证/其他
     */
    @Required
    private List<String> detectionProjects;

    /**
     * 其他检测项目名称
     */
    private String otherProject;

    /**
     * 疾病名称
     */
    @Required
    private String diseaseName;

    /**
     * 致病基因
     */
    @Required
    private String pathogenicGenes;

    /**
     * 本人有无遗传病史 ture:有 false:无
     */
    private Boolean selfGeneticDisease = false;

    /**
     * 本人遗传病史描述
     */
    private String selfGeneticDiseaseDesc;

    /**
     * 亲属有无遗传病史 ture:有 false:无
     */
    private Boolean relativesGeneticDisease = false;

    /**
     * 亲属遗传病史描述
     */
    private String relativesGeneticDiseaseDesc;

    /**
     * 受检者三月是否接受过外源性输血 true:是 false:否
     */
    private Boolean exogenousTransfused = false;

    /**
     * 受检者是否接受过异体干细胞移植
     */
    private Boolean stemCellTransplanted = false;

    /**
     * 临床表现附件访问路径
     */
    private String clinicalManifestations;

    /**
     * 家系图附件访问路径
     */
    private String familyImage;

    /**
     * 样本信息集合
     */
    private List<Sample> samples = Lists.newArrayList();


    public String getMaleName() {
        return maleName;
    }

    public void setMaleName(String maleName) {
        this.maleName = maleName;
    }

    public Integer getMaleAge() {
        return maleAge;
    }

    public void setMaleAge(Integer maleAge) {
        this.maleAge = maleAge;
    }

    public String getMaleNation() {
        return maleNation;
    }

    public void setMaleNation(String maleNation) {
        this.maleNation = maleNation;
    }

    public String getMaleBirthplace() {
        return maleBirthplace;
    }

    public void setMaleBirthplace(String maleBirthplace) {
        this.maleBirthplace = maleBirthplace;
    }

    public String getFemaleName() {
        return femaleName;
    }

    public void setFemaleName(String femaleName) {
        this.femaleName = femaleName;
    }

    public Integer getFemaleAge() {
        return femaleAge;
    }

    public void setFemaleAge(Integer femaleAge) {
        this.femaleAge = femaleAge;
    }

    public String getFemaleNation() {
        return femaleNation;
    }

    public void setFemaleNation(String femaleNation) {
        this.femaleNation = femaleNation;
    }

    public String getFemaleBirthplace() {
        return femaleBirthplace;
    }

    public void setFemaleBirthplace(String femaleBirthplace) {
        this.femaleBirthplace = femaleBirthplace;
    }

    public String getProbandName() {
        return probandName;
    }

    public void setProbandName(String probandName) {
        this.probandName = probandName;
    }

    public Integer getProbandAge() {
        return probandAge;
    }

    public void setProbandAge(Integer probandAge) {
        this.probandAge = probandAge;
    }

    public String getProbandNation() {
        return probandNation;
    }

    public void setProbandNation(String probandNation) {
        this.probandNation = probandNation;
    }

    public String getProbandBirthplace() {
        return probandBirthplace;
    }

    public void setProbandBirthplace(String probandBirthplace) {
        this.probandBirthplace = probandBirthplace;
    }

    public String getClinicNumber() {
        return clinicNumber;
    }

    public void setClinicNumber(String clinicNumber) {
        this.clinicNumber = clinicNumber;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public List<String> getSampleType() {
        return sampleType;
    }

    public void setSampleType(List<String> sampleType) {
        this.sampleType = sampleType;
    }

    public List<String> getDetectionProjects() {
        return detectionProjects;
    }

    public void setDetectionProjects(List<String> detectionProjects) {
        this.detectionProjects = detectionProjects;
    }

    public String getOtherProject() {
        return otherProject;
    }

    public void setOtherProject(String otherProject) {
        this.otherProject = otherProject;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getPathogenicGenes() {
        return pathogenicGenes;
    }

    public void setPathogenicGenes(String pathogenicGenes) {
        this.pathogenicGenes = pathogenicGenes;
    }

    public Boolean getSelfGeneticDisease() {
        return selfGeneticDisease;
    }

    public void setSelfGeneticDisease(Boolean selfGeneticDisease) {
        this.selfGeneticDisease = selfGeneticDisease;
    }

    public String getSelfGeneticDiseaseDesc() {
        return selfGeneticDiseaseDesc;
    }

    public void setSelfGeneticDiseaseDesc(String selfGeneticDiseaseDesc) {
        this.selfGeneticDiseaseDesc = selfGeneticDiseaseDesc;
    }

    public Boolean getRelativesGeneticDisease() {
        return relativesGeneticDisease;
    }

    public void setRelativesGeneticDisease(Boolean relativesGeneticDisease) {
        this.relativesGeneticDisease = relativesGeneticDisease;
    }

    public String getRelativesGeneticDiseaseDesc() {
        return relativesGeneticDiseaseDesc;
    }

    public void setRelativesGeneticDiseaseDesc(String relativesGeneticDiseaseDesc) {
        this.relativesGeneticDiseaseDesc = relativesGeneticDiseaseDesc;
    }

    public Boolean getExogenousTransfused() {
        return exogenousTransfused;
    }

    public void setExogenousTransfused(Boolean exogenousTransfused) {
        this.exogenousTransfused = exogenousTransfused;
    }

    public Boolean getStemCellTransplanted() {
        return stemCellTransplanted;
    }

    public void setStemCellTransplanted(Boolean stemCellTransplanted) {
        this.stemCellTransplanted = stemCellTransplanted;
    }

    public String getClinicalManifestations() {
        return clinicalManifestations;
    }

    public void setClinicalManifestations(String clinicalManifestations) {
        this.clinicalManifestations = clinicalManifestations;
    }

    public String getFamilyImage() {
        return familyImage;
    }

    public void setFamilyImage(String familyImage) {
        this.familyImage = familyImage;
    }

    public List<Sample> getSamples() {
        return samples;
    }

    public void setSamples(List<Sample> samples) {
        this.samples = samples;
    }
}
