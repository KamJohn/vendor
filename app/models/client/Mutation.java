package models.client;

/**
 * @author zanxus
 * @date 2017-12-26 15:14
 * @description
 */
public class Mutation {

    public String diseaseName;

    public String pathogenicGenes;

    public String mutationType;
}
