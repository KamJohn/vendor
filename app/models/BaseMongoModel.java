package models;

import com.alibaba.fastjson.annotation.JSONField;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

import java.util.Date;

/**
 * @author cerpengxi
 * @date 17/4/6 下午4:50
 */
public class BaseMongoModel {
    @MongoId
    @MongoObjectId
    public String id;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    public Date createAt;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    public Date updateAt;

}
