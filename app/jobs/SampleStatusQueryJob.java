package jobs;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import consts.Project;
import controllers.v1.open.LimsApi;
import exceptions.LimsException;
import models.client.Requisition;
import models.client.Sample;
import models.client.SampleState;
import models.client.SampleStateList;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import play.Play;
import play.jobs.Every;
import play.jobs.Job;
import utils.Logs;
import utils.MongoUtils;

import java.io.IOException;
import java.util.*;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-05-26 下午3:25
 * @description
 */
@Every("30mn")
public class SampleStatusQueryJob extends Job {

    private static MongoCollection requisitionCollection = MongoUtils.getRequisitionCollection();

    private static MongoCollection sampleCollection = MongoUtils.getSampleCollection();

    private static MongoCursor requisitionCursor;

    private static MongoCursor sampleCursor;

    private static final List<String> STATE_FINISHED = Lists.newArrayList("报告-已完成", "PGD-已发放", "预实验-PGD-报告-已发放");// TODO: 2017/7/10 最终态文字及质检失败情况待确定

    private static final List<String> STATE_UNUSUAL = Lists.newArrayList("样本接收-已到实验室拒收", "核酸提取-质控未通过");// FIXME: 2017/7/12 确认异常状态

    public static final String SAMPLE_RECEIVED = "样本接收-已到实验并接收完成";

    public static final String SAMPLE_REPORT_GENERATED = "报告-已完成";

    public static Map<String, Integer> projectCycleMap = Maps.newHashMap();

    private static final String JOB_ENABLE = Play.configuration.getProperty("sample.state.polling.enable", String.valueOf(false));

    static {
        projectCycleMap.put(Project.DRR_683.id(), 30);
        projectCycleMap.put(Project.DRR_18.id(), 30);
        projectCycleMap.put(Project.AZF.id(), 10);
        projectCycleMap.put(Project.X_FMR1.id(), 10);
        projectCycleMap.put(Project.PGS.id(), 10);
        projectCycleMap.put(Project.MD_PGD.id(), 31);
        projectCycleMap.put(Project.HLA_PGD.id(), 31);
        projectCycleMap.put(Project.RT_PGD.id(), 10);
        projectCycleMap.put(Project.S_PGD.id(), 31);
        projectCycleMap.put(Project.B_PGD.id(), 10);
        projectCycleMap.put(Project.R_PGD.id(), 10);
        projectCycleMap.put(Project.B_MISEQ.id(), 60);
        projectCycleMap.put(Project.GDNA.id(), 10);
        projectCycleMap.put(Project.AMNIOTIC_FLUID.id(), 10);
    }

    @Override
    public void doJob() {
        try {
            if (Boolean.valueOf(JOB_ENABLE)) {
                Logs.info("Start sync requisition sample states.");
                requisitionCursor = requisitionCollection.find("{stateFinished:#,limsNumber:{$exists:#},limsNumber:{$ne: null} }", false, true).as(Requisition.class);
                List<Requisition> requisitions = Lists.newArrayList(requisitionCursor.iterator());
                Logs.info("Total requisition count:{}", requisitions.size());
                for (int i = 0; i < requisitions.size(); i++) {
                    Logs.info("Sync progress {}/{}", i + 1, requisitions.size());
                    Requisition requisition = requisitions.get(i);
                    syncSampleStates(requisition);
                }
            } else {
                Logs.info("State sync job not enabled");
            }
        } catch (Exception e) {
            Logs.error("Exception occurred when while sync samples state", e);
        }
    }

    /**
     * 判断申请单所有样本的最终状态是否全部为完成态
     *
     * @param sampleStateLists
     * @return
     */
    private static boolean isStateFinished(List<SampleStateList> sampleStateLists) {
        boolean done = true;
        if (sampleStateLists == null || sampleStateLists.isEmpty()) {
            done = false;
        }
        for (SampleStateList sampleStateList : sampleStateLists) {
            List<SampleState> sampleStates = sampleStateList.sampleStates;
            if (!sampleStates.isEmpty()) {
                sampleStates.sort(Comparator.comparing(SampleState::getEndDate).reversed());
                if (!STATE_FINISHED.contains(sampleStates.get(0).sampleState)) {
                    done = false;
                }
            }
        }
        return done;
    }


    public static void syncSampleStates(Requisition requisition) {
        Logs.info("Syncing requisition:{}", requisition.id);
        String orderCode = requisition.limsNumber;
        try {
            List<SampleStateList> requisitionState = LimsApi.querySampleStates(orderCode);
            if (requisitionState == null) {
                Logs.info("Get samples states failed.RequisitionId:{}", requisition.id);
            } else if (requisitionState.isEmpty()) {
                Logs.info("Samples states is empty.RequisitionId:{}", requisition.id);
            } else {
                sampleCursor = sampleCollection.find("{marker:#}", requisition.marker).as(Sample.class);
                List<Sample> samples = Lists.newArrayList(sampleCursor.iterator());
                List<SampleStateList> sampleStateList = requisitionState;
                boolean finished = isStateFinished(sampleStateList);
                boolean exist = existUnusualState(sampleStateList);
                if (finished) {
                    requisition.stateFinished = true;
                }
                if (exist) {
                    requisition.showTips = true;
                }
                if (finished || exist) {
                    requisition.updateAt = new Date();
                }
                ListMultimap<String, List<SampleState>> multimap = ArrayListMultimap.create();
                if (!sampleStateList.isEmpty()) {
                    sampleStateList.forEach(sampleState -> multimap.put(sampleState.barCode, sampleState.sampleStates));
                }
                samples.forEach(sample -> {
                    List<List<SampleState>> states = multimap.get(sample.barCode);
                    if (states != null && !states.isEmpty()) {
                        Logs.info("Updating sample states.Sample Id:{}.Original states:{}, New states:{}", sample.id, JSON.toJSONString(sample.states), JSON.toJSONString(states));
                        Date receivedDate = getReceivedDate(states);
                        if (receivedDate != null && requisition.id.equals(sample.requisitionId) && requisition.expectedReportTime == null) {
                            requisition.expectedReportTime = predicateReportTime(receivedDate, projectCycleMap.get(requisition.projectId));
                        }
                        sample.states = multimap.get(sample.barCode);
                        sampleCollection.save(sample);
                    }
                });
                requisitionCollection.save(requisition);
                Logs.info("Sync requisition {} success", requisition.id);
            }
        } catch (IOException | LimsException e) {
            Logs.error("Failed to get sample states from lims.Sync Requisition {} fail", requisition.id, e);
        }
    }

    /**
     * 获取样本接收日期
     *
     * @param states
     * @return
     */
    private static Date getReceivedDate(List<List<SampleState>> states) {
        if (states != null && !states.isEmpty()) {
            for (List<SampleState> sampleStateList : states) {
                if (!sampleStateList.isEmpty()) {
                    try {
                        sampleStateList.sort(Comparator.comparing(SampleState::getEndDate).reversed());
                        SampleState sampleState = sampleStateList.get(0);
                        if (SAMPLE_RECEIVED.equals(sampleState.sampleState)) {
                            return DateTime.parse(sampleState.endDate, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
                        }
                    } catch (Exception e) {
                        Logs.error("parse sample received date error", e);
                        return null;
                    }
                }
            }
        }
        return null;
    }

    /**
     * 判断是否为接收样本
     *
     * @param states
     * @return
     */
    private static boolean sampleReceived(List<List<SampleState>> states) {
        boolean received = false;
        if (states != null && !states.isEmpty()) {
            for (List<SampleState> sampleStateList : states) {
                if (!sampleStateList.isEmpty()) {
                    sampleStateList.sort(Comparator.comparing(SampleState::getEndDate).reversed());
                    SampleState sampleState = sampleStateList.get(0);
                    if (SAMPLE_RECEIVED.equals(sampleState.sampleState)) {
                        received = true;
                        return received;
                    }
                }
            }
        }
        return received;
    }

    /**
     * 判断是否有异常状态
     *
     * @param sampleStateLists
     * @return
     */
    private static boolean existUnusualState(List<SampleStateList> sampleStateLists) {
        boolean exist = false;
        if (sampleStateLists != null && !sampleStateLists.isEmpty()) {
            for (SampleStateList sampleStateList : sampleStateLists) {
                List<SampleState> sampleStates = sampleStateList.sampleStates;
                if (!sampleStates.isEmpty()) {
                    for (SampleState sampleState : sampleStates) {
                        if (STATE_UNUSUAL.contains(sampleState.getSampleState())) {
                            exist = true;
                        }
                    }
                }
            }
        }
        return exist;
    }


    /**
     * 推算工作日截止时间
     *
     * @param startDate
     * @param workDay
     * @return
     */
    public static Date predicateReportTime(Date startDate, int workDay) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        for (int i = 0; i < workDay; i++) {
            // 判断当天是否为周末，如果是周末加1
            if (Calendar.SATURDAY == calendar.get(Calendar.DAY_OF_WEEK)) {
                calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 2);
                continue;
            } else if (Calendar.FRIDAY == calendar.get(Calendar.DAY_OF_WEEK)) {
                calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 3);
                continue;
            } else {
                calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
            }
        }
        return calendar.getTime();
    }

}
