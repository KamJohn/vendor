package services;

import models.PGDResult;
import models.PGSResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-17 下午6:06
 * @description PGXCloud接口
 */
public interface PGXCloudService {

    @GET("/v1/open/pgs/results/{id}")
    Call<PGSResult> getPGSResult(@Path("id") String id);

    @GET("/v1/open/pgd/results/{id}")
    Call<PGDResult> getPGDResult(@Path("id") String id);
}
