package models.dto.open;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-05 下午5:33
 * @description
 */
public class Contact {


    public String name;
    public String phone;
    public String areaCode;
    public String detlAddr;
    public String fullAddr;
}
