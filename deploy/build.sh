#!/usr/bin/env bash
export PATH=$PATH:/root/tools/play-1.4.4/
cd ../
rm -fr precompiled
rm -fr web.zip
/root/tools/play-1.4.4/play clean
/root/tools/play-1.4.4/play deps --verbose
/root/tools/play-1.4.4/play precompile
zip -r web.zip conf modules lib precompiled
