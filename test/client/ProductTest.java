package client;

import com.google.common.collect.Lists;
import models.client.Product;
import models.client.Project;
import models.client.Series;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.junit.Test;
import play.test.FunctionalTest;
import utils.MongoUtils;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-20 下午6:35
 * @description
 */
public class ProductTest extends FunctionalTest {

    private MongoCollection projectCollection = MongoUtils.getProjectCollection();

    @Test
    public void testSaveProjects() {
        List<Project> projects = Lists.newArrayList();
        projects.add(new Project("孕前遗传病基因检测（683种）", "JCA0001", "ngd-683"));
        projects.add(new Project("孕前遗传病基因检测（18种）", "JCA0002", "ngd-18"));
        projects.add(new Project("Y染色体AZF微缺失检测", "JCA0003", "py-azf"));
        projects.add(new Project("脆性X综合征FMR1基因检测", "JCA0025", "fxs"));

        List<Project> projects2 = Lists.newArrayList();
        projects.add(new Project("孕前遗传病基因检测（683种）", "JCA0001", "ngd-683"));
        projects.add(new Project("孕前遗传病基因检测（18种）", "JCA0002", "ngd-18"));

        List<Series> seriesList = Lists.newArrayList();
        Series series = new Series("优孕安TM","youyunanTM",projects);
        Series series2 = new Series("精准君TM","jingzhunjunTM",projects2);
        seriesList.add(series);
        seriesList.add(series2);

        List<Product> products = Lists.newArrayList();
        Product product = new Product("yunqian",seriesList);


        List<Project> projects3 = Lists.newArrayList();
        projects3.add(new Project("孕前遗传病基因检测（683种）", "JCA0001", "ngd-683"));
        projects3.add(new Project("孕前遗传病基因检测（18种）", "JCA0002", "ngd-18"));

        List<Series> seriesList2 = Lists.newArrayList();
        Series series3 = new Series("易安顺PGT-A","yianshunPGT-A",projects3);
        Series series4 = new Series("易安顺PGT-M","yianshunPGT-M",projects3);
        seriesList2.add(series3);
        seriesList2.add(series4);

        Product product2 = new Product("peitai",seriesList2);

        products.add(product);
        products.add(product2);
        projectCollection.insert(products.toArray());
    }


    @Test
    public void updateYouyunan() {
        Product product = projectCollection.findOne("{type:#}", "youyunan").as(Product.class);
        product.type = "yunqian";
        List<Series> seriesList = Lists.newArrayList();
        //优孕安TM系列
        List<Project> projects = Lists.newArrayList();
        projects.add(new Project("单基因病遗传基因筛查（19种）", "JCA0002", "ngd-18"));
        projects.add(new Project("脆性X综合征FMR1基因检测", "JCA0025", "fxs"));
        Series series = new Series("优孕安TM","youyunanTM",projects);

        //精准君TM系列
        List<Project> projects2 = Lists.newArrayList();
        projects2.add(new Project("Y染色体AZF微缺失检测", "JCA0003", "py-azf"));
        //暂无该项目 禁用
        projects2.add(new Project("男性不孕基因检测", "JCA0001X", "ngd-683"));
        Series series2 = new Series("精准君TM","jingzhunjunTM",projects2);

        seriesList.add(series);
        seriesList.add(series2);

        product.seriesList = seriesList;
        projectCollection.save(product);
    }

    @Test
    public void updateYianshun() {
        Product product = projectCollection.findOne("{type:#}", "yianshun").as(Product.class);
        product.type = "peitai";
        List<Series> seriesList = Lists.newArrayList();
        //易安顺PGT-A
        List<Project> projects = Lists.newArrayList();
        //找不到图中的项目
        projects.add(new Project("胚胎植入前染色体非整倍体筛查", "JCA0018", "cad-pgs"));
        Series series = new Series("易安顺PGT-A", "yianshunPGT-A", projects);

        //易安顺PGT-M
        List<Project> projects2 = Lists.newArrayList();
        projects2.add(new Project("胚胎植入前单基因病检测S-PGD™", "JCA0006", "sgd-spgd"));
        projects2.add(new Project("胚胎植入前HLA配型检测S-PGD™", "JCA0007", "hla-spgd"));
        Series series2 = new Series("易安顺PGT-M","yianshunPGT-M",projects2);

        //易安顺PGT-SR
        List<Project> projects3 = Lists.newArrayList();
        projects3.add(new Project("胚胎植入前平衡易位携带者检测Micro-Seq™", "JCA0012", "btc-seq"));
        projects3.add(new Project("胚胎植入前罗氏易位携带者检测S-PGD™", "JCA0009", "rtc-spgd"));
        projects3.add(new Project("胚胎植入前平衡易位染色体拷贝数分析检测", "JCA0010", "btccna-pgd"));
        projects3.add(new Project("胚胎植入前罗氏易位染色体拷贝数分析检测", "JCA0008", "rtccna-pgd"));
        projects3.add(new Project("胚胎植入前倒位染色体拷贝数分析检测", "JCA0011", "ioccna-pgd"));
        Series series3 = new Series("易安顺PGT-SR","yianshunPGT-SR",projects3);

        seriesList.add(series);
        seriesList.add(series2);
        seriesList.add(series3);

        product.seriesList = seriesList;
        projectCollection.save(product);
    }

    @Test
    public void updateXinanshun() {
        Product product = projectCollection.findOne("{type:#}", "xinanshun").as(Product.class);
        product.type = "chanqian";
        List<Series> seriesList = Lists.newArrayList();
        //欣安顺
        List<Project> projects = Lists.newArrayList();
        projects.add(new Project("流产组织学分析", "JCA0014", "aha"));
        projects.add(new Project("无创产前DNA检测（NIPT）", "JCA0013", "np-dna"));
        projects.add(new Project("羊水组织细胞染色体非整倍体检测", "JCA0021", "af"));
        projects.add(new Project("染色体核型分析", "JCA0028", "hxfx"));
        Series series = new Series("欣安顺", "xinanshun", projects);

        seriesList.add(series);
        product.seriesList = seriesList;
        projectCollection.save(product);
    }


}
