
package consts;


import utils.APIConfig;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-04-06 下午4:22
 */
public class QingCloudConsts {


    public static final String STORE_ACCESS_KEY = "qingcloud.store.access.key";
    public static final String STORE_ACCESS_SECRET = "qingcloud.store.access.secret";
    public static final String STORE_BUCKET_NAME = "qingcloud.store.bucket.name";
    public static final String STORE_BUCKET_ZONE = "qingcloud.store.bucket.zone";


    public static String getStoreAccessKey() {
        return APIConfig.get(STORE_ACCESS_KEY);
    }

    public static String getStoreAccessSecret() {
        return APIConfig.get(STORE_ACCESS_SECRET);
    }

    public static String getStoreBucketName() {
        return APIConfig.get(STORE_BUCKET_NAME);
    }

    public static String getStoreBucketZone() {
        return APIConfig.get(STORE_BUCKET_ZONE);
    }


}
