package controllers.v1.open;

import com.alibaba.fastjson.JSON;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import controllers.api.API;
import interceptors.Signature;
import models.PGDResult;
import models.PGSResult;
import okhttp3.ResponseBody;
import play.data.validation.Required;
import play.mvc.With;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;
import services.LimsService;
import services.PGXCloudService;
import utils.Logs;

import java.io.IOException;
import java.util.Date;

import static consts.Consts.*;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-04-25 下午5:06
 */
@With({Signature.class})
public class Results extends API {

    /**
     * 根据reportId获取pgs分析结果
     *
     * @param id
     */
    public static void getPGSResult(@Required String id) throws IOException {
        Logs.info("received lims request  getting pgs result request.id:{}", id);
        retrofit = pgxRetrofitBuilder
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .registerTypeAdapter(Date.class, (JsonDeserializer<Date>) (jsonElement, type, context)
                                -> new Date(jsonElement.getAsJsonPrimitive().getAsLong()))
                        .create()))
                .build();
        Response<PGSResult> pgsResultResponse = retrofit
                .create(PGXCloudService.class)
                .getPGSResult(id)
                .execute();
        PGSResult pgsResult = pgsResultResponse.body();
        if (pgsResult == null) {
            Logs.error("Client PGS result not found.Id:{}", id);
            notFound("Client PGS result not found.Id:" + id);
        }
        Logs.info("get pgs result success.reportId:{}\n{}", id, JSON.toJSONString(pgsResult, true));
        renderJSON(pgsResult);
    }

    /**
     * 根据reportId获取pgd分析结果
     *
     * @param id
     */
    public static void getPGDResult(@Required String id) throws IOException {
        Logs.info("received lims request  getting pgd result request.id:{}", id);
        retrofit = pgxRetrofitBuilder
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .registerTypeAdapter(Date.class, (JsonDeserializer<Date>) (jsonElement, type, context)
                                -> new Date(jsonElement.getAsJsonPrimitive().getAsLong()))
                        .create()))
                .build();
        Response<PGDResult> pgdResultResponse = retrofit
                .create(PGXCloudService.class)
                .getPGDResult(id)
                .execute();
        PGDResult pgdResult = pgdResultResponse.body();
        if (pgdResult == null) {
            Logs.error("Client PGD result not found.Id:{}", id);
            notFound("Client PGD result not found.Id:" + id);
        }
        Logs.info("get pgd result success.reportId:{}\n{}", id, JSON.toJSONString(pgdResult, true));
        renderJSON(pgdResult);
    }

    /**
     * PGS分析结果通知
     *
     * @param id
     * @throws IOException
     */
    public static void notifyLimsPGSResult(String id) throws Exception {
        Logs.info("received pgxcloud pgs results notification");
        retrofit = limsRetrofitBuilder
                .build();
        Response<ResponseBody> response = retrofit
                .create(LimsService.class)
                .notifyPGSResult(id, "pgs")
                .execute();
        String respStr = response.body() != null ? response.body().string() : response.errorBody().string();
        if (response.code() != 200) {
            Logs.error("通知lims PGS分析结果失败,msg:{}", respStr);
            throw new Exception("通知lims PGS分析结果失败,msg:{}" + respStr);
        }
        ok();
    }

    /**
     * PGD分析结果通知
     *
     * @param id
     * @throws IOException
     */
    public static void notifyLimsPGDResult(String id) throws Exception {
        Logs.info("received pgxcloud pgd results notification");
        retrofit = limsRetrofitBuilder
                .build();
        Response<ResponseBody> response = retrofit
                .create(LimsService.class)
                .notifyPGDResult(id)
                .execute();
        String respStr = response.body() != null ? response.body().string() : response.errorBody().string();
        if (response.code() != 200) {
            Logs.error("通知lims PGD分析结果失败,msg:{}", respStr);
            throw new Exception("通知lims PGD分析结果失败,msg:{}" + respStr);
        }
        ok();
    }


}
