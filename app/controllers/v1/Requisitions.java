package controllers.v1;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mongodb.DuplicateKeyException;
import com.mongodb.QueryBuilder;
import com.mongodb.WriteResult;
import com.qingstor.sdk.exception.QSException;
import consts.Project;
import consts.QingCloudConsts;
import controllers.api.API;
import controllers.api.interceptor.Secure;
import controllers.api.validation.MongoId;
import controllers.v1.open.LimsApi;
import exceptions.LimsException;
import exceptions.ReflectException;
import jobs.SampleStatusQueryJob;
import models.api.Jsonable;
import models.client.Disease;
import models.client.File;
import models.client.Requisition;
import models.client.Sample;
import models.dto.client.BatchTrackingNumber;
import models.dto.client.RequisitionDTO;
import models.dto.open.SampleDelivery;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.hashids.Hashids;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.jongo.Oid;
import play.Play;
import play.data.validation.Required;
import play.libs.MimeTypes;
import play.mvc.With;
import utils.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static consts.ClientConsts.CLIENT_NUMBER_INCR_KEY;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-06-28 上午11:21
 * @description 对前端接口
 */
@With({Secure.class})
public class Requisitions extends API {


    private static final String FILE_URL_PREFIX = Play.configuration.getProperty("file.url.prefix");

    public static Map<String, String> projectMap = Maps.newHashMap();

    private static MongoCollection requisitionCollection;

    private static MongoCollection sampleCollection;

    private static MongoCollection fileCollection;

    private static MongoCursor cursor;

    private static String codeAlphabet = "abcdefghijklmnpqrstuvwxyz123456789";

    private static String clientNumberSalt = Play.configuration.getProperty("application.client.number.secret");
    private static Hashids hashids = new Hashids(clientNumberSalt, 10, codeAlphabet);


    static {
        requisitionCollection = MongoUtils.getRequisitionCollection();
        sampleCollection = MongoUtils.getSampleCollection();
        fileCollection = MongoUtils.getFileCollection();
        projectMap.put("PGS", Project.PGS.id());
        projectMap.put("单基因病PGD预实验", Project.MD_PGD_PRE.id());
        projectMap.put("单基因病PGD", Project.MD_PGD.id());
        projectMap.put("HLA配型预实验", Project.HLA_PGD_PRE.id());
        projectMap.put("HLA配型", Project.HLA_PGD.id());
        projectMap.put("Sanger验证", Project.SANGER.id());
        projectMap.put("断点分析", Project.BREAKPOINT.id());
        projectMap.put("平衡易位PGD检测", Project.B_MISEQ.id());
        projectMap.put("FMR1筛查", Project.FMR1_FILTER.id());
        projectMap.put("查找病因", Project.FIND_CAUSE.id());
        projectMap.put("携带者筛查", Project.CARRIER_FILTER.id());
        projectMap.put("染色体非整倍体检测", Project.AMNIOTIC_FLUID.id());
        projectMap.put("脆性X综合征FMR1基因检测", Project.X_FMR1.id());
    }


    public static void query(int limit, int offset, String projectIds, String keywords, String start, String end) {
        limit = Pages.safeLimit(limit);
        offset = Pages.safeOffset(offset);
        String userId = currentUserId();
        QueryBuilder queryBuilder = Samples.queryBuilder(userId, projectIds, keywords, start, end);
        MongoCursor<RequisitionDTO> requisitionMongoCursor = requisitionCollection.find("#", queryBuilder.get()).limit(limit).skip(offset).sort("{updateAt: -1}").as(RequisitionDTO.class);
        List<RequisitionDTO> pgsRequisitions = Lists.newArrayList(requisitionMongoCursor.iterator());
        if (pgsRequisitions.isEmpty()) {
            renderEmptyList();
        }
        setTotalCount(requisitionCollection.count("#", queryBuilder.get()));
        for (RequisitionDTO requisitionDTO : pgsRequisitions) {
            List<String> customers = Lists.newArrayList(String.valueOf(requisitionDTO.data.get("name")), String.valueOf(requisitionDTO.data.get("femaleName")), String.valueOf(requisitionDTO.data.get("maleName")));
            customers.removeAll(Collections.singleton("null"));
            String customer = StringUtils.join(customers.toArray(), ",");
            requisitionDTO.customers = customer;
            requisitionDTO.organization = String.valueOf(requisitionDTO.data.get("organization"));
            Object detectionModel = requisitionDTO.data.get("detectionModel");
            if (detectionModel != null) {
                requisitionDTO.detectionModel = String.valueOf(detectionModel);
            }
        }
        renderJSON(pgsRequisitions);
    }

    /**
     * 获取申请单详情
     *
     * @param id
     */
    public static void get(@Required @MongoId String id) {
        Requisition requisition = requisitionCollection.findOne("{_id:#,userId:#}", new ObjectId(id), currentUserId()).as(Requisition.class);
        notFoundIfNull(requisition, "Requisition not found in mongo. Object Id:" + id);
        cursor = sampleCollection.find("{requisitionId:#,userId:#}", id, currentUserId()).as(Sample.class);
        List<Sample> samples = Lists.newArrayList(cursor.iterator());
        requisition.samples = samples;
        renderJSON(requisition);
    }


    /**
     * 新建申请单
     */
    public static void save() {
        Requisition requisition = readBody(Requisition.class);
        String userId = currentUserId();
        requisition.userId = userId;
        requisition.marker = UUID.randomUUID().toString().replaceAll("-", "");
        requisition.id = new ObjectId().toString();
        requisition.clientNumber = generateClientNumber();
        List<Sample> samples = JSON.parseArray(JSON.toJSONString(requisition.samples), Sample.class);
        if (samples != null && !samples.isEmpty()) {
            for (Sample sample : samples) {
                sample.requisitionId = requisition.id;
                sample.userId = userId;
                sample.batchNumber = 0;
                sample.marker = requisition.marker;
            }
            try {
                sampleCollection.insert(samples.toArray());
            } catch (DuplicateKeyException e) {
                badRequest(getDuplicateMsg(e));
            }
        }
        requisition.sampleCount = samples.size();
        requisition.samples = null;
        requisitionCollection.save(requisition);
        requisition.samples = samples;
        created(requisition);
    }

    /**
     * 编辑申请单
     *
     * @param id
     */
    public static void update(@Required @MongoId String id) {
        Requisition updatedRequisition = readBody(Requisition.class);
        String userId = currentUserId();
        Requisition requisition = requisitionCollection.findOne("{_id:#,userId:#}", new ObjectId(id), userId).as(Requisition.class);
        badRequestIfNull(requisition, "Requisition not exist.Id:" + id);
        if (!requisition.editable) {
            badRequest("Requisition is not editable after submitting.Id:" + id);
        }
        MongoCursor<Sample> sampleCursor = sampleCollection.find("{requisitionId:#,userId:#}", id, userId).as(Sample.class);
        List<Sample> samples = Lists.newArrayList(sampleCursor.iterator());
        requisition.data = updatedRequisition.data;
        Integer batchNumber = requisition.batchNumber;
        if (!samples.isEmpty()) {
            List<ObjectId> ids = samples
                    .stream()
                    .map(sample -> new ObjectId(sample.id))
                    .collect(Collectors.toList());
            sampleCollection.remove("{_id:{$in:#}}", ids);
        }
        samples = JSON.parseArray(JSON.toJSONString(updatedRequisition.samples), Sample.class);
        if (samples != null && !samples.isEmpty()) {
            samples.forEach(sample -> {
                sample.requisitionId = id;
                sample.userId = userId;
                sample.batchNumber = batchNumber;
                sample.marker = requisition.marker;
            });
            try {
                sampleCollection.insert(samples.toArray());
            } catch (DuplicateKeyException e) {
                badRequest(getDuplicateMsg(e));
            }
        }
        requisition.updateAt = new Date();
        requisition.sampleCount = samples.size();
        requisition.samples = null;
        requisitionCollection.save(requisition);
        requisition.samples = samples;
        renderJSON(requisition);
    }

    /**
     * 提交申请单
     *
     * @param id
     */
    public static void submit(@MongoId @Required String id) {
        String userId = currentUserId();
        Requisition requisition = requisitionCollection.findOne("{_id:#,userId:#}", new ObjectId(id), userId).as(Requisition.class);
        badRequestIfNull(requisition, "Requisition not exist.Id:" + id);
        if (!requisition.editable) {
            badRequest("This requisition already submitted.Id:" + id);
        }
        cursor = sampleCollection.find("{userId:#,requisitionId:#}", userId, id).as(Sample.class);
        List<Sample> samples = Lists.newArrayList(cursor.iterator());
        requisition.samples = samples;
        try {
            FormValidateUtils.validate(requisition);
        } catch (IllegalArgumentException e) {
            badRequest(e.getMessage());
        }

        try {
            LimsApi.submitRequisition(requisition);
        } catch (IOException e) {
            Logs.error("Failed to submit requisition.RequisitionId:{}", requisition.id);
            badRequest("网络异常，申请单提交失败，请稍后重试");
        } catch (LimsException e) {
            error("服务器内部错误，请联系技术支持");
        }
        requisitionCollection.update(new ObjectId(requisition.id)).with("{$set:{editable:#}}", false);
        ok();
    }

    /**
     * 补充样本信息
     *
     * @param id
     */
    public static void supplementSamples(@MongoId @Required String id) {
        String userId = currentUserId();
        Requisition firstRequisition = requisitionCollection.findOne("{_id:#,userId:#}", new ObjectId(id), userId).as(Requisition.class);
        badRequestIfNull(firstRequisition, "First batch requisition not found.Id:" + id);
        if (firstRequisition.editable) {
            badRequest("Requisition is not submitted,can not supplement samples.Id:" + id);
        }
        String marker = firstRequisition.marker;
        cursor = requisitionCollection.find("{ marker:#}", marker).as(Requisition.class);
        Requisition newRequisiton = readBody(Requisition.class);
        try {
            FormValidateUtils.validate(newRequisiton);
        } catch (IllegalArgumentException e) {
            badRequest(e.getMessage());
        }
        newRequisiton.id = ObjectId.get().toString();
        newRequisiton.marker = marker;
        newRequisiton.userId = userId;
        newRequisiton.limsNumber = firstRequisition.limsNumber;
        newRequisiton.editable = false;
        newRequisiton.clientNumber = generateClientNumber();
        newRequisiton.batchNumber = cursor.count();
        newRequisiton.trackingNumber = null;
        newRequisiton.stateFinished = false;
        newRequisiton.showTips = false;
        Date now = new Date();
        newRequisiton.createAt = now;
        newRequisiton.updateAt = now;
        List<Sample> samples = JSON.parseArray(JSON.toJSONString(newRequisiton.samples), Sample.class);
        if (samples == null || samples.isEmpty()) {
            badRequest("未添加任何样本信息");
        }
        if (samples != null && !samples.isEmpty()) {
            samples.forEach(sample -> {
                sample.id = ObjectId.get().toHexString();
                sample.requisitionId = newRequisiton.id;
                sample.userId = userId;
                sample.batchNumber = cursor.count();
                sample.marker = marker;
            });
            try {
                sampleCollection.insert(samples.toArray());
            } catch (DuplicateKeyException e) {
                badRequest(getDuplicateMsg(e));
            }
            firstRequisition.sampleCount += samples.size();
        }
        try {
            newRequisiton.samples = samples;
            firstRequisition.totalBatch++;
            firstRequisition.updateAt = now;
            requisitionCollection.save(firstRequisition);
            newRequisiton.totalBatch = firstRequisition.totalBatch;
            newRequisiton.sampleCount = samples.size();
            requisitionCollection.save(newRequisiton);
            LimsApi.submitRequisition(newRequisiton);
        } catch (IOException e) {
            Logs.error("Faild to supplement samples to lims.RequisitionId:{}", id, e);
            badRequest("网络异常，补充样本信息失败，请稍后重试");
        } catch (LimsException e) {
            Logs.error("Lims server error when supplement samples.RequisitionId:{} new requisition data:{}", id, JSONFormatter.toNullFormatPrettyJson(newRequisiton), e);
            error("服务器内部错误，请联系技术支持");
        }
        created(JSON.toJSONString(newRequisiton, SerializerFeature.DisableCircularReferenceDetect));
    }

    /**
     * 访问图片
     */
    public static void getFile(@Required String fileName) {
        try {
            InputStream inputStream = QingstorUtils.getObject(fileName);
            renderBinary(inputStream, fileName, MimeTypes.getContentType(fileName), false);
        } catch (QSException e) {
            Logs.error("get file error! path:{}", fileName, e);
            badRequest(e.getErrorMessage());
        }
    }


    /**
     * @param id
     */
    public static void deleteFile(String requisitionId, @Required String field, @Required String id) {
        File file = fileCollection.findOne(new ObjectId(id)).as(File.class);
        if (file == null) {
            badRequest("File not found.File Id:" + id);
        }
        fileCollection.update(new ObjectId(id)).with("{$set:{valid:#}}", false);
        if (StringUtils.isNotBlank(requisitionId)) {
            Requisition requisition = requisitionCollection.findOne(new ObjectId(requisitionId)).as(Requisition.class);
            if (requisition == null) {
                badRequest("Requisition not found.RequisitionId:" + requisitionId);
            }
            List<File> files = JSON.parseArray(JSON.toJSONString(requisition.data.get(field)), File.class);
            if (files != null && !files.isEmpty()) {
                IntStream.range(0, files.size()).forEach(index -> {
                    if (files.get(index).id == id) {
                        files.remove(index);
                        return;
                    }
                });
                requisition.data.put(field, files);
                requisitionCollection.save(requisition);
            }
        }
        noContent();
    }

    /**
     * 保存上传文件
     */
    public static void saveFile(@Required java.io.File file) {
        if (file.length() > 1024 * 1024 * 20) {
            badRequest("文件大小不能超过10M");
        }
        String objectKey = UUID.randomUUID().toString().replaceAll("-", "") + "." + FilenameUtils.getExtension(file.getName());
        try {
            QingstorUtils.upload(file, objectKey);
            Logs.info("upload file {} success. {} => {}", file.getName(), objectKey);
        } catch (QSException e) {
            Logs.error("上传文件至青云对象存储异常 文件名称:{}", objectKey);
            badRequest(e.getErrorMessage());
        }
        File mongoFile = new File();
        mongoFile.originalName = file.getName();
        mongoFile.finalName = objectKey;
        mongoFile.path = QingCloudConsts.getStoreBucketName() + "/" + mongoFile.finalName;
        mongoFile.url = FILE_URL_PREFIX + mongoFile.finalName;
        mongoFile.contentType = MimeTypes.getContentType(file.getName());
        fileCollection.save(mongoFile);
        created(Jsonable.toPrettyJson(mongoFile));
    }


    private static String generateClientNumber() {
        Long incrKey = RedisUtils.executeResult(jedis -> jedis.incr(CLIENT_NUMBER_INCR_KEY));
        Logs.info("Redis client number incr value:{}", incrKey);
        String clientNumber = hashids.encode(incrKey);
        return clientNumber;
    }

    private static List<Sample> getGenericSamples(Requisition requisition) {
        List<Sample> samples = Lists.newArrayList();
        try {
            samples = Reflect.on(requisition).get("samples");
            if (samples.isEmpty()) {
                Logs.warn("Samples not read.Requisition Id:{}", requisition.id);
            }
        } catch (ReflectException e) {
            Logs.error("Error occurred while reflect on field samples.Requisition Id:{}", requisition.id);
        }
        Reflect.on(requisition).set("samples", null);//将申请单中的samples置空
        return samples;
    }


    private static <T> T getField(Requisition requisition, String fieldName) {
        T t = null;
        try {
            t = Reflect.on(requisition).get(fieldName);
        } catch (ReflectException e) {
            Logs.error("Failed to reflect on field {}.Requisition Id:{}", fieldName, requisition.id);
        }
        return t;
    }

    /**
     * 单号录入
     *
     * @param id
     */
    public static void saveTrackingNumber(@MongoId @Required String id) {
        RequisitionDTO input = readBody(RequisitionDTO.class);
        String userId = currentUserId();
        Requisition requisition = requisitionCollection.findOne("{_id:#,userId:#}", new ObjectId(id), userId).as(Requisition.class);
        badRequestIfNull(requisition, "requisition not exist.Id:" + id);
        handleTrackingNumber(requisition, input.trackingNumber, userId);
        ok();
    }


    public static void batchSaveTrackingNumber() {
        String userId = currentUserId();
        List<BatchTrackingNumber> batchTrackingNumbers = readBodyList(BatchTrackingNumber.class);
        if (!batchTrackingNumbers.isEmpty()) {
            List<ObjectId> ids = batchTrackingNumbers.stream()
                    .map(batchTrackingNumber -> new ObjectId(batchTrackingNumber.id))
                    .collect(Collectors.toList());
            Map<String, String> trackingNumberMap = Maps.newHashMap();
            batchTrackingNumbers.forEach(batchTrackingNumber -> trackingNumberMap.put(batchTrackingNumber.id, batchTrackingNumber.trackingNumber));
            List<Requisition> requisitions = MongoUtils.toList(requisitionCollection.find("{_id:{$in:#}}", ids).as(Requisition.class));
            if (requisitions.isEmpty()) {
                badRequest("未找到符合条件的申请单");
            }
            requisitions.forEach(requisition -> handleTrackingNumber(requisition, trackingNumberMap.get(requisition.id), userId));
        }
        ok();
    }

    private static void handleTrackingNumber(Requisition requisition, String trackingNumber, String userId) {
        if (StringUtils.isNotBlank(requisition.trackingNumber)) {
            return;
        }
        requisition.trackingNumber = trackingNumber;
        requisitionCollection.save(requisition);
        WriteResult writeResult = sampleCollection.update("{requisitionId:#,userId:#}", requisition.id, userId)
                .with("{$set:{trackingNumber:#}}", requisition.trackingNumber);
        if (!writeResult.isUpdateOfExisting()) {
            Logs.warn("sample中数据不存在, requisitionId :{}", requisition.id);
        }
        SampleDelivery sampleDelivery = new SampleDelivery();
        sampleDelivery.limsNumber = requisition.limsNumber;
        sampleDelivery.trackingNumber = requisition.trackingNumber;
        MongoCursor<Sample> sampleCursor = sampleCollection.find("{requisitionId: #,userId:#}", requisition.id, userId).as(Sample.class);
        sampleDelivery.samples = Lists.newArrayList(sampleCursor.iterator());
        try {
            LimsApi.notifySampleDelivery(sampleDelivery);
        } catch (IOException e) {
            Logs.error("Failed to notify sample delivery.requisitionId:{}", requisition.id);
            badRequest("网络异常，推送快递信息失败，请稍后重试");
        } catch (LimsException e) {
            error("服务器内部错误，请联系技术支持");
        }
    }


    public static void backfillDiseases(@Required String id) {
        Requisition requisition = requisitionCollection.findOne(Oid.withOid(id)).as(Requisition.class);
        badRequestIfNull(requisition, "申请单不存在");
        List<Disease> diseases = readBodyList(Disease.class);
        requisition.data.put("diseases", diseases);
        requisition.updateAt = new Date();
        requisitionCollection.save(requisition);
        renderJSON(requisition);
    }


    /**
     * 主动策略同步申请单样本状态
     *
     * @param id
     */
    public static void syncSampleStates(@MongoId @Required String id) {
        Requisition requisition = requisitionCollection.findOne(Oid.withOid(id)).as(Requisition.class);
        badRequestIfNull(requisition, String.format("申请单%s不存在", id));
        SampleStatusQueryJob.syncSampleStates(requisition);
    }


    private static void setField(Requisition requisition, String fieldName, Object fieldValue) {
        try {
            Reflect.on(requisition).set(fieldName, fieldValue);
        } catch (ReflectException e) {
            Logs.error("Failed to reflect on field {}.Requisition Id:{}", fieldName, requisition.id);
        }
    }

    public static String getDuplicateMsg(DuplicateKeyException e) {
        String errMsg = e.getErrorMessage();
        Logs.error("Get mongo duplicate exception:{}", errMsg, e);
        int beginIndex = errMsg.indexOf("\"") + 1;
        int endIndex = errMsg.lastIndexOf("\"");
        String duplicateBarcode;
        try {
            duplicateBarcode = errMsg.substring(beginIndex, endIndex);
        } catch (StringIndexOutOfBoundsException e1) {
            Logs.error("解析Mongo duplicate Exception 时发生异常", e);
            return errMsg;
        }
        return String.format("已存在的条码:%s", duplicateBarcode);
    }


    public static List<String> processDetectionProjects(Object detectionProjects, String primaryProjectId) {
        List<String> projects = JSON.parseArray(JSON.toJSONString(detectionProjects), String.class);
        List<String> projectIds = Lists.newArrayList();
        if (projects != null && !projects.isEmpty()) {
            projects.forEach(s -> {
                Object projectId = projectMap.get(s);
                if (projectId != null) {
                    // FIXME: 2018/5/2 临时处理不同单子PGS对应检测项目的问题，后续把所有检测项目改为接口获取形式
                    if ("PGS".equals(s)) {
                        //平衡易位携带者的PGS项目对应的是平衡易位染色体拷贝数分析
                        //罗氏易位携带者的PGS项目对应的是罗氏易位染色体拷贝数分析
                        if (Project.B_MISEQ.id().equals(primaryProjectId)) {
                            projectId = Project.B_PGD.id();
                        } else if (Project.S_PGD.id().equals(primaryProjectId)) {
                            projectId = Project.RT_PGD.id();
                        }
                    }
                    projectIds.add(String.valueOf(projectId));
                }
            });
        }
        return projectIds;
    }


}
