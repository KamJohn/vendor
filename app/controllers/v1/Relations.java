package controllers.v1;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import consts.RelationConsts;
import controllers.api.API;
import controllers.api.interceptor.Secure;
import jobs.RelationSyncJob;
import models.client.Relation;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import play.mvc.With;
import utils.MongoUtils;
import utils.RedisUtils;
import utils.StringUtils;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-06 上午10:37
 * @description 亲缘关系
 */
@With({Secure.class})
public class Relations extends API {

    private static MongoCollection relationCollection = MongoUtils.getRelationCollection();

    public static void getRelations() {
        String relationStr = RedisUtils.executeResult(jedis -> jedis.get(RelationConsts.RELATION_KEY));
        List<Relation> relations;
        if (StringUtils.isBlank(relationStr)) {
            MongoCursor cursor = relationCollection.find().as(Relation.class);
            relations = Lists.newArrayList(cursor.iterator());
            relationStr = JSON.toJSONString(relations);
        }
        renderJSON(relationStr);
    }


    public static void syncRelations() {
        new RelationSyncJob().now();
    }

}
