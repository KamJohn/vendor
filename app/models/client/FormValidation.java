package models.client;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-25 下午7:46
 * @description
 */
public class FormValidation {


    private List<String> base = Lists.newArrayList();

    private List<String> sample = Lists.newArrayList();


    public static FormValidation with() {
        return new FormValidation();
    }

    public FormValidation base(String... fields) {
        this.base.addAll(Arrays.asList(fields));
        return this;
    }

    public FormValidation base(List fields) {
        this.base.addAll(fields);
        return this;
    }

    public FormValidation baseIgnore(String... fields) {
        this.base.removeAll(Arrays.asList(fields));
        return this;
    }

    public FormValidation sampleIgnore(String... fields) {
        this.sample.removeAll(Arrays.asList(fields));
        return this;
    }


    public FormValidation sample(String... fields) {
        this.sample.addAll(Arrays.asList(fields));
        return this;
    }

    public FormValidation sample(List fields) {
        this.sample.addAll(fields);
        return this;
    }

    public List<String> getBase() {
        return base;
    }

    public List<String> getSample() {
        return sample;
    }
}
