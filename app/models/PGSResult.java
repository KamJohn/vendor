package models;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:fivesmallq@gmail.com">fivesmallq</a>
 * @version Revision: 1.0
 * @date 16/8/6 下午3:13
 */
public class PGSResult extends BaseOpenModel {

    private String reportId;
    private Map<String, String> statistical;
    private List<Chromosome> chromosomes;
    private Map<String, String> imgWithXY;
    private Map<String, String> imgNoXY;
    private Map<String, String> imgForXiangya;
    private String reportGroupId;
    private String reportGroupName;
    private String sampleName;
    private String result;
    private String dataName;
    private String resultM;
    private String resultGender;
    private String resultAneu;
    private String resultCNV;
    private String summary;
    private String summaryGender;
    private String summaryAneu;
    private String summaryCNV;
    private String dataVersion;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public Map<String, String> getStatistical() {
        return statistical;
    }

    public void setStatistical(Map<String, String> statistical) {
        this.statistical = statistical;
    }

    public List<Chromosome> getChromosomes() {
        return chromosomes;
    }

    public void setChromosomes(List<Chromosome> chromosomes) {
        this.chromosomes = chromosomes;
    }

    public Map<String, String> getImgWithXY() {
        return imgWithXY;
    }

    public void setImgWithXY(Map<String, String> imgWithXY) {
        this.imgWithXY = imgWithXY;
    }

    public Map<String, String> getImgNoXY() {
        return imgNoXY;
    }

    public void setImgNoXY(Map<String, String> imgNoXY) {
        this.imgNoXY = imgNoXY;
    }

    public Map<String, String> getImgForXiangya() {
        return imgForXiangya;
    }

    public void setImgForXiangya(Map<String, String> imgForXiangya) {
        this.imgForXiangya = imgForXiangya;
    }

    public String getReportGroupId() {
        return reportGroupId;
    }

    public void setReportGroupId(String reportGroupId) {
        this.reportGroupId = reportGroupId;
    }

    public String getReportGroupName() {
        return reportGroupName;
    }

    public void setReportGroupName(String reportGroupName) {
        this.reportGroupName = reportGroupName;
    }

    public String getSampleName() {
        return sampleName;
    }

    public void setSampleName(String sampleName) {
        this.sampleName = sampleName;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDataName() {
        return dataName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public String getResultM() {
        return resultM;
    }

    public void setResultM(String resultM) {
        this.resultM = resultM;
    }

    public String getResultGender() {
        return resultGender;
    }

    public void setResultGender(String resultGender) {
        this.resultGender = resultGender;
    }

    public String getResultAneu() {
        return resultAneu;
    }

    public void setResultAneu(String resultAneu) {
        this.resultAneu = resultAneu;
    }

    public String getResultCNV() {
        return resultCNV;
    }

    public void setResultCNV(String resultCNV) {
        this.resultCNV = resultCNV;
    }

    public String getSummaryGender() {
        return summaryGender;
    }

    public void setSummaryGender(String summaryGender) {
        this.summaryGender = summaryGender;
    }

    public String getSummaryAneu() {
        return summaryAneu;
    }

    public void setSummaryAneu(String summaryAneu) {
        this.summaryAneu = summaryAneu;
    }

    public String getSummaryCNV() {
        return summaryCNV;
    }

    public void setSummaryCNV(String summaryCNV) {
        this.summaryCNV = summaryCNV;
    }

    public String getDataVersion() {
        return dataVersion;
    }

    public void setDataVersion(String dataVersion) {
        this.dataVersion = dataVersion;
    }
}
