package controllers.v1;

import com.google.common.collect.Lists;
import controllers.api.API;
import controllers.api.interceptor.Secure;
import models.client.Product;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import play.mvc.With;
import utils.MongoUtils;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-07-20 下午6:28
 * @description
 */
@With({Secure.class})
public class Projects extends API {

    private static MongoCollection projectCollection = MongoUtils.getProjectCollection();

    public static void get() {
        MongoCursor cursor = projectCollection.find().sort("{_id:1}").as(Product.class);
        List<Product> products = Lists.newArrayList(cursor.iterator());
        renderJSON(products);
    }
}
