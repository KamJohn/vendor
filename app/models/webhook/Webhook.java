package models.webhook;


import java.util.Map;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-04-25 下午4:20
 */
public class Webhook {
    public String businessType;
    public String notificationType;
    public String dataId;
    public Map<String, Object> data;

    public Webhook(String businessType, String notificationType, String dataId, Map<String, Object> data) {
        this.businessType = businessType;
        this.notificationType = notificationType;
        this.dataId = dataId;
        this.data = data;
    }

    public Webhook() {
    }

}
