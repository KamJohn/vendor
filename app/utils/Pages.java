package utils;

/**
 * @author <a href="mailto:fivesmallq@gmail.com">fivesmallq</a>
 * @version Revision: 1.0
 * @date 17/1/16 上午10:12
 */
public class Pages {
    private static final int MAX_OFFSET = 100;
    private static final int MAX_LIMIT = 50;
    private static final int DEFAULT_LIMIT = 10;

    public static int safeLimit(int limit) {
        limit = limit <= 0 ? DEFAULT_LIMIT : limit;
        return limit > MAX_LIMIT ? MAX_LIMIT : limit;
    }

    public static int safeOffset(int offset) {
        offset = offset < 0 ? 0 : offset;
        return offset > MAX_OFFSET ? MAX_OFFSET : offset;
    }
}
